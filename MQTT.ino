/*********
  Rui Santos
  Complete project details at https://randomnerdtutorials.com  
*********/

using namespace std;

#include <WiFi.h>
#include <PubSubClient.h>
#include <Wire.h>
#include "DHT.h"
#define DHTPIN 4 // Digital pin connected to the DHT sensor
#define DHTTYPE DHT11 // DHT 11
#define SWITCH 32 //PIN5 / Switch to GND
#define LED 14
DHT dht(DHTPIN, DHTTYPE); // initialize DHT

// Replace the next variables with your SSID/Password combination
//------------Zuhause----------------------------
//const char* ssid = "FRITZ!Box 6490 Cable";
//const char* password = "59662761641603061018";
//-----------------------------------------------
//-----------Mosbach-----------------------------
const char* ssid = "WLAN-799284";
const char* password = "0885995454408199";
//-----------------------------------------------

// Add your MQTT Broker IP address, example:
//const char* mqtt_server = "192.168.1.144";
const char* mqtt_server = "192.168.2.106";

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;

//uncomment the following lines if you're using SPI
/*#include <SPI.h>
#define BME_SCK 18
#define BME_MISO 19
#define BME_MOSI 23
#define BME_CS 5*/


float temperature = 0;
bool state_taster = true;


void setup() {
  Serial.begin(115200);
  // default settings
  // (you can also pass in a Wire library object like &Wire2)

  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  pinMode(LED, OUTPUT);
  pinMode(SWITCH, INPUT_PULLUP); // setzt den Pin als Eingang und auf HIGH
  dht.begin();
}

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* message, unsigned int length) {
  Serial.print("Message arrived on topic: ");
  Serial.print(topic);
  Serial.print(". Message: ");
  String messageTemp;
  
  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();

  // If a message is received on the topic esp32/output, you check if the message is either "on" or "off". 
  // Changes the output state according to the message
  if (String(topic) == "paketbox/frontend/door") {
    Serial.print("Changing output to ");
    if(messageTemp == "{\"Door\": 1}"){
      Serial.println("Door: 1");
      digitalWrite(LED, HIGH);
    }
    else if(messageTemp == "{\"Door\": 0}"){
      Serial.println("Door: 0");
      digitalWrite(LED, LOW);
    }
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("Esp32")) {
      Serial.println("connected");
      // Subscribe
      client.subscribe("paketbox/frontend/#");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  if (digitalRead(SWITCH)==HIGH){
    state_taster = false;
  }
    
   if (digitalRead(SWITCH)==LOW && state_taster == false){
    Serial.println("Bell: 1");
    client.publish("paketbox/backend/Bell", "Bell: 1");
    state_taster = true;
  }
  
  // Temperature in Celsius
  delay(100);
  if (temperature != dht.readTemperature()){
    temperature = dht.readTemperature();
    char temp[5];
    char text1[24] = "{\"Temperature\": ";
    char text2[3] = "}";
    
    sprintf(temp, "%g", temperature);
    strcat(text1, temp);
    strcat(text1, text2);
    Serial.print(text1);
    client.publish("paketbox/frontend/Temperature", text1);  
   }
}
