let socket = io();
socket.connect("http://" + location.host);

//beim Verbinden mit dem Websocket-Server wird eine Bestätigungsnachricht ausgegeben
socket.on("connect", () => {
    console.log("connected with host");
});

//Funktion: beim laden der Seite wird das Eingabefeld überprüft und die Tabelle für alle Logs wird generiert
//Parameter (): keine
//Rückgabe: keine
document.addEventListener("DOMContentLoaded", function() { 
    //Funktion: holt sich Daten aus API und führt das Erstellen der Logs aus
    //Parameter (recent_logs): die letzten 10 eingetragenen Logs
    //Rückgabe: keine
    get_json("/recent_logs").then(recent_logs => {    
        const TABLE_ID = document.getElementById("log_tabel");
        let use_table_creater = new Create_table( TABLE_ID , recent_logs);
        use_table_creater.fill_unchangeable_table();
        const LOADING = document.getElementById("loading");
        LOADING.remove();
        return; 
    });
});

//Funktion: auf diesem Websocket-Kanal wird die Tabelle für die Logs geupdated
//Parameter (recent_log): der neueste Log
//Rückgabe: keine
socket.on('log_confirmation', function( recent_log ) {
    const TABLE_ID = document.getElementById("log_tabel");
    let use_table_creater = new Create_table( TABLE_ID , recent_log);
    use_table_creater.update_unchangeable_table(10);
    return;
});