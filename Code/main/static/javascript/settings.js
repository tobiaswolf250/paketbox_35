let socket = io();
socket.connect("http://" + location.host);



//Funktion: beim laden der Seite wird das Eingabefeld überprüft und die Tabelle für alle MAils wird generiert
//Parameter (): keine
//Rückgabe: keine
document.addEventListener("DOMContentLoaded", function() { 
    //prüft, ob das Textfeld erlaubt oder verboten wird
    disable_rows("emails" , "mail_content" , "E-Mail hinzufügen" , 5);
    //Funktion: holt sich Daten aus API und führt das Erstellen der Eingetragenen Mails aus
    //Parameter (all_emails): alle eingetragenen Mails
    //Rückgabe: keine
    get_json("/all_emails").then(all_emails => { 
        const TABLE_ID = document.getElementById("emails");
        let use_table_creater = new Create_table( TABLE_ID , all_emails);
        use_table_creater.fill_table();
        const LOADING = document.getElementById("loading");
        LOADING.remove();
        return; 
    });
});

//beim Verbinden mit dem Websocket-Server wird eine Bestätigungsnachricht ausgegeben
socket.on("connect", () => {
    console.log("connected with host");
});

//Funktion: auf diesem Websocket-Kanal wird die Tabelle für die Mails geupdated
//Parameter (reason): Grund des sendens 
//Rückgabe: keine
socket.on('mail_confirmation', function( reason ) {
    //Funktion: holt sich Daten aus API und führt das Updaten der Mailtabelle aus
    //Parameter (all_emails): alle eingetragenen Mails
    //Rückgabe: keine
    get_json("/all_emails").then(all_emails => { 
        let use_table_creater = new Create_table( document.getElementById("emails") , all_emails);
        use_table_creater.update_table(reason);
        return; 
    });
    disable_rows("emails" , "mail_content" , "E-Mail hinzufügen" , 5);

});

//Funktion:  gibt auskunft übrr änderung der Sprachausgabe
//Parameter ( choosen_text ):  Text der Gesprochen wird 
//Rückgabe: keine
socket.on('change_choosen_scanner', function( choosen_text ) {
  
    //hier auch das mit polipo machen
    //hier auch das mit dem anzeigen achen
    let polipop= new Polipop('mypolipop', {
        layout: 'popups',
        insert: 'before',
        pool: 5,
        progressbar: true,
        effect: 'slide',
        theme: 'compact',
        closer: false
    });
    polipop.add({
        content: "Die Sprachausgabe wurde auf: " + choosen_text + " geändert",
        title: 'Erfolgreich',
        type: 'success'
    });
    

});


//Funktion: hört die ausgewählte Nachricht an
//Parameter (this_button ): den button der gedrückt wurde
//Rückgabe: keine
function listen_scanner_text ( this_button ) {
    const THIS_BUTTON_ID = this_button.id;
    if (THIS_BUTTON_ID.includes("melodie")) {
        const URL = Choose_scanner_text.check_id( this_button.id ); 
        const audio = new Audio(URL);
        audio.play();
    }else {
        const listen_text = Choose_scanner_text.check_id( this_button.id ); 
        let text_for_speak = { "text" : listen_text };
        play_button(text_for_speak);
    }
    return;

}


//Funktion: veraltet das sende der nachticht zum updaten der Sprachausgabe
//Parameter (this_button): der button der gedrückt wurde 
//Rückgabe: keine
function submit_scanner_speak ( this_button ) {
    const THIS_BUTTON_ID = this_button.id;
    if (THIS_BUTTON_ID.includes("Melodie")) {
        send_melody(THIS_BUTTON_ID);
    }else {
        send_text(THIS_BUTTON_ID);
    }

    return;
}

//Funktion: verpackt und sendet die Nachricht zum updaten der sprachausgabe 
//Parameter (this_button_id): die id des buttons der in html gedrückt wurde
//Rückgabe: keine
function send_melody (this_button_id) { 
    const CHANNEL = "scanner_confirmation_speak";
    const MSG = JSON.stringify({ "choosen_button" : this_button_id });
    socket.emit(CHANNEL , MSG);
}

//Funktion: verpackt und sendet die nachricht zum updaten der sprachausgabe
//Parameter (this_button_id): die id des buttons der auf dem frontend gedrückt wurde 
//Rückgabe: keine
function send_text (this_button_id) {
    const NEW_TEXT = Choose_scanner_text.check_id( this_button_id ); 
    const CHANNEL = "scanner_confirmation_speak";
    const MSG = JSON.stringify({ "text" : NEW_TEXT , "choosen_button" : this_button_id });
    socket.emit(CHANNEL , MSG);
}

//Funktion: sende die Nachricht zum updaten des values
//Parameter (msg): zu sendente Nachricht
//Parameter (chanel): der Kanal, auf dem gesendet werden soll
//Rückgabe: keine
function socket_send_for_update (msg , chanel) {
    socket.emit(chanel , msg);
    return;
}

//Funktion: wird aufgerufen, wenn eine Mail zum updaten eingetragen wurde
//Parameter (this_row): html-Identifer, der Zeile, in der die Mail steht
//Rückgabe: keine
function update_row ( this_row ) {
    //Funktion: holt sich Daten aus API und führt die Funktion des Knopfes zum änder der Mail aus und sendet eine Nachricht ans backend
    //Parameter (all_emails): alle eingetragenen Mails
    //Rückgabe: keine
    get_json("/all_emails").then(all_emails => { 
        let update_email = new Check_emails();
        const DATA_TO_SEND = update_email.change_email( this_row , all_emails );
        // wenn nichts eingegeben, wird Funktion abgebrochen
        if (! DATA_TO_SEND) {
            return; 
        }

        // sendet Nachricht mit Datum ans Backend via Websocket
        socket_send_for_update (DATA_TO_SEND , "change_mail");
        return; 
    });
}

//Funktion: wird aufgerufen, wenn eine Mail gelöscht wird
//Parameter (this_row): html-Identifer, der Zeile, in der die Mail steht
//Rückgabe: keine
function delete_row ( this_row ) {
    let delete_mail = new Check_emails();
    const DATA_TO_SEND = delete_mail.delete_email(this_row );
    // wenn nichts eingegeben, wird Funktion abgebrochen
    if (! DATA_TO_SEND) {
        return; 
    }
    // sendet Nachricht mit Datum ans Backend via Websocket
    socket_send_for_update (DATA_TO_SEND , "delete_mail");
    return;
}

//Funktion: wird aufgerufen, wenn eine neue Mail eingetragen wurde
//Parameter (): keine
//Rückgabe: keine
function new_email_entry() {
    // holt sich Daten aus API und führt in callback das updaten aus
    get_json("/all_emails").then(all_emails => { 
        let check_email = new Check_emails();
        const DATA_TO_SEND = check_email.enter_new_email(all_emails);
        //leert Eingabefeld
        document.getElementById('mail_content').value = '';
        // wenn nichts eingegeben, wird Funktion abgebrochen
        if (! DATA_TO_SEND) {
            return; 
        }

        // sendet Nachricht mit Datum ans Backend via Websocket
        socket_send_for_update (DATA_TO_SEND , "new_mail");
        return; 
    });
}

//Funktion: wird aufgerufen, wenn eine die zeiten geändert wurde
//Parameter (old_start_h): die alte start-Öffnungszeit-Stunde
//Parameter (old_start_m): die alte start-Öffnungszeit-Minunte
//Parameter (old_stop_h): die alte schließungs-Öffnungszeit-Stunde
//Parameter (old_stop_m): die alte schließungs-Öffnungszeit-Minunte
//Rückgabe: keine
function check_times( old_start_h , old_start_m , old_stop_h , old_stop_m ) {
    let use_time_checker = new Time_checker( old_start_h , old_start_m , old_stop_h , old_stop_m );
    const DATA_TO_SEND = use_time_checker.check_times();
    if (! DATA_TO_SEND) {
        return; 
    }
    
    // sendet Nachricht mit Datum ans Backend via Websocket
    socket_send_for_update (DATA_TO_SEND , "new_time");
    return;
}


//Funktion: auf diesem Websocket-Kanal werden die zeiten auf dem Frontend dynamisch verändert
//Parameter (new_times): die neuen Öffnunszeiten 
//Rückgabe: keine
socket.on('time_confirmation', ( new_times ) => {
    let data_update = "Aktuelle Nutzungszeit: " + new_times["start_hour"] + ":" + new_times["start_minute"]
                                + " Uhr bis " + new_times["end_hour"] + ":" + new_times["end_minute"] + " Uhr ";
    document.getElementById('old_val').innerHTML = data_update;
    let polipop= new Polipop('mypolipop', {
        layout: 'popups',
        insert: 'before',
        pool: 5,
        progressbar: true,
        effect: 'slide',
        theme: 'compact',
        closer: false
    });
    polipop.add({
        content: 'Nutzungszeit aktualisiert',
        title: 'Erfolgreich',
        type: 'success'
    });
});

