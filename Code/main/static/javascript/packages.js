let socket = io();
socket.connect("http://" + location.host);

//beim Verbinden mit dem Websocket-Server wird eine Bestätigungsnachricht ausgegeben
socket.on("connect", () => {
    console.log("connected with host");
});

//Funktion: auf diesem Websocket-Kanal wird die Tabelle für die erwarteten Paketnummer erstellt
//Parameter (reason): Grund des sendens 
//Rückgabe: keine
socket.on('number_confirmation', function( reason ) {
    //Funktion: holt sich Daten aus API und führt das erstellen der erwarteten Paketnummer aus
    //Parameter (all_packagenumbers): alle eingetragenen Paketnummern
    //Rückgabe: keine
    get_json("/all_packagenumbers").then(all_packagenumbers => { 
        let use_table_creater = new Create_table( document.getElementById("packages") , all_packagenumbers);
        use_table_creater.update_table(reason);
        return; 
    });
});

//Funktion: sende die Nachricht zum updaten des values
//Parameter (msg): zu sendente Nachricht
//Parameter (chanel): der Kanal, auf dem gesendet werden soll
//Rückgabe: keine
function socket_send_for_update (msg , chanel) {
    socket.emit(chanel , msg );
    return;
}

//schaut, ob die Maximale Anzahl an Mails bereits eingetragen ist
//Funktion: auf diesem Websocket-Kanal wird die Tabelle für die erwarteten Paketnummer erstellt
//Parameter (reason): Grund des sendens 
//Rückgabe: keine
document.addEventListener("DOMContentLoaded", function() { 
    //Funktion: holt sich Daten aus API und führt das updaten der erwarteten Paketnummer aus
    //Parameter (all_packagenumbers): alle eingetragenen Paketnummern
    //Rückgabe: keine
    get_json("/all_packagenumbers").then(all_packagenumbers => { 
        const TABLE_ID = document.getElementById("packages");
        let use_table_creater = new Create_table( TABLE_ID , all_packagenumbers);
        use_table_creater.fill_table();
        const LOADING = document.getElementById("loading_01");
        LOADING.remove();
        return; 
    });

    //Funktion: holt sich Daten aus API und führt das updaten der zugestellten Paketnummer aus
    //Parameter (all_delivered_packagenumbers): alle zugestellten Paketnummern
    //Rückgabe: keine
    get_json("/all_delivered_packagenumbers").then(all_delivered_packagenumbers => {        
        const TABLE_ID = document.getElementById("packagenumbers_delivered");
        let use_table_creater = new Create_table( TABLE_ID , all_delivered_packagenumbers);
        use_table_creater.fill_unchangeable_table();
        const LOADING = document.getElementById("loading_02");
        LOADING.remove(); 
        return; 
    });
});

//Funktion: auf diesem Websocket-Kanal wird die Tabelle der zugestellten Paketnummer auf dem Frontend dynamisch verändert
//Parameter (all_delivered_packagenumbers): die neu zugestellte Paketnummer 
//Rückgabe: keine
socket.on('number_delivered_confirmation', function( all_delivered_packagenumbers ) {
    const TABLE_ID = document.getElementById("packagenumbers_delivered");
    let use_table_creater = new Create_table( TABLE_ID , all_delivered_packagenumbers);
    use_table_creater.update_unchangeable_table(5);
    //Funktion: holt sich Daten aus API und führt das erstellen der Tabelle aller erwarteten Paketnummern aus
    //Parameter (all_emails): alle eingetragenen Mails
    //Rückgabe: keine
    get_json("/all_packagenumbers").then(all_packagenumbers => { 
        let use_table_creater = new Create_table( document.getElementById("packages") , all_packagenumbers);
        use_table_creater.update_table("delete");
        return; 
    });

});

//Funktion: wird aufgerufen, wenn eine Paketnummer zum updaten eingetragen wurde
//Parameter (this_row): html-Identifer, der Zeile, in der die Paketnummer steht
//Rückgabe: keine
function update_row (this_row ) {
    //Funktion: holt sich Daten aus API und führt die Funktion des Knopfes zum änder der Paketnummer aus und sendet eine Nachricht ans backend
    //Parameter (all_packagenumbers): alle  eingetragenen erwarteten Paketnummern
    //Rückgabe: keine
    get_json("/all_packagenumbers").then(all_packagenumbers => { 
        let update_packagenumber = new Handle_packenumbers();
        const DATA_TO_SEND = update_packagenumber.change_packagenumber( this_row, all_packagenumbers);
        // wenn nichts eingegeben, wird Funktion abgebrochen
        if (! DATA_TO_SEND) {
            return;
        }
        
        // sendet Nachricht mit Datum ans Backend via Websocket
        socket_send_for_update (DATA_TO_SEND , "change_packagenumber");
        return;
    }); 
}

//Funktion: wird aufgerufen, wenn eine erwartete Paketnummer gelöscht wird
//Parameter (this_row): html-Identifer, der Zeile, in der die Paketnummer steht
//Rückgabe: keine
function delete_row ( this_row ) {
    let delete_packagenumber = new Handle_packenumbers();
    const DATA_TO_SEND = delete_packagenumber.delete_packagenumber(this_row);
    // wenn nichts eingegeben, wird Funktion abgebrochen
    if (! DATA_TO_SEND) {
        return; 
    }
    
    // sendet Nachricht mit Datum ans Backend via Websocket
    socket_send_for_update (DATA_TO_SEND , "delete_packagenumber");
    return;
} 

//Funktion: wird aufgerufen, wenn eine neue Paketnummer eingetragen wurde
//Parameter (): keine
//Rückgabe: keine
function check_new_packagenumber () {
    //Funktion: holt sich Daten aus API und führt die Funktion zum verarbeiten einer neuen Paketnummern aus
    //Parameter (all_packagenumbers): alle eingetragenen Paketnummern
    //Rückgabe: keine
    get_json("/all_packagenumbers").then(all_packagenumbers => { 
        let check_packagenumber = new Handle_packenumbers();
        const DATA_TO_SEND = check_packagenumber.enter_new_packagenumber(all_packagenumbers);
        //leert Eingabefeld
        document.getElementById('package_number_content').value = '';
        // wenn nichts eingegeben, wird Funktion abgebrochen
        if (! DATA_TO_SEND) {
            return;
        } 

        // sendet Nachricht mit Datum ans Backend via Websocket
        socket_send_for_update (DATA_TO_SEND , "new_packagenumber");
        return;
    }); 
}