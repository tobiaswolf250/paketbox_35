let socket = io();
socket.connect("http://" + location.host);

//Funktion: updated die States die angezeigt werden
//Parameter (states): dict der Zustände
//Rückgabe: keine
get_json("/states").then(states => { 
    states_update = new Handle_states();
    states_update.get_new_states(states);
}).catch(error => {
    console.error(error);
}); 

//beim Verbinden mit dem Websocket-Server wird eine Bestätigungsnachricht ausgegeben
socket.on("connect", () => {
    console.log("connected with host");
});

//Funktion: sendet Nachricht für geöffnete Paketbox via Websocket 
//Parameter (): keine
//Rückgabe: keine
function open_package_box(){
    socket.emit ("my_states_changes_by_frontend" , {"Boxstate" : 1} );
}

//Funktion: sendet Nachricht für geöffnete Tür via Websocket 
//Parameter (): keine
//Rückgabe: keine
function door(){
    socket.emit ("my_states_changes_by_frontend" ,  {"Door" : 1});
}

//Funktion: auf diesem Websocket-Kanal werden die Zustände aktualisiert 
//Parameter (states): dict der Zustände
//Rückgabe: keine
socket.on('my_states_changes_surface', function(states) {
    //verarbeitet den neuen Zustand
    states = JSON.parse(states); 
    states_update = new Handle_states();
    states_update.get_new_states(states);
    return;
});

//Funktion: sende die Nachricht zum updaten des values
//Parameter (msg): zu sendente Nachricht
//Parameter (chanel): der Kanal, auf dem gesendet werden soll
//Rückgabe: keine
function socket_send_for_update (msg , chanel) {
    socket.emit(chanel , msg );
    return; 
}

//Funktion: prüft die Paketnummer
//Parameter (): keine
//Rückgabe: keine

//Funktion: wird aufgerufen, wenn eine neue Paketnummer eingetragen wurde
//Parameter (): keine
//Rückgabe: keine
function check_new_packagenumber () {
    //Funktion: holt sich Daten aus API und führt die Funktion zum verarbeiten einer neuen Paketnummern aus
    //Parameter (all_packagenumbers): alle eingetragenen Paketnummern
    //Rückgabe: keine
    get_json("/all_packagenumbers").then(all_packagenumbers => { 
        //Prüft neue Paketnummer
        let check_packagenumber = new Handle_packenumbers();
        const DATA_TO_SEND = check_packagenumber.enter_new_packagenumber(all_packagenumbers);
        //leert Eingabefeld
        document.getElementById('package_number_content').value = '';
        if (! DATA_TO_SEND) {
            return;
        }
        
        //sendet die Websocket-Nachricht
        socket_send_for_update (DATA_TO_SEND , "new_packagenumber");
        return;
    }); 
}

//Funktion: führt die Sprachfunktion aus
//Parameter (): keine
//Rückgabe: keine
function speecher () {
    //holt sich Sprachausgabe-Array und führt Sprachfunktion aus
    get_json("/states_for_speak").then(states_for_speak => { 

        play_button(states_for_speak);
    
    }).catch(error => {
        console.error(error);
    }); 
    return;
}