//Funktion: führt die Sprachausgabe aus
//Parameter (states_for_speak): das dict, welches den Sprachtext enthält
//Rückgabe: keine
function play_button(states_for_speak) {
  if (!speechSynthesis.speaking) {
    play_text(states_for_speak);
  }else if (speechSynthesis.speaking) {
    stop_text();
  }

  return;
}



//Funktion: erstellt die Eigenschaften der Sprchausgabe und startet diese
//Parameter (text_dict): ein dict mit dem Inhalt des Textes im value, der gesprochen werden soll
//Rückgabe: keine
function play_text(text_dict) {
  let utterance = new SpeechSynthesisUtterance();
  let text_to_speak = "";

  for (var key in text_dict){
      text_to_speak += (text_dict[key] + " ");
  }

  if (speechSynthesis.speaking) {
    return;
  }
  //Eigenschaften der Sprachausgabe
  utterance.text = text_to_speak;
  utterance.rate = 0.7;
  utterance.lang = "de-DE";
  speechSynthesis.speak(utterance);
}

//Funktion: stop die Sprachfunktion
//Parameter (): keine
//Rückgabe: keine
function stop_text() {
  speechSynthesis.resume();
  speechSynthesis.cancel();
}
