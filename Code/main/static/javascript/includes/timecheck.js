//prüft die Eingabe für die neuen Öffnungs-Zeiten
class Time_checker {

    //Funktion: weist die alten und neuen Zeit-Werte zu
    //Parameter (old_start_h): die alte start-Öffnungszeit-Stunde
    //Parameter (old_start_m): die alte start-Öffnungszeit-Minunte
    //Parameter (old_stop_h): die alte schließungs-Öffnungszeit-Stunde
    //Parameter (old_stop_m): die alte schließungs-Öffnungszeit-Minunte
    //Rückgabe: keine
    constructor( old_start_h , old_start_m , old_stop_h , old_stop_m ) {

        const start = document.getElementById('appt').value;
        const end = document.getElementById('end').value;
        //neuen Zeitwerte
        this.__new_times = {};
        this.__new_times.start_hour = start.slice( 0 , 2 );
        this.__new_times.start_minute = start.slice( 3 , 5);
        this.__new_times.end_hour = end.slice( 0 , 2);
        this.__new_times.end_minute = end.slice( 3 , 5); 
        //alte Zeitwerte
        this.__old_times = {};
        this.__old_times.start_hour = old_start_h;
        this.__old_times.start_minute = old_start_m;
        this.__old_times.end_hour = old_stop_h;
        this.__old_times.end_minute = old_stop_m; 

        this.__polipop__ = new Polipop('mypolipop', {
            layout: 'popups',
            insert: 'before',
            pool: 5,
            progressbar: true,
            effect: 'slide',
            theme: 'compact',
            closer: false
        });
    }

    //Funktion: führt die Zeitprüfung durch
    //Parameter (): keine
    //Rückgabe: keine
    check_times = function () {
        this.__complete_times_dict();
        //schaut ob zeiten gleich sind, wenn, dann soll bearbeitung abgebrochen werden und diese Zeiten sollen eingetragen werden in die DB
        this.__check_timeisanumber();
        if (this.__times_equal()){
            this.__polipop__.add({
                content: 'Eingegebene Zeiten sind gleich und werden nicht verändert',
                title: 'Warnung',
                type: 'warning'
            });
            return;
        }
            
        //prüft die Reihenfolge der eingetragenen Zeiten und --> vgl von start und end
        if (!this.__times_right_pos()){
            //ändert die Reihenfolge, da die Endzeit kleiner als die Startzeit ist
            this.__swap_times();
            this.__polipop__.add({
                content: 'Nutzungszeit wurden falsch herum eingegeben',
                title: 'Info',
                type: 'info'
            });
        }

        return this.__new_times;
    }

    //Funktion:  wechselt Position der Eingegeben Zeiten
    //Params: keine
    //return: keine  
    __swap_times = function () {
        //ändert die Zeiten, wenn der Wert nicht richtig rum eingegeben wurde
        let new_start_h = this.__new_times.end_hour;
        let new_start_m = this.__new_times.end_minute;
        this.__new_times.end_hour = this.__new_times.start_hour;
        this.__new_times.end_minute = this.__new_times.start_minute;
        this.__new_times.start_hour = new_start_h;
        this.__new_times.start_minute = new_start_m;
        return;
    }

    //Funktion:  prüft die Reihenfolge der eingetragenen Zeiten
    //Params: sich selbst
    //return: false, wenn Reihenfolge nicht passt
    __times_right_pos = function () {
        //geht die Bedingungen durch, dass die Zeiten in der richtigen Reihenfolge eingegeben wurden
        if ( parseInt( this.__new_times.end_hour ) < parseInt( this.__new_times.start_hour )) {
            return false;
        }
        if ( this.__new_times.end_hour == this.__new_times.start_hour && 
           parseInt( this.__new_times.end_minute ) < parseInt( this.__new_times.start_minute )) {
            return false;
        }
        return true;
    }

    //Funktion:  schaut, ob gesamte Eingabe in das Schema einer Zahl und das Schema Stunden/ Minuten passt
    //Params: sich selbst
    //return: true;, wenn alle gleich sind
    __times_equal = function () {
        if( this.__new_times.end_hour == this.__new_times.start_hour &&
            this.__new_times.end_minute == this.__new_times.start_minute ) {
            return true;

        }else{
            return false;
        }

    }    
    
    //Funktion:  schaut, ob gesamte Eingabe in das Schema einer Zahl und das Schema Stunden/ Minuten passt
    //Params (): keine
    //return: true;, wenn alles nötige abgearbeitet ist
    __check_timeisanumber = function () {
        for (const [key, value] of Object.entries(this.__new_times)) {
            if( this.__single_number_check( this.__new_times[key] , key) == false ) {
                this.__new_times[key] = this.__old_times[key]
            }
        }
        return;
    }

    //Funktion: prüft die einzelnen Eingagebfelder auf Korrektheit entsprehcne der Definition Stunde oder Minute
    //Parameter (number): Zeit-Auswahl (Stundenauswahl oder Minutenauswahl)
    //Parameter (minute_or_hour): Minute oder Stunde
    //Rückgabe: true, wenn die Zeit passt
    __single_number_check = function( number,  key ) {
        if((number.match(/^[0-9]+$/) != null)) {

            //schauen, ob Eingabe dem Format Stunden oder Minuten entspricht
            let minute_or_hour = key.includes("hour");
            //Zahl für das Zeitformat
            let number = 0;
            if(  minute_or_hour >= 0 ) {
                //prüft, ob Eingabe dem Stunden FOrmat entspricht
                number = 23;
            }else{   
                minute_or_hour = key.includes("minute");
                if( minute_or_hour >= 0 ) {
                    number = 59;
                //prüft, ob Eingabe dem Stunden FOrmat entspricht 
                }
            }

            if( number == 0 ) {
                return false;
            }else{
                return this.__check_time_format( parseInt ( number ) , number);   
            }    
        }else{
            return false;

        }
    }

    //Funktion:  schaut, ob  Zahö in das Schema Stunden/ Minuten passt
    //Params (time_entry): den Eintrag aus dem dict, den man prüfen muss 
    //Params (max_number): die Zahl, entsprechend dem Zahlenformat
    //return: true;, wenn alles nötige abgearbeitet ist
    __check_time_format = function( time_entry , max_number ){
        if( time_entry < 0 || time_entry > max_number ) {
            return false;
        }else{
            return true;
        }

    } 

    //Funktion: fügt in zeitendict der neuen Zahlen die Zahl alten Zeiten ein, wenn nichts eingetragen wurde
    //Parameter (): nichts
    //Rückgabe: keine
    __complete_times_dict = function () {
        for (const [key, value] of Object.entries(this.__new_times)) {
            if( !this.__new_times[key] ) {
                this.__new_times[key] = this.__old_times[key];
            }

        }

        return
    }
}