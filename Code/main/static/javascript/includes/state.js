class Handle_states {

    //Funktion: bekommt die neuen Zuständer und verarbeitet diese
    //Parameter (new_states): die/den neuen Zustand
    //Rückgabe: keine
    get_new_states = function ( new_states ){
        //geht jeden Zustands-Eintrag im dict durch
        for ( let state in new_states ) {
            //ändert das Aussehen des Zustandes
            this.refresh_states( state , new_states[state] );
        }

        return;
    }
    
    //Funktion: geht alle möglichen Zustände durch und bearbeitet entsprechend des übergebenen Wertes d
    //Parameter (key): definiert, um welchen Zustand es sich handelt 
    //Parameter (value): definiert den Wert, des Zustandes
    //Rückgabe: keine
    refresh_states = function  ( key , value ){    
        switch ( key ) {
            case "Boxload":
                this.refreshBoxFull( value );
                break;
            case "Boxstate":
                this.refreshBoxOpen( value );  
                break;
            case "Door":
                this.refreshDoorOpen( value );
                break;
            case "Bell":
                this.refreshBellActive( value );
                break;
            case "ErrorButton":
                this.refreshErrorButton( value );
                break;
            case "Scanner":
                this.refreshScanner( value );
                break;
            case "Lighting":
                this.refreshLight( value );
                break;
            case "Temperature":
                this.refreshTemperature( value );
                break;
            default:
                console.log("der Zustand " + key + " existiert nicht")
                break;    
        }

        return; 
    }
    
    //Funktion: verarbeitet Zustand des Inhaltes der Paketbox und verändert DOM entsprechend
    //Parameter (value): definiert den Wert, der Zustandänderung
    //Rückgabe: keine
    refreshBoxFull = function( value){     
        const BoxFull = document.getElementById('BoxFull');
        const ColorBoxFull = document.getElementById('colorBoxFull');

        if(value){
            BoxFull.innerHTML = 'Paketbox </br> Voll';
            ColorBoxFull.style.background = '#eeb42e';
        }else{
            BoxFull.innerHTML = 'Paketbox </br> Leer';
            ColorBoxFull.style.background = '#92D050';
        } 

        return;
    }

    //Funktion: verarbeitet Zustand ob Paketbox geöffnet ist und verändert DOM entsprechend
    //Parameter (value): definiert den Wert, der Zustandänderung
    //Rückgabe: keine
    refreshBoxOpen = function ( value){
        const ColorBoxState = document.getElementById('colorBoxState');
        const BoxOpen = document.getElementById('BoxOpen');
        if(value){
            BoxOpen.innerHTML = 'Paketbox </br> Geöffnet';
            ColorBoxState.style.background = '#eeb42e';
        }else{
            BoxOpen.innerHTML = 'Paketbox </br> Geschlossen';
            ColorBoxState.style.background = '#92D050';
        } 

        return;
    }

    //Funktion: verarbeitet Zustand welcher anzeigt, ob die Eingangstüre offen ist und verändert DOM entsprechend
    //Parameter (value): definiert den Wert, der Zustandänderung
    //Rückgabe: keine
    refreshDoorOpen = function ( value){
        const DoorOpen = document.getElementById('DoorOpen');
        const ColorDoorState = document.getElementById('colorDoorState');
        if(value){
            DoorOpen.innerHTML = 'Türöffner </br> Betätigt';
            ColorDoorState.style.background = '#eeb42e';
        }else{
            DoorOpen.innerHTML = 'Türöffner </br> Nicht betätigt';
            ColorDoorState.style.background = '#92D050';
        } 

        return;
    }

    //Funktion: verarbeitet Zustand ob Klingeltaster gedrückt wurde und verändert DOM entsprechend
    //Parameter (value): definiert den Wert, der Zustandänderung
    //Rückgabe: keine
    refreshBellActive = function ( value){
        const ColorBellState = document.getElementById('colorBellState');
        const Bell = document.getElementById('Bell');
        if(value){
            Bell.innerHTML = 'Klingeltaster </br> Betätigt';
            ColorBellState.style.background = '#eeb42e';
        }else{
            Bell.innerHTML = 'Klingeltaster </br> Nicht betätigt';
            ColorBellState.style.background = '#92D050';
        } 

        return;
    }

    //Funktion: verarbeitet Zustand der Fehlertaste verändert DOM entsprechend
    //Parameter (value): definiert den Wert, der Zustandänderung
    //Rückgabe: keine
    refreshErrorButton = function ( value){
        const ColorErrorState = document.getElementById('colorErrorState');
        const ErrorButton = document.getElementById('Error');
        if(value){
            ErrorButton.innerHTML = 'Fehlertaste </br> Betätigt';
            ColorErrorState.style.background = '#eeb42e';
        }else{
            ErrorButton.innerHTML = 'Fehlertaste </br> Nicht betätigt';
            ColorErrorState.style.background = '#92D050';
        } 

        return;
    }

    //Funktion: verarbeitet Zustand des Scanners und verändert DOM entsprechend
    //Parameter (value): definiert den Wert, der Zustandänderung
    //Rückgabe: keine
    refreshScanner = function ( value){
        const ColorScannerState = document.getElementById('colorScannerState');
        const Scanner = document.getElementById('Scanner');
        if(value){
            Scanner.innerHTML = 'Scanner </br> Betätigt';
            ColorScannerState.style.background = '#eeb42e';
        }else{
            Scanner.innerHTML = 'Scanner </br> Nicht betätigt';
            ColorScannerState.style.background = '#92D050';
        } 

        return;
    }

    //Funktion: verarbeitet Zustand der Wegbeleuchtung und verändert DOM entsprechend
    //Parameter (value): definiert den Wert, der Zustandänderung
    //Rückgabe: keine
    refreshLight = function( value){
        const ColorLightState = document.getElementById('colorLightState');
        const Light = document.getElementById('Light');
        if(value){
            Light.innerHTML = 'Beleuchtung </br> Aktiv';
            ColorLightState.style.background = '#eeb42e';
        }else{
            Light.innerHTML = 'Beleuchtung </br> Nicht aktiv';
            ColorLightState.style.background = '#92D050';
        } 

        return;
    }

    //Funktion: verarbeitet Zustand der Temperaturanzeige und verändert DOM entsprechend
    //Parameter (value): definiert den Temperaturwert
    //Rückgabe: keine
    refreshTemperature = function ( value){
        const Temperature = document.getElementById('temperature');
        Temperature.innerHTML = value + "°C";
        return;
    }

}

