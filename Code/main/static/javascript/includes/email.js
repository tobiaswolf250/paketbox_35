
//prüft die Eingabe für die Mails auf die gefoderten Gegebenheiten wie z.B: gültige Adresse, vorhanden
class Check_emails {

    //Funktion: weist die alten und neuen Zeit-Werte zu
    //Parameter (): keine
    //Rückgabe: keine
    constructor() {
        //Funktion: weist die alte E-Mail zu
        //Parameter (old_email): die alte E-Mail
        //Rückgabe: keine
        this.__give_old_email = function (old_email) {
            this.__old_email = old_email.trim();
        };
        //Funktion: weist die neue E-Mail zu
        //Parameter (new_email): die neue E-Mail
        //Rückgabe: keine
        this.__give_new_mail = function (new_email) {
            this.__new_email = new_email.trim();
        };
        //Funktion: weist alle E-Mails zu
        //Parameter (all_emails): alle E-Mails
        //Rückgabe: keine
        this.__give_all_emails = function (all_emails) {
            this.__all_mails = all_emails;
        };
        //zum verarbeiten der Message-Banner
        this.__polipop = new Polipop('mypolipop', {
            layout: 'popups',
            insert: 'before',
            pool: 5,
            progressbar: true,
            effect: 'slide',
            theme: 'compact',
            closer: false
        });
    }

    //Funktion: führt den Ablauf zur Änderung eine ausgwählten Mailadresse aus
    //Parameter (this_row): html-Identifer, der Zeile, in der die zu ändernde E-Mail steht
    //Parameter (all_emails): alle gespeicherten E-Mails wurden
    //Rückgabe: wenn erfolgreich, dann wird die nachricht, die gesendet werden soll gesendet
    change_email = function( this_row , all_emails) {
        //speichert die alte Mail als Klassenattribut
        this.__give_old_email(this_row.id);
        //speichert die neue Mail als Klassenattribut aus einem Eingabefenster und beendet die Verarbeitung und gibt eine Meldung aus
        let new_email = prompt("Bitte hier die E-Nummer eintragen", this.__old_email); 
        this.__give_new_mail(new_email);

        if (this.__no_entry()){
            this.__polipop.add({
                content: 'Es wurde keine E-Mail-Adresse eingegeben',
                title: 'Info',
                type: 'info'
              });
            return; 
        }

        //prüft ob die Adresse schon vorhanden ist und gibt diese Meldung aus
        if (this.__the_same_entry ()){
            this.__polipop.add({
                content: 'E-Mail-Adresse ist schon vorhanden',
                title: 'Info',
                type: 'info'
              });
            return;
        }
        //speichert alle Mails in einem Attribut
        this.__give_all_emails(all_emails);
        //prüft die Mail und bereitet sie wenn alles passt so vor, dass Sie mit Websocket versendet werden kann
        if ( ! this.__check_new_mail()){
            return;
        }else{
            return this.__prep_for_update_sending();
        }

    }

    //Funktion: führt den Ablauf zur Eingabe einer neuen E-Mail aus
    //Parameter (all_packagenumbers): alle E-Mails die bisher eingetragen wurden
    //Rückgabe: die neue Mail, wenn die Eingabe passt, sonst keine Rückgabe
    enter_new_email = function (all_emails) {
        //holt den neuen Wert der Mail aus dem Eingabefeld
        let new_email = document.getElementById("mail_content").value;
        //speichert die neue Mail als Klassenattribut
        this.__give_new_mail(new_email);
        //prüft, ob etwas eingetragen wurde
        if (this.__no_entry()){
            this.__polipop.add({
                content: 'Es wurde keine E-Mail-Adresse eingegeben',
                title: 'Info',
                type: 'info'
              });
            return;
        }
        //speichert alle Mails in einem Attribut
        this.__give_all_emails(all_emails);
        //prüft die Mail und gibt sie zurück, wenn alles passt
        if ( ! this.__check_new_mail()){
            return;
        }else{
            return this.__new_email; 
        }

    }

    //Funktion: führt den Ablauf zum Löschen einer alten E-Mail aus
    //Parameter (this_row): html-Identifer, der Zeile, in der die zu löschende E-Mail steht
    //Rückgabe: die zu löschende E-Mail
    delete_email = function ( this_row ) {
        //speichert die alte Mail in einem Attribut
        this.__give_old_email(this_row.id);
        //erwartet die Bestätigung der Löschung
        if ( ! this.__confirm_deletion () ) {
            return;   
        }

        this.__polipop.add({
            content: 'Email: ' + this.__old_email + ' wurde gelöscht',
            title: 'Info',
            type: 'info'
        });
        // //speichert die Reihe der zu löschenden Mail in einem Attribut
        // this.__give_chosen_row(this_row);
        // //löscht die Mail
        // this.__delete_row (table_name);
        // disable_rows( table_name , "mail_content" , "E-Mail eingeben" , 5);
        return this.__old_email; 
    }

    //Funktion: prüft die Mail darauf, ob sie bereits existiert und ob sie nur gültige Zeichen enthält
    //Parameter (): keine
    //Rückgabe: true, wenn die Eingabe  passt
    __check_new_mail = function () {
        //boolscher Wert, welcher true ist, wenn Mail valide ist
        let check = false;
        //schaut ob die mail bereits existiert
        if (this.__email_exist_already(this.__new_email)) {
            this.__polipop.add({
                content: 'Die E-Mail-Adresse ' + this.__new_email + ' ist bereits vorhanden',
                title: 'Info',
                type: 'info'
              });
            return check;
        }

        //schaut ob Mail gültige Zeichen enthält
        if(this.__mail_good()){
            var output = 'Die E-Mail-Adresse ' + this.__new_email + ' wurde hinzugefügt';
            var title = 'Erfolgreich';
            var type = 'success';
            check = true;
        }else{   
            var output = 'Ungültige Eingabe';
            var title = 'Warnung';
            var type = 'warning';
        }
        
        this.__polipop.add({
            content: output,
            title: title,
            type: type
          });
        return check;
    }

    //Funktion: fragt ob Mail gelöscht werden soll
    //Parameter (): keine
    //Rückgabe: die Bestätigung (->true) oder Ablehnung (->false)
    __confirm_deletion = function () {
        return confirm("Wirklich "+ this.__old_email + " löschen?");
    }

    //Funktion: verpackt die alte und neue Mail so, dass sie via Websocket zum updaten gesendet werden kann
    //Parameter (): keine
    //Rückgabe: die verpackte Nachricht
    __prep_for_update_sending = function () {
        let values = {old:this.__old_email, new:this.__new_email};
        values =  JSON.stringify(values);
        return values;
    }

    //Funktion: prüft ob die alte = neue Mail ist
    //Parameter (): keine
    //Rückgabe: true, wenn Mails gleich sind 
    __the_same_entry = function (){
        return this.__new_email == this.__old_email;
    }

    //Funktion: keine Mail eingegeben wurde
    //Parameter (): keine
    //Rückgabe: true, wenn nichts eingetragen wurde
    __no_entry = function () {
        return (!this.__new_email);
    }

    //Funktion: schaut ob nicht erlaubte Zeichen in Adresse
    //Parameter (): keine
    //Rückgabe: true, wenn Mail nur erlaubte Zeichen enthält
    __mail_good = function () {
        //alle Zeichen die nicht erlaubt sind
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return this.__new_email.match(re) != null;
    }
    
    //Funktion: schaut ob Mail bereits existiert
    //Parameter (mail_to_compare): die zu vergleichende Mail
    //Rückgabe: true, wenn Mail noch nicht enthalten
    __email_exist_already = function (mail_to_compare ) {
        for (var key in this.__all_mails){
            if (mail_to_compare==this.__all_mails[key]){
                return true;
            }

        }
        return false;
    }
}
    

