
const clockContainer = document.getElementById("date");

// Gibt das aktuelle Datum aus jedes Mal, wenn die Seite neu geladen wird
window.onload = (event) => {
    let date_obj = new Date();
    let day = date_obj.getDate();
    let month = date_obj.getMonth() +1;
    let year = date_obj.getFullYear();
    if (day <10){
        day = '0'+day;
    }

    if (month <10){
        month = '0'+month;
    }

    let time = day+'.'+month+'.'+year;
    clockContainer.innerHTML = time;
  };