//Erstellt und Verwaltet das Bearbeiten aller Tabellen
class Create_table{

    //Funktion: weist Charakterischtische Tabelleneigenschaften hinzu
    //Parameter (): keine
    //Rückgabe: keine
    constructor ( table_get_by_id , table_entries ) {
        //id der Tabelle, in die ich Einfüge
        this.__table_get_by_id = table_get_by_id;
        //wichtig für die id Vergabe der Zeilen/Spalten
        this.__prefix = this.__table_get_by_id.id + "_";
        //die Einträge, die mit denen Veglichen wid, bzw. die eingetragen werden 
        this.__table_entries = {};
        Object.assign(this.__table_entries, table_entries);
    }

    //Funktion: füllt die Tabellen mit Inhalt, welche die möglichkeit aben, ihre Einträge anzupassen
    //Parameter (): keine
    //Rückgabe: keine
    fill_table = function () {
        //dieses dict durchgehen und für jeden eintrag eine Zeile erstellen
        for (const [KEY, VALUE] of Object.entries(this.__table_entries)) {
        
            let insert_row = this.__insert_row( KEY );

            //dieser Block baut das Grundgerüst der Spalten und füllt gleich den Wert ein 
            let insert_cell1 = insert_row.insertCell(0);
            insert_cell1.id = insert_row.id + "_1";
            insert_cell1.innerHTML = VALUE;

            let insert_cell2 = insert_row.insertCell(1);
            insert_cell2.id = insert_row.id + "_2";

            this.__create_edit_pic( insert_cell2 , VALUE );
            this.__create_delete_pic( insert_cell2 , VALUE );
        
        }      

        return;
    }

    //Funktion: füllt die Tabellen mit Inhalt, welche nicht veränderbar sind
    //Parameter (): keine
    //Rückgabe: keine
    fill_unchangeable_table = function () {
        const VALUE = this.__table_entries.value;
        const DATE = this.__table_entries.date;
        const TIME = this.__table_entries.time;
        const TABLE_ID = this.__table_get_by_id;
        const PREFIX= this.__prefix ;
        VALUE.forEach(function (entry, i) {

            let insert_row = TABLE_ID.insertRow(1);
            insert_row.id = PREFIX + i;
            
            let insert_cell1 = insert_row.insertCell(0);
            insert_cell1.innerHTML = entry;

            let insert_cell2 = insert_row.insertCell(1);
            insert_cell2.innerHTML = DATE[i];

            let insert_cell3 = insert_row.insertCell(1);
            insert_cell3.innerHTML = TIME[i];

        });

        return;  
    }

    //Funktion: fügt hinzu zu der Tabelle welche nicht veränderbar Inhalte hat 
    //Parameter (max_size): die maximale Zeilenanzahl
    //Rückgabe: keine
    update_unchangeable_table = function (max_size) {
        const ROW_COUNT = this.__table_get_by_id.rows.length-1;
        //wenn die maximale Nazahl noch nicht erreicht ist, wird eine Zeile hinzugefügt, ansonsten muss zuerst die älteste Zeile entfenrt werden
        if (ROW_COUNT < max_size ) {
            this.__add_new_row( ROW_COUNT - 1 );
            //einfach nur oben hinzuügen 
            //eine Funktion für das hinzufügen 
        }else {
            this.__delete_oldest_row(max_size -1);
            this.__add_new_row(ROW_COUNT -1);
  
        }

        return;
    }

    //Funktion: verwaltet die verschiedenen Aktionen der zu veränderbaren Tabelle
    //Parameter (which_option): wie die Tabelle verändert werden soll
    //Rückgabe: keine
    update_table = function (which_option) {
        switch ( which_option ) {
            case "delete":
                this.__delete_row ();
                break;
            case "new":
                this.__add_row ();
                break;
            case "change":
                this.__change_row ();
                break;
            default:
                console.log("Fehlerhaftes Senden");
                break;
        }

        return;      
    }

    //Funktion: fügt eine neue Reihe in die nicht anpassbare Tabelle
    //Parameter (row_id): die id der neuen Zeile
    //Rückgabe: keine
    __add_new_row = function ( row_id ) {
        let insert_row = this.__insert_row( row_id );

        let insert_cell1 = insert_row.insertCell(0);
        insert_cell1.innerHTML = this.__table_entries.value;

        let insert_cell2 = insert_row.insertCell(1);
        insert_cell2.innerHTML = this.__table_entries.date;

        let insert_cell3 = insert_row.insertCell(1);
        insert_cell3.innerHTML = this.__table_entries.time;
    }

    //Funktion: löscht eine  Reihe in der  nicht anpassbare Tabelle
    //Parameter (last_index_number): der letzte zuzuweisende Index --> Index starte mit 0
    //Rückgabe: keine
    __delete_oldest_row = function (last_index_number) {
        let row = document.getElementById(this.__prefix + "0");
        row.parentNode.removeChild(row);

        const LAST_NUMBER_INDEX =  last_index_number;
        const NEXT_ROW = 1;

        //durchgehen aller Zeilen und die Zuweisung einer neuen ID
        for ( let i = NEXT_ROW ; i <= LAST_NUMBER_INDEX ; i++  ) {
            let new_number = (i -1);
            new_number = new_number.toString();
            let old_number = (i);
            old_number = old_number.toString();
            document.getElementById( this.__prefix + old_number ).id = this.__prefix + new_number;
            
        }
        return;

    }

    //Funktion: erzeugt das ICON mit der delete Funktin
    //Parameter (insert_cell): spalte, in die man das einfügt
    //Parameter (value): der Wert, welcher in der ersten Spalte angezeigt wird, wird auch als ID genommen
    //Rückgabe: keine
    __create_delete_pic = function( insert_cell , value ) {
        //dieser Blockk kümmert sich um das ERstellen des Bildes mit der Funktion des Löschens einer Email
        let delete_img = document.createElement("INPUT");
        delete_img.src = "../picture/delete.png";
        insert_cell.appendChild(delete_img);
        delete_img.setAttribute( "type" , "image" );
        delete_img.setAttribute('onclick','delete_row( this );'); 
        delete_img.setAttribute( "id" , value);
        return;
    }

    //Funktion: erzeugt das ICON mit der change Funktion
    //Parameter (insert_cell): spalte, in die man das einfügt
    //Parameter (value): der Wert, welcher in der ersten Spalte angezeigt wird, wird auch als ID genommen
    //Rückgabe: keine
    __create_edit_pic = function( insert_cell , value ) {
        //dieser Blockk kümmert sich um das ERstellen des Bildes mit der Funktion des bupdaten einer Email
        let edit_img = document.createElement("INPUT");
        edit_img.src = "../picture/edit.png";
        insert_cell.appendChild(edit_img);
        edit_img.setAttribute( "type" , "image" );
        edit_img.setAttribute('onclick','update_row( this );'); 
        edit_img.setAttribute('class', 'edit_button');
        edit_img.setAttribute( "id" , value);
        return;
    }

    //Funktion: dieser Block greift auf die Tabelle zu und erstellt eine Zeile, die wir im folgenden bearbeiten werden
    //Parameter (key): einen Int wert, welcher für die Nummer des eintrages steht und al id fungiert
    //Rückgabe: das Object der erzeugten Reihe
    __insert_row = function (key) {
        let insert_row = this.__table_get_by_id.insertRow(1);
        insert_row.id = this.__prefix + key;
        return insert_row;
    }

    //Funktion: schaut alle Einträge durch und vergleich sie mit den aus der Schnittstelle
    //Parameter (): keinen
    //Rückgabe: keine
    __change_row = function () {
        for (const [key, value] of Object.entries(this.__table_entries)) {

            const FIELD_ID = this.__prefix + key + "_1";
            let field_value = document.getElementById(FIELD_ID).innerHTML;
            //wenn das ungleiche Paar gefunden wurde, ist sicher, dass das eine zu ändernde Zeile ist
            if ( field_value != value) {
                //hier muss jetzt nur der WErt im html geänder werden und die eine id
                document.getElementById(FIELD_ID).innerHTML = value;
                document.getElementById( field_value ).id = value;
            }

        }

        return;
    }

    //Funktion: fügt eine neue Zeile in die anpassbare Tabelle hinzu
    //Parameter (): keinen
    //Rückgabe: keine
    __add_row = function () {
        const KEYS = Object.keys(this.__table_entries);
        const NEWEST_ENTRY = Math.max(...KEYS);
        const VALUE = this.__table_entries[NEWEST_ENTRY];

        let insert_row = this.__insert_row(NEWEST_ENTRY);

        let insert_cell1 = insert_row.insertCell(0);
        insert_cell1.id = insert_row.id + "_1";
        insert_cell1.innerHTML = VALUE

        let insert_cell2 = insert_row.insertCell(1);
        insert_cell2.id = insert_row.id + "_2";

        this.__create_edit_pic( insert_cell2 , VALUE );
        this.__create_delete_pic( insert_cell2 , VALUE );

        return;
    }

    //Funktion: führt das Löschen einer Zeile aus
    //Parameter (): keinen
    //Rückgabe: keine
    __delete_row = function () {
        for (const [key, value] of Object.entries(this.__table_entries)) {

            const FIELD_ID = this.__prefix + key + "_1";
            let field_value = document.getElementById(FIELD_ID).innerHTML;
            
            //wenn der Wert eines Eintrages ungleich dem Wert der Einträge API ist, wird diese Zeile als die zu lschende identifizert
            if ( field_value != value) {
                let row = document.getElementById(this.__prefix + key);
                row.parentNode.removeChild(row);
                //jetzt muss noch die ID angepasst werdenum eine, alle weiteren Werte müssen um eine Stelle nach unten verschoben werden  
                let row_number = parseInt(key);
                this.__update_row_id (row_number);
                return;
            }

        }

        //wenn man bis hierher gekommen ist, dann ist die letzte die nicht mehr vorhandene und muss somit abgesondert gelöscht werden
        const LAST_ROW_NUMBER = this.__table_get_by_id.rows.length - 2;
        let row = document.getElementById(this.__prefix + LAST_ROW_NUMBER.toString()); 
        row.parentNode.removeChild(row);
        return;
    }

    //Funktion: führt das updaten der ID aus
    //Parameter (row_number): die erste Reihe, deren ID um eins nach unten verschoben werden muss 
    //Rückgabe: keine
    __update_row_id = function (row_number){      
        const LAST_NUMBER_INDEX =  this.__table_get_by_id.rows.length -1;
        const NEXT_ROW = row_number + 1;

        //geht jeden WErt ab dem Startwert durch und zeiht vom ID eins ab
        for ( let i = NEXT_ROW ; i <= LAST_NUMBER_INDEX ; i++  ) {
            let new_number = (i -1);
            new_number = new_number.toString();
            let old_number = (i);
            old_number = old_number.toString();
            document.getElementById( this.__prefix + old_number ).id = this.__prefix + new_number;
            document.getElementById( this.__prefix + old_number + "_1" ).id = this.__prefix + new_number + "_1";
            document.getElementById( this.__prefix + old_number + "_2" ).id = this.__prefix + new_number + "_2"; 
        }

        return;
    }
}
