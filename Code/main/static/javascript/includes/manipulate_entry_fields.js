//Funktion: erlaubt oder verbietet die Eingabe in einem EIngabe-Textfeld 
//Parameter (table_name): name der Tabelle, aus der gezählt wird, wieviele Entrage bereits enthalten sind
//Parameter (inputtext_id): die ID, des Textfeldes
//Parameter (placeholder_text): Text des Platzhalters im Textfeld
//Parameter (max_size): maximale Anzahl der Nummern 
//Rückgabe: keine
disable_rows = function (table_name , inputtext_id , placeholder_text , max_size) {
    //Anzahl der Zeilen
    let row_count = document.getElementById(table_name).rows.length - 1;    
    //Verweis auf das Textfeld, in das man eintragan kann
    const textfield = document.getElementById(inputtext_id);
    //Verweis auf die Buttons
    const hidden_button = document.getElementById('hidden_button');
    const button = document.getElementById('button');
    //es wird entweder das Eintragen in das Textfeld erlaubt oder verboten
    if (row_count >= max_size) {

        button.disabled = true;
        button.style.backgroundColor  = "rgb(204, 204, 204)";
        hidden_button.disabled = true;

        textfield.disabled = true;
        textfield.style.backgroundColor  = "rgb(204, 204, 204)";
        document.getElementsByName('content')[0].placeholder="Maximum erreicht";

    }else{

        button.disabled = false;
        button.style.backgroundColor  = "rgb(255, 255, 255)";
        hidden_button.disabled = false;

        textfield.disabled = false;
        textfield.style.backgroundColor  = "rgb(255, 255, 255)";
        document.getElementsByName('content')[0].placeholder=placeholder_text;

    }
}