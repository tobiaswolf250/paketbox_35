//Wählt Sprachausgabe aus
class Choose_scanner_text {

    //Funktion: bekommt als parameter die Id und entscheide entsprechend der id den text --> Ohne instanzierung 
    //Parameter(this_button_id): ein string, welcher den String der id des Buttons ethält der gedrückt wurde 
    //Rückgabe: zusprechender Text
    static check_id (this_button_id) {
        let choosen_text;
        switch ( this_button_id ) {
            case "Ansage 1": case "option1.1":
                choosen_text = this.text_1();
                break;
            case "Ansage 2": case "option2.1":
                choosen_text = this.text_3();
                break;
            case "Melodie 1": case "melodie_option3.1":
                choosen_text = this.sound_1();
                break;
            case "Melodie 2": case "melodie_option4.1":
                choosen_text = this.sound_2();
                break;
            default:
                choosen_text = "Box bitte einlegen";
                console.log("Fehler in der Auswahl");
                break;
        }
        return choosen_text;
    }

    //Funktion: Gibt einen ausgewählten Text zurück
    //Paramater: keinen
    //Rückgabe: Text der gesprochen wird
    static text_1 () {
        return "Bitte legen sie das Paket in die Box"
    }

    //Funktion: Gibt einen ausgewählten Text zurück
    //Paramater: keinen
    //Rückgabe: Text der gesprochen wird
    static text_2 () {
        return "Das Scannen war erfolgreich. Sie können das Paket nun einlegen"
    }

    //Funktion: Gibt einen ausgewählten Text zurück
    //Paramater: keinen
    //Rückgabe: Text der gesprochen wird
    static text_3 () {
        return "Hervorragend. Das Scannen war von erfolg gegrönt. Sie können das Paket nun einlegen"
    }

    //Funktion: Gibt einen ausgewählten Text zurück
    //Paramater: keinen
    //Rückgabe: Text der gesprochen wird
    static sound_1 () {
        return "/mp3_melody_1";
    }

    //Funktion: Gibt einen ausgewählten Text zurück
    //Paramater: keinen
    //Rückgabe: Text der gesprochen wird:
    static sound_2 () {
        return "/mp3_melody_2";
    }

}