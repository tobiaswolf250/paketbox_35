//Funktion: holt sich Daten aus einer API
//Parameter (url): die URL wo Daten zu finden sind
//Rückgabe: die eingelesenen Daten
const get_json = async url => {
    const response = await fetch(url);
    // prüft ob das holen der Daten von der Quelle funktioniert hat (also keine Errors)
    if(!response.ok) {
      throw new Error(response.statusText);
    }
  
    // Bekommt von Quelle ein JSON
      const data = response.json(); 
    // returnt die Daten
    return data;
}