
//prüft die Eingabe für die Paketnummern auf die gefoderten Gegebenheiten wie z.B: gültige Adresse, vorhanden
class Handle_packenumbers {

    //Funktion: weist die alten und neuen Zeit-Werte zu
    //Parameter (): keine
    //Rückgabe: keine
    constructor() {
        //Funktion: weist die alte Paketnummer zu
        //Parameter (old_packagenumber): die alte Paketnummer
        //Rückgabe: keine
        this.__give_old_packagenumber = function (old_packagenumber) {
            this.__old_packagenumber = old_packagenumber.trim();
        };
        //Funktion: weist die neue Paketnummer zu
        //Parameter (new_packagenumber): die neue Paketnummer
        //Rückgabe: keine
        this.__give_new_packagenumber = function (new_packagenumber) {
            this.__new_packagenumber = new_packagenumber.trim();
        };
        //Funktion: weist alle Paketnummern zu
        //Parameter (all_packagenumbers): alle Paketnummern
        //Rückgabe: keine
        this.__give_all_packagenumbers = function (all_packagenumbers) {
            this.__all_packagenumbers = all_packagenumbers;
        };
        //zum verarbeiten der Message-Banner
        this.__polipop = new Polipop('mypolipop', {
            layout: 'popups',
            insert: 'before',
            pool: 5,
            progressbar: true,
            effect: 'slide',
            theme: 'compact', 
            closer: false
        });
    }

    //Funktion: führt den Ablauf zur Änderung eine ausgwählten Paketnummer aus
    //Parameter (this_row): html-Identifer, der Zeile, in der die zu ändernde  Paketnummer steht
    //Parameter (all_packagenumbers): alle gespeicherten Paketnummern
    //Rückgabe: wenn erfolgreich, dann wird die nachricht, die gesendet werden soll übergeben
    change_packagenumber = function( this_row , all_packagenumbers ) {
        //speichert die alte Paketnummer als Klassenattribut
        this.__give_old_packagenumber(this_row.id);
        //speichert die neue Paketnummer als Klassenattribut aus einem Eingabefenster und beendet die Verarbeitung und gibt eine Meldung aus
        let new_packagenumber = prompt("Bitte hier die Paketnummer eintragen", this.__old_packagenumber); 
        this.__give_new_packagenumber(new_packagenumber);

        if (this.__no_entry()){
            this.__polipop.add({
                content: 'Es wurde keine Paketnummer eingegeben',
                title: 'Info',
                type: 'info'
              });
            return;
        }

        //prüft ob die Paketnummer schon vorhanden ist und gibt diese Meldung aus
        if (this.__the_same_entry ()){
            this.__polipop.add({
                content: 'Paketnummer ist bereits vorhanden',
                title: 'Info',
                type: 'info'
              });
              
            return; 
        }
        //speichert alle Paketnummer in einem Attribut
        this.__give_all_packagenumbers(all_packagenumbers);
        //prüft die Paketnummer und bereitet sie wenn alles passt so vor, dass Sie mit Websocket versendet werden kann
        if ( ! this.__check_new_packagenumber()){
            return;
        }else{
            return this.__prep_for_update_sending();
        }
    }

    //Funktion: führt den Ablauf zur Eingabe einer neuen Paketnummer aus
    //Parameter (all_packagenumbers): alle Paketnummern die bisher eingetragen wurden
    //Rückgabe: die neue Paketnummer, wenn die Eingabe passt, sonst keine Rückgabe
    enter_new_packagenumber = function (all_packagenumbers) {
        //holt den neuen Wert der Paketnummer aus dem Eingabefeld
        let new_packagenumber = document.getElementById("package_number_content").value;
        //speichert die neue Paketnummer als Klassenattribut
        this.__give_new_packagenumber(new_packagenumber);
        //prüft, ob etwas eingetragen wurde
        if (this.__no_entry()){

            this.__polipop.add({
                content: 'Es wurde keine Paketnummer eingegeben',
                title: 'Info',
                type: 'info'
              });
            return;
        }
        //speichert alle Paketnummern in einem Attribut
        this.__give_all_packagenumbers(all_packagenumbers);
        //prüft die Paketnummer und gibt sie zurück, wenn alles passt
        if ( ! this.__check_new_packagenumber()){
            return;
        }else{
            return this.__new_packagenumber;
        }
    }

    //Funktion: führt den Ablauf zum Löschen einer alten Paketnummer aus
    //Parameter (this_row): html-Identifer, der Zeile, in der die zu löschende Paketnummer steht
    //Rückgabe: die zu löschende Paketnummer
    delete_packagenumber = function ( this_row ) {
        //speichert die alte Paketnummer in einem Attribut
        this.__give_old_packagenumber(this_row.id);
        //erwartet die Bestätigung der Löschung
        if ( ! this.__confirm_deletion () ) {
            return;   
        }
        
        this.__polipop.add({
            content: 'Paketnummer ' + this.__old_packagenumber + ' wurde gelöscht',
            title: 'Info',
            type: 'info'
          });
        //speichert die Reihe der zu löschenden Paketnummer in einem Attribut
        // this.__give_chosen_row(which_row);
        //löscht die Paketnummer
        // this.__delete_row (table_name , all_packagenumbers);
        return this.__old_packagenumber; 
    }

    //Funktion: prüft die Paketnummer darauf, ob sie bereits existiert und ob sie nur gültige Zeichen enthält
    //Parameter (): keine
    //Rückgabe: false, wenn die Eingabe nicht passt
    __check_new_packagenumber = function () {
        //boolscher Wert, welcher true ist, wenn Paketnummer valide ist
        let check = false;
        //schaut ob die Paketnummer bereits existiert
        if (this.__packagenumber_exist_already(this.__new_packagenumber)) {
            this.__polipop.add({
                content: 'Paketnummer ' + this.__new_packagenumber + ' ist bereits vorhanden',
                title: 'Info',
                type: 'info'
              });
            return check;
        }

        //schaut ob Paketnummer gültige Zeichen enthält
        if(this.__packagenumber_good()){
            this.__polipop.add({
                content: 'Paketnummer ' + this.__new_packagenumber + ' wurde hinzugefügt',
                title: 'Erfolgreich',
                type: 'success'
            });

            check = true;
        }else{   
            this.__polipop.add({
                content: 'Falsche Eingabe wird nicht gespeichert',
                title: 'Warnung',
                type: 'warning'
            });
        }
        
        return check;
    }
    
    //Funktion: fragt ob Paketnummer gelöscht werden soll
    //Parameter (): keine
    //Rückgabe: die Bestätigung (->true) oder Ablehnung (->false)
    __confirm_deletion = function () {
        return confirm("Wirklich "+ this.__old_packagenumber + " löschen?");   
    }

    //Funktion: verpackt die alte und neue Paketnummer so, dass sie via Websocket zum updaten gesendet werden kann
    //Parameter (): keine
    //Rückgabe: die verpackte Nachricht
    __prep_for_update_sending = function () {
        let values = {old:this.__old_packagenumber, new:this.__new_packagenumber};
        values =  JSON.stringify(values);
        return values;
    }

    //Funktion: prüft ob die alte = neue Paketnummer ist
    //Parameter (): keine
    //Rückgabe: true, wenn Paketnummern gleich sind 
    __the_same_entry = function (){
        return this.__new_packagenumber == this.__old_packagenumber;
    }

    //Funktion: keine Paketnummer eingegeben wurde
    //Parameter (): keine
    //Rückgabe: true, wenn nichts eingetragen wurde
    __no_entry = function () {
        return (!this.__new_packagenumber);
    }

    //Funktion: schaut ob nicht mehr als 40 Zeichen in Paketnummer
    //Parameter (): keine
    //Rückgabe: true, wenn Paketnummer nur erlaubte Zeichen enthält
    __packagenumber_good = function () {
        return ((this.__new_packagenumber.length < 40));
    }

    //Funktion: schaut ob Paketnummer bereits existiert
    //Parameter (Paketnummer): die zu vergleichende Paketnummer
    //Rückgabe: true, wenn Paketnummer noch nicht enthalten
    __packagenumber_exist_already = function (packagenumber_to_compare) {
        for (let key in this.__all_packagenumbers){
            if (packagenumber_to_compare==this.__all_packagenumbers[key]){
                return true;
            }
        }
        return false;
    }
}
    

