import socket

#Klasse stellt die IP-Adresse des Systems fest
class Ip_address():
    
    #Konstruktor
    def __init__(self):

        #IP-Adresse des Systems auslesen
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        self.__ip_address = s.getsockname()[0]

    #Funktion: Die Methode schreibt die IP-Adresse in die Datei "ip_address.json" 
    #Paramter (): keinen
    #Rückgabe: keinen
    def get_ip_address(self):
        #Einlesen der IP-Adresse in die Datei "state.json"
        print(self.__ip_address)
        return self.__ip_address


