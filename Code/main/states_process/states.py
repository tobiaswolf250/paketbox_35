import sys
import os
import json
import traceback



#Klasse liest und schreibt den Zustand der Ein- und Ausgänge in die Datei "state.json"
class States():  

    #Konstruktor
    def __init__(self):
        self.__file = "state"
        #Dateipfad für state.json feststellen
        try:
            sys.path.append(os.path.dirname( __file__ ).split("/Code")[0] + "/Code/main")
            from filefolder_finder import Find_file
        except:
            traceback.print_exc()

        self.path_to_json_file = Find_file().find_filefolder(self.__file)
        
        self.__state_file = self.state_read()

    #Destruktor
    def __del__(self):
        print ("States Object deleted")

    #Methode schreibt die Zustände in die Datei
    #Übergabeparameter: state_dict: Zustände im dict-Format, falls nichs üebrgeben wird ist state_dict = None
    #Rückgabe: keine
    def state_write(self, state_dict = None):
        if state_dict is None:
            for state_safe in self.__state_file:
                self.__state_file[state_safe] = 0
        else:
            for key in state_dict:
                self.__state_file[key] = state_dict[key]
        #Beschreibt alles nochmal neu und nimmt so den alten Wert mit 
        with open(self.path_to_json_file, "w") as states_file:
            json.dump(self.__state_file, states_file)
        return 

    #Methode schreibt die Message so um, dass sie im dict-Format ist
    #Übergabeparameter: msg: Zustand, der sich geändert hat
    #Rückgabe: Nachricht im dict-Format
    def optimzie_mqtt(self , msg):
        state_and_value = str(msg.payload.decode("utf-8"))
        data_to_send=json.loads(state_and_value)
        return data_to_send

    #Methode schreibt die Zustände so um, dass sie im dict-Format sind
    #Übergabeparameter: change_for_js: False: Zustände werden nicht entpackt
    #                                   True: Zustände für Javascript entpacken
    #Rückgabe: alle Zustände
    def state_read(self , change_for_js =True):
        with open(self.path_to_json_file,"r+") as states_file:
            #Speichert das Eingelesene extra ab in einer map-klasse
            states_data = states_file.read()
            #konvertiert zu json File
        if change_for_js:
            states_data = json.loads(states_data)
        return states_data 