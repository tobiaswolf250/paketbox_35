#from sre_parse import State
import sys
import os
import json
import traceback
from states_process.states import States

#Klasse Speaker gibt die Zustände der Ein- und Ausgänge in Textform zurück
class Speaker():

#Methode wandelt die in der Datei state.json gespeicherten Zustände der Ein- und Ausgänge in Text um
#Übergabeparameter: keine
#Rückgabe: - text: Zustand der Ein- und Ausgängen im dict-Format, mit ausformulierten Zustandsbeschreibungen
    def states_to_text(self):

        #Einlesen der aktuellen Zustände
        all_states = States().state_read()

        #Zustand der Paketbox (leer/voll) ausformulieren
        if all_states["Boxload"]:
            boxload = '{"Boxload": "Paketbox voll,", '
        else:
            boxload = '{"Boxload": "Paketbox leer,", '
        
        #Zustand der Paketbox (geöffent/geschlossen) ausformulieren
        if all_states["Boxstate"]:
            boxstate = '"Boxstate": "Paketbox geöffnet,", '
        else:
            boxstate = '"Boxstate": "Paketbox geschlossen,", '

        #Zustand des Türöffners ausformulieren
        if all_states["Door"]:
            door = '"Door": "Türöffner betätigt,", '
        else:
            door = '"Door": "Türöffner nicht betätigt,", '

        #Zustand der Klingel ausformulieren
        if all_states["Bell"]:
            bell = '"Bell": "Klingeltaster betätigt,", '
        else:
            bell = '"Bell": "Klingeltaster nicht betätigt,", '
        
        #Zustand des Fehlertasters ausformulieren
        if all_states["ErrorButton"]:
            error_button = '"ErrorButton": "Fehlertaster betätigt,", '
        else:
            error_button = '"ErrorButton": "Fehlertaster nicht betätigt,", '

        #Zustand des Scanners ausformulieren
        if all_states["Scanner"]:
            scanner = '"Scanner": "Scanner betätigt,", '
        else:
            scanner = '"Scanner": "Scanner nicht betätigt,", '

        #Zustand der Beleuchtung ausformulieren
        if all_states["Lighting"]:
            light = '"Lighting": "Beleuchtung aktiv,", '
        else:
            light = '"Lighting": "Beleuchtung nicht aktiv,", '

        #Zustand der Temperatur ausformulieren
        temp = str(all_states["Temperature"])
        temperature = '"Temperatur": "Temperatur '+ temp + ' Grad Celsius"}'

        #Zusammenfügen des Texts für die Rückgabe
        text = boxload + boxstate + door + bell + error_button + scanner + light + temperature

        return text