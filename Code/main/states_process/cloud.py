import requests
from states_process.states import States
import time
import json
import sys
import os
from filefolder_finder import Find_file

def cloud_login():
    sys.path.append(os.path.dirname(os.path.abspath(__file__)).split("/Code")[0] + "/Code/main")
    

    path_to_json_file = Find_file().find_filefolder("config")

    with open(path_to_json_file,"r+") as states_file:
        #Speichert das Eingelesene extra ab in einer map-klasse
        states_data = states_file.read()
        #konvertiert zu json File

    states_data = json.loads(states_data)

    key = states_data["thingspeak_key"]

    return key

#Klasse sendet die akuellen Zustände der Ein- und Ausgängen an die ThingsSpeak-Cloud
class Cloud():

    #Methode sendet alle 5 Sekunden ein Update der Zustände an die Cloud
    #Übergabeparameter: keine
    #Rückgabe: keine
    def send(self):

        private_key = cloud_login()

        #Zustände aus state.json lesen und in "status_frontend" speichern
        status_fronted = States().state_read()
        message = ""
        
        #Schleife weist jedem Feld der Cloud einen Wert zu
        for count, key in enumerate(status_fronted):

            message += "&field"+str(count +1 ) + "=" + str(status_fronted[key])
        requests.get('https://api.thingspeak.com/update?api_key='+private_key+message)
        return