# Nach Anleitung von https://realpython.com/python-send-email/#option-2-setting-up-a-local-smtp-server

import smtplib, ssl
from check_process.sql.sqlalchemy_api import * 
import sys
import os

#Die Klasse Email versendet Emails an alle in der Datenbank gespeicherten Mail-Adressen
class Email:

    #Variablen werden zugewiesen
    def __init__( self , reason = None ):
        
        #Pfad zur Datei email.json bestimmen
        try:
            sys.path.append(os.path.dirname( __file__ ).split("/Code")[0] + "/Code/main")
            from filefolder_finder import Find_file
        except:
            traceback.print_exc()
            exit()

        #Daten aus email.json einlesen
        with open(Find_file().find_filefolder(file = "email")) as json_file:
            data = json.load(json_file)
        
        self.__reason = reason 
        self.__port = 465  # Für SSL
        self.__smtp_server = "smtp.gmail.com"
        self.__sender_email = data["sender"]
        self.__password = data["password"]
        
        #Inhalt der Mail, wenn der Fehlertaster betätigt wurde
        if (self.__reason == "error_button"):
            self.message = """Subject: iSYSTEM-Zustellfehler\
                \n\nSehr geehrter iSystem-Nutzer, \n\ndie Fehlertaste Ihrer Paketbox wurde aktiviert. \
                \nDies bedeutet, dass es ein Problem in der Zustellung gegeben hat. \
                \nEs ist die Anwesenheit einer Person erforderlich, um das Problem zu beheben.\
                \nBei Problemen wenden Sie sich bitte an unsere Mitarbeiter.\
                \n\nIhr TPS Technologies-Team"""
        
        #Inhalt der mail, wenn das Scannen fehlgeschlagen ist
        elif (self.__reason == "scanner_error"):
            self.message = """Subject: iSYSTEM-Zustallfehler\
                \n\nSehr geehrter iSystem-Nutzer, \n\nder Scanner Ihrer Paketbox hat eine falsche oder keine Nummer eingescannt.\
                \nBei Problemen wenden Sie sich bitte an unsere Mitarbeiter.\
                \n \nIhr TPS Technologies-Team"""
        #Falls kein oder ein anderer Grund der Mail gesendet wird, wird eine Fehlermeldung ausgegeben
        else:
            traceback.print_exc() 
            print ("There is a problem, sending mails")

        #Mailadressen werden aus der DB geholt und in Variable gespeichert
        addresses = Main_sqlalchemy("emailaddress")
        self.receiver_email  = addresses.execute_get_data()
             

    #Methode versendet Emails
    #Übergabeparameter: self (siehe init)
    #Rückgabe: keine
    def send_mail(self):

        try:
            context = ssl.create_default_context()
            with smtplib.SMTP_SSL(self.__smtp_server, self.__port, context=context) as server:
                server.login(self.__sender_email, self.__password)
                server.sendmail(self.__sender_email, self.receiver_email, self.message)
                print("email sent successfully")
        
        except:
            print("Error sending email")
