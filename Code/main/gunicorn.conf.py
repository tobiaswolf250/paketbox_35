import multiprocessing
import json
import traceback
import os
import sys
from ip_address import Ip_address

#IP-Adresse des Systems einlesen
sys_ip = Ip_address()
ip_address = sys_ip.get_ip_address()

bind = ip_address

workers = multiprocessing.cpu_count() * 2 + 1
