# Erweitert von https://www.piddlerintheroot.com/barcode-scanner/
# https://www.raspberrypi.org/forums/viewtopic.php?f=45&t=55100

#Klasse wandelt die Signale des Scanners zu einem verarbeitbarem String um
class Barcode_reader:
    
    #Konstruktor
    def __init__(self):
        self.__CHARMAP_LOWERCASE = {4: 'a', 5: 'b', 6: 'c', 7: 'd', 8: 'e', 9: 'f', 10: 'g', 11: 'h', 12: 'i', 13: 'j', 14: 'k',
                     15: 'l', 16: 'm', 17: 'n', 18: 'o', 19: 'p', 20: 'q', 21: 'r', 22: 's', 23: 't', 24: 'u', 25: 'v',
                     26: 'w', 27: 'x', 28: 'y', 29: 'z', 30: '1', 31: '2', 32: '3', 33: '4', 34: '5', 35: '6', 36: '7',
                     37: '8', 38: '9', 39: '0', 44: ' ', 45: '-', 46: '=', 47: '[', 48: ']', 49: '\\', 51: ';',
                     52: '\'', 53: '~', 54: ',', 55: '.', 56: '/'}
        self.__CHARMAP_UPPERCASE = {4: 'A', 5: 'B', 6: 'C', 7: 'D', 8: 'E', 9: 'F', 10: 'G', 11: 'H', 12: 'I', 13: 'J', 14: 'K',
                            15: 'L', 16: 'M', 17: 'N', 18: 'O', 19: 'P', 20: 'Q', 21: 'R', 22: 'S', 23: 'T', 24: 'U', 25: 'V',
                            26: 'W', 27: 'X', 28: 'Y', 29: 'Z', 30: '!', 31: '@', 32: '#', 33: '$', 34: '%', 35: '^', 36: '&',
                            37: '*', 38: '(', 39: ')', 44: ' ', 45: '_', 46: '+', 47: '{', 48: '}', 49: '|', 51: ':', 52: '"',
                            53: '~', 54: '<', 55: '>', 56: '?'}
        self.__CR_CHAR = 40
        self.__SHIFT_CHAR = 2
        self.__ERROR_CHARACTER = '?'

    #Die Methode wandelt die Signale des Scanners zu einem String um
    #Übergabeparameter: self
    #Rückgabe: barcode_string_output: string mit Inahlt des eingescannten Barcodes
    def barcode_reader(self):
        barcode_string_output = ''
        
        
        CHARMAP = self.__CHARMAP_LOWERCASE
        n = 0
        with open('/dev/hidraw0', 'rb') as fp:

            
            while True:
                
                #Iterieren durch die Elemente des Barcodes
                for char_code in [element for element in fp.read(8) if element > 0]:
                    
                    if char_code == self.__CR_CHAR:
                        # Auf das Ende warten
                        CHARMAP = self.__CHARMAP_LOWERCASE
                        return barcode_string_output
                    if char_code == self.__SHIFT_CHAR:
                        n = n + 1
                        # Zu Großbuchsteben bzw. Register 2 shiften
                        if n == 2:
                            CHARMAP = self.__CHARMAP_UPPERCASE
                    else:
                        #Fehlerverarbeitung
                        barcode_string_output += CHARMAP.get(char_code, self.__ERROR_CHARACTER)
                        #Rücksetzen zu Register 1
                        n = 0
                        CHARMAP = self.__CHARMAP_LOWERCASE
