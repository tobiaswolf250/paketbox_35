#Klasse beinhaltet die Konfigurationsparameter für die Flask-MQTT-Verbindung
class Config_app:
    
    @property
    def config():
        MQTT_BROKER_URL = 'localhost' 
        MQTT_BROKER_PORT = 1883
        MQTT_USERNAME = ''
        MQTT_PASSWORD = '' 
        MQTT_KEEPALIVE = 5
        MQTT_TLS_ENABLED = False  
        return

