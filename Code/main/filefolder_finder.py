import os

#Klasse findet Dateien und den dazugehörigen Dateipfad
class Find_file:

    script_dir = os.path.dirname( __file__ )

    def __init__(self) -> None:
       pass

    #Funktion: findet eine Datei
    #Parameter (name) : den namen der Datei
    #Parameter (folder_name): die Datei bei der gesplittete werden soll
    #Rückgabe: den Pfad zum File hin
    def find_in_exch(self , file_name , folder_name , end):
        new_path = self.script_dir + "/"
        #erstellt Liste mit allen Foldern die jetzt zu sehen sind
        folders = os.listdir(new_path)
        #sucht in Liste nach dem folder exchange
        folder_exists = folder_name in folders
        if not folder_exists:
            print ("Error while serching for folder: ", folders)
            return
        #navigiert zu exchange
        new_path += folder_name + "/"
        #sucht das json file das gsucht wird und gibt den vollen Pfad zurück
        files = os.listdir(new_path)
        file_name += end
        #sucht in Liste nach dem folder exchange
        file_exists =  file_name in files
        if not file_exists:
            print ("Error while searching for file: ", files)
            return
        new_path += file_name 
        return new_path


    #Funktion: Findet die gesuchte Datei
    #Parameter: file: Name der gesuchten Datei
    #Rückgabe: gibt den Pfad zu dem gewünschten File wieder
    def find_filefolder(self , file , end=".json" ):
        if (self.script_dir.find("Code/main") != -1):
            new_path = self.script_dir.split("Code/main")[0]
            new_path += "Code"
            self.script_dir = new_path
            return self.find_in_exch(file , "exchange" , end)
        else:
            print ("Installation error")
        return 


        #Funktion: Findet die gesuchte Datei
    #Parameter: file: Name der gesuchten Datei
    #Rückgabe: gibt den Pfad zu dem gewünschten File wieder
    def find_mp3(self , file):
        if (self.script_dir.find("Code/main") != -1):
            new_path = self.script_dir.split("Code/main")[0]
            new_path += "Code"
            self.script_dir = new_path
            return self.find_in_exch(file , "exchange" , ".mp3")
        else:
            print ("Installation error")
        return 

