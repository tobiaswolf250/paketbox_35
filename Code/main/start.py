import os
import sys

def path_s ():
    return os.path.dirname( __file__ ).split("/Code")[0] + "/Code/main"

os.system("( cd " + path_s() + "; gunicorn --worker-class eventlet -w 1 app:app) & python3 " + path_s() + "/raspberry_pi.py & python3 " +  path_s() + "/backend.py")
