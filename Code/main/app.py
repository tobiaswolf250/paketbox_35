from ast import keyword
from flask import Flask, render_template, request, redirect, Response, send_from_directory , send_file
import paho.mqtt.client as paho
from flask_socketio import SocketIO
import json
from flask_mqtt import Mqtt
import sys
import traceback
import json
import sys
import os

from states_process.cloud import Cloud
from check_process.check_packagenumbers import New_packagenumbers,\
    Changed_packagenumbers, Deleted_packagenumbers
from check_process.check_mail import Emails, Changed_emails, Deleted_emails
from check_process.time_check import Work_with_times

from check_process.sql.sqlalchemy_api import Main_sqlalchemy,\
    Limited_output, Create_csv

from states_process.states import States
from states_process.speaker import Speaker

from filefolder_finder import Find_file

import eventlet

import paho.mqtt.publish as publish


eventlet.monkey_patch

app = Flask(__name__)

app.config.from_pyfile('app_config.py')

sio = SocketIO(app, cors_allowed_origins='*')

mqtt = Mqtt(app)


@app.route('/picture/<path:filename>')
def download_file(filename):
    return send_from_directory( "static/" , filename )

#Funktion: Laden der Home-Seite
#Parameter: keine
#Rückgabe: alles nötige zum erstellen oder navigieren der home.html
@app.route('/home')
def home(): 

    return render_template('home.html')


#Funktion: Es wird eine neue Paketnummer empfangen und wenn die Paketnummer geprüft wurde wird sie in die Datenbank eingefügt
#Parameter: number: Neue Paketnummer
#Rückgabe: keine
@sio.on('new_packagenumber')
def new_number(number):
    #prüft die Paketnummer und fügt diese in die Datenbank ein
    use_packagenumber = New_packagenumbers(number)

    if use_packagenumber.check_new_packagenumber():
        sio.emit('number_confirmation', "new")
        
    else:
        print("There is a problem adding a new number")
    return 

#Funktion: Sendet neue MQTT-Nachricht, wenn die Zustände von der Weboberfläche geändert werden
#Parameter: state: Zustände des Frontends
#Rückgabe: keine
@sio.on('my_states_changes_by_frontend')
def change_state_client(state):
    change_states = States()
    change_states.state_write(state)
    key = state.keys()
    key_mqtt = " "
    for k in key:
        key_mqtt = k 
    state = json.dumps(state)
    #Websocket-Nachricht senden
    sio.emit('my_states_changes_surface', state)
    #MQTT-Nachricht an das Backend versenden
    channel = 'paketbox/backend/' + key_mqtt
    mqtt.publish(channel, state)

    return 

#Funktion: Gibt die aktuellen Zustände aus
#Parameter: keine
#Rückgabe: all_states: alle Zustände im dict-Format
@app.route('/states')
def give_all_states():
    try: 
        states_for_load = States()
        all_states = states_for_load.state_read()
        return all_states
    except:
        traceback.print_exc() 
        print ("There is a problem, sending the states")
        return 

#Funktion: Gibt Text für Spachausgabe zurück
#Parameter: keine
#Rückgabe: Text für Sprachausgabe im dict-Format
@app.route('/states_for_speak')
def give_states_for_speak():
    try: 
        states_to_speak = Speaker()
        states_speak = states_to_speak.states_to_text()
        return states_speak
    except:
        traceback.print_exc() 
        print ("There is a problem, sending the states")
        return 


#Funktion: beim Post Request wird eine Tabelle aller zugestellten Pakete heruntergeladen
#Funktion: beim Get Request wird die Information zur Darstellung der packages.html-Seite bereitgestellt
#Parameter: keine
#Rückgabe: bei einem Fehler eine Textnachricht, sonst alles nötige zum erstellen oder navigieren der packages.html
@app.route('/log', methods=['GET', 'POST'])
def log():
    if request.method == 'POST':
        #speichert, prüft und gibt das csv file aus
        output = Create_csv("log").write_excel_file()

        if output:
            return Response(output, mimetype="text/csv", headers={"Content-Disposition":"attachment,filename=Log.csv"})
        else:
           return "There is a Problem showing you the List "
           
    else:
        
        try:
            return render_template('log.html')
        
        except:
            traceback.print_exc() 
            return 'There was an issue loading the logs'
    
           

#Funktion: beim Post Request wird eine Tabelle aller zugestellten Pakete heruntergeladen
#Funktion: beim Get Request wird die Information zur Darstellung der packages.html Seite bereitgestellt
#Parameter: keine
#Rückgabe: bei einem Fehler eine Textnachricht, sonst alles nötige zum erstellen oder navigieren der packages.html
@app.route('/packages', methods=['POST', 'GET'])
def add_package():
    try: 
        if request.method == 'POST': 
            #Prüfung, ob wirklich der Download-Button betätigt wurde
            status_downloadpackage_button = request.form ['post_identifier'] == 'click_download'

            #speichert, prüft und gibt die CSV-Datei aus
            if status_downloadpackage_button:
                output = Create_csv("package_number_delivered").write_excel_file()
                if output:
                    return Response(output, mimetype="text/csv", headers={"Content-Disposition":"attachment,filename=Packagenumber_delivered.csv"})
                else:
                    return "There is a Problem showing you the List"
        else:
            return render_template('packages.html')
    except:
        traceback.print_exc() 
        return "There was an issue loading the package numbers"

#Funktion: Gibt alle erwarteten Paketnummern zurück
#Parameter keine
#Rückgabe: all_packagenumbers: Alle erwarteten Paketnummern im dict-Format
@app.route('/all_packagenumbers')
def give_all_packagenumbers():
    try: 
        #Datenbankzugriff
        all_packagenumbers = Main_sqlalchemy("package_number").get_change_sql_output()
        return all_packagenumbers 
    except:
        traceback.print_exc() 
        print ("There is a problem, loading the packege numbers")
        return 

#Funktion: löschen der Paketnummer
#Parameter (packagenumber): die zu löschende Paketnummer
#Rückgabe: keine
@sio.on('delete_packagenumber')
def delete_pck_message(packagenumber):
    #prüft die paketnummer und löscht sie aus der Datenbank 
    use_packagenumber_checker = Deleted_packagenumbers(packagenumber)
    if use_packagenumber_checker.packagenumber_delete_checker():
        sio.emit('number_confirmation', "delete")
    else:
        print ("There is a Problem with deleting your packagenumber")
    return 


#Funktion: Ändern einer Paketnummer
#Parameter (packagenumbers): Die alte Paketnummer
#Rückgabe: keine
@sio.on('change_packagenumber')
def change_pck_message(packagenumbers):
    packagenumbers = json.loads(packagenumbers)
    #Prüfen, ob neue Paketnummer gültig ist
    use_packagenumbers_checker = Changed_packagenumbers(packagenumbers) 
    if use_packagenumbers_checker.packagenumber_change_checker():
        sio.emit('number_confirmation', "change") 
    else:
        print ("There is a Problem with changing your mail")
    return 

#Funktion: Gibt die letzten 5 zugestellten Paketnummern zurück
#Parameter keine
#Rückgabe: delivered_numbers: die letzten 5 zugestellten Paketnummern im dict-Format
@app.route('/all_delivered_packagenumbers')
def all_delivered_packagenumbers():
    try: 
        #Zugestellte Paketnummern aus Datenbank auslesen
        delivered_numbers = Limited_output("package_number_delivered" , 5)
        delivered_numbers.get_data_amount(True)
        return delivered_numbers.get_all()

    except:
        traceback.print_exc() 
        print ("There is a problem, loading the packege numbers")
        return 

#Funktion: Laden der Inhalt der Einstellungs-Seite (Mail-Adressen, Nutzungszeiten)
#Parameter: keine
#Rückgabe: bei einem Fehler eine Textnachricht, sonst alles nötige zum Erstellen und Navigieren der settings.html
@app.route('/settings')
def handle_settings():
    try:
        #Mail-Adressen und Nutzungszeit aus der Datenbank lesen
        all_emails = Main_sqlalchemy("emailaddress").execute_get_data( )
        all_times = Main_sqlalchemy().get_worktime( )
        all_times = list(all_times.values())

        sys.path.append(os.path.dirname(os.path.abspath(__file__)).split("/Code")[0] + "/Code/main")
    
        path_to_json_file = Find_file().find_filefolder("speak")

        with open(path_to_json_file,"r+") as states_file:
            #Speichert das Eingelesene extra ab in einer map-klasse
            speak_data = states_file.read()
            #konvertiert zu json File

        speak_data = json.loads(speak_data)

        key = speak_data["choosen_button"]
        return render_template('settings.html', result_time = all_times , all_emails=all_emails, key = key)

    except:
        traceback.print_exc() 
        return "There was an issue loading the data"

#Funktion: Stellt alle gespeicherten Mail-Adressen zur Verfügung
#Parameter kenie 
#Rückgabe: all_mail: Alle gespeicherten Mail-Adressen im dict-Format
@app.route('/all_emails')
def give_all_emails():
    try: 
        all_mail = Main_sqlalchemy("emailaddress").get_change_sql_output(False, "emails_" )
        return all_mail 
    except:
        traceback.print_exc() 
        print ("There is a problem, loading all mails")
        return 

#Funktion: Gibt die letzten 10 Logeinträge zurück
#Parameter keine
#Rückgabe: logs: die letzten 10 Logeinträge im dict-Format
@app.route('/recent_logs')
def recent_logs():
    try: 
        #Neuste Logeinträge aus Datenbank auslesen
        last_logs = Limited_output("log" , 10)
        last_logs.get_data_amount(True)
        return last_logs.get_all()
    except:
        traceback.print_exc() 
        print ("There is a problem, loading the logs")
        return

#Funktion: Gibt die letzten 10 Logeinträge zurück
#Parameter keine
#Rückgabe: logs: die letzten 10 Logeinträge im dict-Format
@app.route('/mp3_melody_1')
def mp3_file_sender_1():
    return send_file( Find_file().find_filefolder("piep_ton" , ".mp3") , as_attachment=True)

#Funktion: Gibt die letzten 10 Logeinträge zurück
#Parameter keine
#Rückgabe: logs: die letzten 10 Logeinträge im dict-Format
@app.route('/mp3_melody_2')
def mp3_file_sender_2():
    return send_file( Find_file().find_filefolder("klingel_ton" , ".mp3") , as_attachment=True)


#Funktion: Fügt neue Nutzungszeit in DB ein bzw. überprüft Eingaben
#Parameter: time: dict mit Start- und Endzeit 
#Rückgabe: Bei Fehler: Fehlernachricht
@sio.on('new_time')
def new_time(time):
    #prüft die Zeit und trägt sie in die Datenbank ein
    start_time = str( time["start_hour"] + ":" +  time["start_minute"])
    end_time = str( time["end_hour"] + ":" +  time["end_minute"])
    use_times = Work_with_times(start_time, end_time)


    if use_times.check_times():
        sio.emit('time_confirmation', time)
        return   
    else:
        return "There is a problem with using your time entrys --> check the class" 


#Funktion: Fügt neue E-Mail-Adresse in die DB ein bzw. überprüft Adresse
#Parameter (email):  neue Mail-Adresse 
#Rückgabe: Bei Fehler: Fehlernachricht
@sio.on('new_mail')
def add_new_mail(email):
    use_emails = Emails(email)

    #Neue Mail-Adresse prüfen
    if use_emails.check_new_email():
        sio.emit('mail_confirmation', "new")
        return      
    else:
        return "There is a problem with adding your email entrys --> check the class" 

#Funktion: Löscht die E-Mail
#Parameter (email): die zu löschende Email  
#Rückgabe: Bei Fehler: Fehlernachricht
@sio.on('delete_mail')
def delete_mail_message(email):
    use_mails_checker = Deleted_emails(email)
    if use_mails_checker.mail_delete_checker():
        sio.emit('mail_confirmation', "delete")
        return
    else:
        print ("There is a Problem with deleting your mail")
        return 

#Funktion: ändert die E-Mail-Adresse
#Parameter (emails): die alte Email 
#Rückgabe: Bei Fehler: Fehlernachricht
@sio.on('change_mail')
def change_mail_message_sio(emails):
    #Alle Mai-Adressen laden
    emails = json.loads(emails)

    #Geänderte Mail-Adresse prüfen
    use_mails_checker = Changed_emails(emails) 
    if use_mails_checker.mail_change_checker():
        sio.emit('mail_confirmation', "change")
        return 
    else:
        print ("There is a Problem with changing your mail")
        return
 
#Funktion: Gibt die letzten 10 Logeinträge über Websocket an das Frontend weiter
#Parameter keine
#Rückgabe: logs: die letzten 10 Logeinträge im dict-Format
def recent_logs_sio():
    try: 
        #Neuste Logeinträge aus Datenbank auslesen
        last_logs = Limited_output("log" , 1)
        last_logs.get_data_amount(True)
        sio.emit('log_confirmation', last_logs.get_all()) 
    except:
        traceback.print_exc() 
        print ("There is a problem, loading the logs")
    return

#Funktion: Gibt die letzten 5 zugestellten Paketnummern an das Frontend weiter
#Parameter keine
#Rückgabe: delivered_numbers: die letzten 5 zugestellten Paketnummern im dict-Format
def all_delivered_packagenumbers_sio():
    try: 
        #Zugestellte Paketnummern aus Datenbank auslesen
        delivered_numbers = Limited_output("package_number_delivered" , 1)
        delivered_numbers.get_data_amount()
        sio.emit('number_delivered_confirmation', delivered_numbers.get_all())
    except:
        traceback.print_exc() 
        print ("There is a problem, loading the packege numbers")
    return 

#Funktion: ändert die E-Mail-Adresse
#Parameter (emails): die alte Email 
#Rückgabe: Bei Fehler: Fehlernachricht
@sio.on('scanner_confirmation_speak')
def change_text(new_text):
    try:
        #text laden
        new_text = json.loads(new_text)
        #text schreiben in das json file
        path_to_json_file = Find_file().find_filefolder("speak")
        file = open(path_to_json_file, "w")
        json.dump(new_text, file)
        file.close()
        sio.emit('change_choosen_scanner', new_text["choosen_button"])
    except:
        traceback.print_exc()

    

#Funktion: Verbindung zum MQTT-Broker aufbauen und MQTT-Channel für das Frontend abonnieren
#Parameter (client): Daten über Client
#Parameter (userdata): Daten über den Absender
#Parameter (msg): Inhalt der Nachricht, die gesendet wurd
#Rückgabe : keine
@mqtt.on_connect()
def handle_connect(client, userdata, flags, rc):
    print("Connected with the MQTT-Broker")
    # Alle Nachrichten unter dem topic "paketbox/frontend/#" werden abboniert
    mqtt.subscribe("paketbox/frontend/#")


#Funktion: Wird ausgeführt, sobald eine neue MQTT-Nachricht erhalten wurde. 
#          Sendet die Änderung über Websockets weiter
#Parameter (client): Daten über Client
#Parameter (userdata): Daten über den Absender
#Parameter (msg): Inhalt der Nachricht, die gesendet wurde
#Rückgabe : keine
@mqtt.on_message()
def handle_mqtt_message(client, userdata, msg):
    #Schreibt erhaltene Nachricht so um, dass sie in das JSON Format passt
    change_states = States() 
    state = change_states.optimzie_mqtt(msg)
    print(state)
    if "log" in state:
        recent_logs_sio()
    elif "package" in state:
        all_delivered_packagenumbers_sio()
    else:
        #Schreibt erhaltene Nachricht so um, dass sie in das JSON Format passt
        # Cloud sendet den Zustand der Ein- und Ausgänge an Thingspeak
        change_states.state_write(state)
        #Websocket-Nachricht, mit den neuen Zuständen
        sio.emit('my_states_changes_surface', json.dumps(state))
        #Nahcricht an die Cloud senden
        status_cloud = Cloud()  
        status_cloud.send()
        
    
#Funktion: Bei Verbindungsabbruch mit dem MQTT-Broker Nachricht ausgeben
#Parameter (client): Daten über Client
#Parameter (userdata): Daten über den Absender
#Parameter (msg): Inhalt der Nachricht, die gesendet wurd
#Rückgabe : keine
@mqtt.on_disconnect()
def handle_disconnect(client, userdata,rc=0):
    print ("Connection with MQTT closed")
    print (str(rc))


#Initialisierung der app.py
if __name__ == 'app':
    #schreibt Werte auf 0
    States().state_write()
    mqtt.init_app(app)
