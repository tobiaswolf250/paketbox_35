import json
import pygame
import os
from filefolder_finder import Find_file

class speaker():

    #Constructor
    #Findet den Pfad zum speak.json file
    def __init__(self):
        self.__file = "speak"
        self.path_to_json_file = Find_file().find_filefolder(self.__file)
        self.__speak_file = self.speak_read()

    #Liest das speak.json file aus
    #Rückgabe: Gibt den Inhalt des speak.json file ais Dictionary zurück
    def speak_read(self):
        with open(self.path_to_json_file,"r+") as speak_file:
            #Speichert das Eingelesene extra ab in einer map-klasse
            speak_data = speak_file.read()    
        speak_data = json.loads(speak_data)
        return speak_data

    #Tonausgabe am Raspberry Pi
    def speak(self):
        #Wird ausgeführt wenn im speak.json file das key value Paar choosen_buttton: Melodie 1 existiert 
        if self.__speak_file["choosen_button"] == "Melodie 1": 
            pygame.mixer.init()
            #Gibt den Inhalt des piep_ton.mp3 file wieder
            pygame.mixer.music.load(Find_file().find_filefolder("piep_ton", ".mp3"))
            pygame.mixer.music.play()
        #Wird ausgeführt wenn im speak.json file das key value Paar "choosen_buttton": "Melodie 2" existiert 
        elif self.__speak_file["choosen_button"] == "Melodie 2":
            pygame.mixer.init()
            #Gibt den Inhalt des klingel_ton.mp3 file wieder
            pygame.mixer.music.load(Find_file().find_filefolder("klingel_ton", ".mp3"))
            pygame.mixer.music.play()
        else:
            #Gibt den Inhalt des speak.json file des key "text" als Tonausgabe aus
            os.system('espeak -vde "{}"'.format(self.__speak_file["text"]))

if __name__ == "__main__":
    speaker().speak()
    
