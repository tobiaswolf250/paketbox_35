# Orientiert an: https://github.com/roppert/mosquitto-python-example


from check_process.worktime import Check_time
import time
from check_process.sql.sqlalchemy_api import Main_sqlalchemy  
from sqlalchemy import null, true
import threading 
import paho.mqtt.client as paho
import paho.mqtt.publish as publish
import json
from send_email import Email
from raspi_speak import speaker


# Klasse dient der Kommunikation des Raspberry Pi mit dem Webserver und dem Rsp32 über mqtt

class Backend():

    # Konstruktor
    def __int__(self):
        print("States created")

    # Verbindung zum mqtt-Broker wird hergestellt
    # Die Methode ruft bei einer Nachricht über mqtt die entsprechende Methode auf
    # Rückgabe: keine
    def connection(self):
        client = paho.Client()
        
        # Bei einer Nachricht wird die zuständige Methode aufgerufen
        client.message_callback_add("paketbox/backend/Boxstate", self.__state_boxstate)   
        client.message_callback_add("paketbox/backend/Door", self.__state_door)     
        client.message_callback_add("paketbox/backend/Bell", self.__state_bell)
        client.message_callback_add("paketbox/backend/Errorbutton", self.__state_error_button)
        client.message_callback_add("paketbox/backend/Scanner_number", self.__scanner_number)
        
        
        # Verbindung zum MQTT-Broker wird hergestellt        
        client.connect("127.0.0.1", 1883, 60)
        # Alle Nachrichten unter dem topic "paketbox" werden abboniert
        client.subscribe("paketbox/backend/#", 0)

        client.loop_forever()
    

    # Die Methode sendet Nachrichten über MQTT 
    # Übergabeparameter: - message: Inhalt der Nachricht die verschickt wird
    #                    - topic: Topic unter dem die Nachricht gesendet wird    
    # Rückgabe: keine   
    def __publish(self, message, topic):
        host = "localhost"
       
        publish.single( topic=topic, payload = message, hostname=host)
        #publish.single( topic="paketbox/errorbutton", payload = msgs, hostname=host)
        print("Just published a message!")


    # Die Methode eröffnet einen neuen Thread um den Zustand der Paketbox zu verarbeiten
    # Übergabeparameter: - message: Inhalt der Nachricht die verschickt wird
    # Rückgabe: keine
    def __state_boxstate(self, client, userdata, message):
        threading.Thread(target=self.__boxstate, args=(message,)).start()
        
    # Die Methode eröffnet einen neuen Thread um den Zustand der Türe zu verarbeiten
    # Übergabeparameter: - message: Inhalt der Nachricht die verschickt wird
    # Rückgabe: keine
    def __state_door(self, client, userdata, message):    
         threading.Thread(target=self.__doorstate, args=(message,)).start()

    # Die Methode eröffnet einen neuen Thread um den Zustand der Klingel zu verarbeiten
    # Übergabeparameter: - message: Inhalt der Nachricht die verschickt wird
    # Rückgabe: keine
    def __state_bell(self, client, userdata, message): 
        # Thread wird eröffnet, wenn der Aufruf innerhalb der Nutzungszeit stattfindet
        threading.Thread(target=self.__bell, args=(message,)).start()

    # Die Methode eröffnet einen neuen Thread um den Zustand des Fehlertasters zu verarbeiten
    # Übergabeparameter: - message: Inhalt der Nachricht die verschickt wird
    # Rückgabe: keine
    def __state_error_button(self, client, userdata, message):
        if Check_time().worktime():
            threading.Thread(target=self.__error_button, args=(message,)).start()

    # Die Methode eröffnet einen neuen Thread um die gescannte Paketnummer zu verarbeiten
    # Übergabeparameter: - message: Inhalt der Nachricht die verschickt wird
    # Rückgabe: keine
    def __scanner_number(self, client, userdata, message):
        # Thread wird eröffnet, wenn der Aufruf innerhalb der Nutzungszeit stattfindet
        if Check_time().worktime():    
            threading.Thread(target=self.__scanner, args=(message,)).start()


    # Die Methode setzt den Zustand der Tür und erzeugt einen Logeintrag in der Datenbank
    # Übergabeparameter: - message: Inhalt der MQTT-Nachricht
    # Rückgabe: keine
    def __doorstate(self, msg):
        # Logeintrag wird erzeugt und Zustand der Türe wird über MQTT geteilt
        threading.Thread(target=self.__set_state, \
            args=('{"Door": 1}', "paketbox/frontend/door", "Türe geöffnet", 0)).start()
        # Zustand der Türe wird nach 20s wieder auf geschlossen zurückgesetzt
        threading.Thread(target=self.__set_state, args=('{"Door": 0}', "paketbox/frontend/door", None, 20)).start()


    # Die Methode setzt den Zustand der Paketbox und erzeugt einen Logeintrag in der Datenbank
    # Übergabeparameter: - message: Inhalt der MQTT-Nachricht
    # Rückgabe: keine
    def __boxstate (self, msg):
        # Logeintrag wird erzeugt und Zustand der Paketboxtüre wird auf geöffnet gesetzt und über MQTT geteilt
        threading.Thread(target=self.__set_state, \
            args=('{"Boxstate": 1}', "paketbox/frontend/boxstate", "Paketbox geöffnet", 0)).start()
        # Zustand des Füllstandes der Paketbox zurücksetzen über MQTT teilen
        threading.Thread(target=self.__set_state, args=('{"Boxload": 0}', "paketbox/frontend/boxload", None, 0)).start()
        # Timer für das Zurücksetzen wird gestartet
        threading.Thread(target=self.__set_state, args=('{"Boxstate": 0}', "paketbox/frontend/boxstate", None, 5)).start()
        

    # Die Methode teilt eine Nachricht über MQTT und legt bei bedarf einen Logeintrag zum jeweiligen Thema an
    # Übergabeparameter:    - msg: Inhalt der nachricht, die über MQTT verschickt werden soll
    #                       - topic: Topic der MQTT-Nachricht
    #                       - log: Inhalt des Logeintrags, falls keiner erzeugt werden soll ist dieser None
    #                       - time_wait: Zeit die gewartet werden soll, bevor eine MQTT-Nachricht verschickt 
    #                                    und ein Logeintrag gespeichert wird
    # Rückgabe: keine
    def __set_state(self, msg, topic, log, time_wait):
        time.sleep(time_wait)

        # MQTT-Nachricht verschicken
        self.__publish(msg, topic)

        if log:
            # Logeintrag wird in Db gespeichert
            Main_sqlalchemy("log").insert_data(log)
            Main_sqlalchemy("log").execute_get_data(log)
            self.__publish(json.dumps({"log": "new"}), "paketbox/frontend/new_log")
            print("Log entry saved successfully")


    # Die Methode setzt den Zustand der Klingel und erzeugt einen Logeintrag in der Datenbank.
    # Es wird eine neue MQTT-Nachricht gesendet, dass das Licht angehen soll, 
    # nach 60s wird der Lichtzustand wieder zurückgesetzt
    # Übergabeparameter: - message: Inhalt der MQTT-Nachricht
    # Rückgabe: keine
    def __bell(self, msg):
        # Logeintrag, dass Klingel betätigt wurde
        #data.insert_data(db.Log, "bell_button pressed")
        threading.Thread(target=self.__set_state, \
            args=(json.dumps({"Bell": 1}), "paketbox/frontend/bell", "Klingel aktiviert", 0)).start()

        # Nach 20s wird der Zustand der Klingel wieder zurückgesetzt
        threading.Thread(target=self.__set_state, \
            args=(json.dumps({"Bell": 0}), "paketbox/frontend/bell", None, 20)).start()

        # Die Nachricht, dass die Türe geöffnet werden soll wird gesendet
        threading.Thread(target=self.__set_state, \
            args=(json.dumps({"Door": 1}), "paketbox/frontend/door", "Türe geöffnet", 5)).start()
        # Nach 20s wird der Zustand der Klingel wieder zurückgesetzt
        threading.Thread(target=self.__set_state, \
            args=(json.dumps({"Door": 0}), "paketbox/frontend/door", None, 20)).start()
        
        # Die Nachricht, dass das Licht angeschaltet werden soll wird gesendet
        threading.Thread(target=self.__set_state, \
            args=(json.dumps({"Lighting": 1}), "paketbox/frontend/light", "Beleuchtung aktiviert", 0)).start()

        # Nach 60s wird der Zustand des Lichts wieder zurückgesetzt
        threading.Thread(target=self.__set_state, \
            args=(json.dumps({"Lighting": 0}), "paketbox/frontend/light", None, 30)).start()


    # Die Methode setzt den Zustand des Fehlertasters und erzeugt einen Logeintrag in der Datenbank.
    # Es wird zusätzlich eine Email versendet
    # Übergabeparameter: - message: Inhalt der MQTT-Nachricht 
    # Rückgabe: keine  
    def __error_button(self, msg):
        # Zustand des Fehlertasters im Log eintragen und über MQTT teilen
        threading.Thread(target=self.__set_state, \
            args=('{"ErrorButton": 1}', "paketbox/frontend/errorbutton", "Fehlertaste betätigt", 0)).start()
        # Nach 20s wird der Zustand des Fehlertasters wieder zurückgesetzt 
        # und Logeintrag wird erzeugt und in der Datenbank gespeichert
        threading.Thread(target=self.__set_state, \
            args=('{"ErrorButton": 0}', "paketbox/frontend/errorbutton", None, 20)).start()
          
        # Email wird versendet
        mail = Email("error_button")
        threading.Thread(target=mail.send_mail(), args=()).start()

    # Die Methode setzt den Zustand des Scanners und erzeugt einen Logeintrag in der Datenbank.
    # Es wird zusätzlich eine Email versendet
    # Übergabeparameter: - message: Inhalt der MQTT-Nachricht 
    # Rückgabe: keine
    def __scanner(self, msg):
        package_number = str(msg.payload.decode("utf-8"))
        # Zustand des Scanners im Log eintragen und über MQTT teilen
        threading.Thread(target=self.__set_state, \
            args=('{"Scanner": 1}', "paketbox/frontend/scanner", "Scanner aktiviert", 0)).start()
        # Zustand des Scannners nach 20 zurücksetzen
        threading.Thread(target=self.__set_state, \
            args=('{"Scanner": 0}', "paketbox/frontend/scanner", None, 20)).start()

        # Überprüfen ob Paketnummer in Datenbank eingetragen ist und nur aus Zahlen besteht
        if Main_sqlalchemy("package_number").find_data(package_number):
            
            # Paketbox wird geöffnet
            self.__boxstate(package_number)
            
            # Zustand des Füllstandes der Paketbox auf voll setzen über MQTT teilen
            threading.Thread(target=self.__set_state, \
                args=('{"Boxload": 1}', "paketbox/frontend/boxload", None, 0)).start()
            

            # Paketnummer wird in Db mit empfangenen Paketnummern eingefügt
            Main_sqlalchemy("package_number_delivered").insert_data(package_number)
            # Paketnummer wird aus Db mit erwarteten Paketen entfernt
            Main_sqlalchemy("package_number").delete_data(package_number)

            # Änderung der Paketboxtabellen an das Frontend schicken
            threading.Thread(target=self.__set_state, \
                args=(json.dumps({"package": "new"}), "paketbox/frontend/boxload", None, 0)).start()

            threading.Thread(target=speaker().speak()).start()
        
        else:
            # Email versenden
            mail = Email("scanner_error")
            threading.Thread(target=mail.send_mail(), args=()).start()
            
         
#Aufruf der Klasse "Backend"
if __name__ == '__main__':
    backend = Backend()
    backend.connection()



