from check_process.sql.sqlalchemy_api import Main_sqlalchemy
import traceback

#Klasse zum Bearbeiten der Öffnungszeiten der Paketbox
class Work_with_times:

    #Konstruktor
    def __init__(self, start_time = None , end_time = None):
        #dict für die Zeiten aus dem Frontend
        self.__new_times = {}
        #dict für die zeiten aus der db
        self.__timecheck = {}
        #Zeiten ab wann Paketbox offen ist
        self.__new_times.update({ "start_hour": start_time[:2]})
        self.__new_times.update({ "start_minute": start_time[3:5]})
        #Zeiten ab wann Paketbox geschlossen ist
        self.__new_times.update({ "end_hour": end_time[:2]})
        self.__new_times.update({ "end_minute": end_time[3:5]})

    #Funktion: geht den Ablauf der Zeitprüfung durch 
    #Params: sich selbst
    #return: True, wenn alles nötige abgearbeitet ist
    def check_times(self):
        try: 
            #bekommt Info der Eingetragenen Zeiten aus der DB
            self.__db_entry_times()
            #geht durch und schaut, ob ein Eintrag None ist und fügt in diesem Fall den Wert aus der DB ein und ändert eintrag vom dict
            self.__complete_times_dict()
            #schaut ob zeiten gleich sind, wenn, dann soll bearbeitung abgebrochen werden und diese Zeiten sollen eingetragen werden in die DB
            self.__check_timeisanumber()
            if self.__times_equal():
                return True
            
            #prüft die Reihenfolge der eingetragenen Zeiten und --> vgl von start und end
            if not self.__times_right_pos ():
                #ändert die Reihenfolge, da die Endzeit kleiner als die Startzeit ist
                self.__swap_times()         
            
            #trägt neue Zahlen in DB ein
            self.__change_sqlentry_time()

            return True
        except:
            traceback.print_exc() 
            return False

    #Funktion:  trägt die neuen Zeiten in die DB ein
    #Params: sich selbst
    #return: keine 
    def __change_sqlentry_time(self):
        #ändert die Zeiten, wo die Paketbox offen ist      
        
        Main_sqlalchemy("working_time").update_worktime(self.__timecheck  ,self.__new_times)
        return 

    #Funktion: wechselt Position der Eingegeben Zeiten
    #Params: sich selbst
    #return: keine  
    def __swap_times(self):
        #ändert die Zeiten, da der Wert nicht richtig eingegeben wurde
        new_start_h = self.__new_times["end_hour"]
        new_start_m = self.__new_times["end_minute"]
        self.__new_times["end_hour"] = self.__new_times["start_hour"]
        self.__new_times["end_minute"] = self.__new_times["start_minute"]
        self.__new_times["start_hour"] = new_start_h
        self.__new_times["start_minute"] = new_start_m 
        return 

    #Funktion: prüft die Reihenfolge der eingetragenen Zeiten
    #Params: sich selbst
    #return: False, wenn Reihenfolge nicht passt
    def __times_right_pos(self):
        #geht die Bedingungen durch, dass die Zeiten in der richtigen Reihenfolge eingegeben wurden
        if int(self.__new_times["end_hour"]) < int(self.__new_times["start_hour"]):
            return False
        if self.__new_times["end_hour"] == self.__new_times["start_hour"] and \
           int(self.__new_times["end_minute"]) < int(self.__new_times["start_minute"]):
            return False
        return True

    #Funktion:  schaut, ob gesamte Eingabe in das Schema einer Zahl und das Schema Stunden/ Minuten passt
    #Params: sich selbst
    #return: True, wenn alle gleich sind
    def __times_equal(self):
        if self.__new_times["end_hour"] == self.__new_times["start_hour"] and\
           self.__new_times["end_minute"] == self.__new_times["start_minute"]:
            return True
        else:
            return False

    #Funktion:  schaut, ob gesamte Eingabe in das Schema einer Zahl und das Schema Stunden/ Minuten passt
    #Params: sich selbst
    #return: True, wenn alles nötige abgearbeitet ist
    def __check_timeisanumber(self):
        for key in self.__new_times:
            #wenn ein Eintrag nicht den anforderungen entspricht, wird der alte Wert eingetragen
            if self.__single_number_check(self.__new_times[key] , key) == False:
                self.__new_times[key] = self.__timecheck[key]

    #Funktion:  schaut, jede einzelne Eingabe, ob sie in das Schema einer Zahl und das Schema Stunden/ Minuten passt
    #Params: sich selbst
    #return: True, wenn alles nötige abgearbeitet ist
    def __single_number_check(self ,entry,  key):
        if entry.isdecimal():

            #schauen, ob Eingabe dem Format Stunden oder Minuten entspricht
            minute_or_hour = key.find("hour")

            #Zahl für das Zeitformat
            number = 0
            if  minute_or_hour >= 0:
                #prüft, ob Eingabe dem Stunden FOrmat entspricht
                number = 23
            else:   
                minute_or_hour = key.find("minute")
                if minute_or_hour >= 0:
                    number = 59
                #prüft, ob Eingabe dem Stunden FOrmat entspricht   
            if number == 0:
                return False
            else:
                return self.__check_time_format(int (entry) , number)
                
        else:
            return False

    #Funktion:  schaut, ob  Zahö in das Schema Stunden/ Minuten passt
    #Params (self): sich selbst ;  den Eintrag aus dem dict, den man prüfen muss ; die Zahl, entsprechend dem Zahlenformat
    #Params (time_entry): den Eintrag aus dem dict, den man prüfen muss 
    #Params (max_number): die Zahl, entsprechend dem Zahlenformat
    #return: True, wenn alles nötige abgearbeitet ist
    def __check_time_format(self, time_entry , max_number):
        if time_entry < 0 or time_entry > max_number:
            return False
        else:
            return True 

    #Funktion: fügt in bearbeitungsdict der neuen Zahlen die Zahl aus der DB ein
    #Params (self): sich selbst
    #return: True, wenn alles nötige abgearbeitet ist
    def __complete_times_dict(self):
        for key in self.__new_times:
            if self.__new_times[key] == None:
                self.__new_times[key] = self.__timecheck[key]
        return 


    #Funktion: erzeugt ein dict mit den Daten aus der DB
    #Params (self): sich selbst
    #return: nichts
    def __db_entry_times(self):
        #DB-Verbindung und SQL Befehl
        self.__timecheck = Main_sqlalchemy("working_time").get_worktime()
        return 
