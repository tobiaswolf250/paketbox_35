import json
import traceback
from check_process.sql.sqlalchemy_api import Main_sqlalchemy


#Klasse verwaltet die Paketnummern
class New_packagenumbers():

    #Funktion: vergleicht Inhalt der Datenbank mit dem Wert, der Übergeben wird
    #Parameter (_packagenumber): die aktuell, eingegebene Paketnummer  
    #Rückgabe : True wenn Nummer noch nicht vorhanden ist
    def __init__(self , new_package_number) -> None:
        self._packagenumber = new_package_number
        self.__delete_space()
        return  

    #Funktion: vergleicht Inhalt der Datenabnk mit dem Wert, der Übergeben wird
    #Parameter (_packagenumber): die aktuell, eingegebene Paketnummer  
    #Rückgabe : True wenn Nummer noch nicht vorhanden ist
    def __delete_space(self):
        self._packagenumber = self._packagenumber.lstrip() 
        self._packagenumber = self._packagenumber.rstrip() 
        return 

    #Funktion: vergleicht Inhalt der Datenabnk mit dem Wert, der Übergeben wird
    #Parameter (_packagenumber): die aktuell, eingegebene Paketnummer  
    #Rückgabe : True wenn Nummer noch nicht vorhanden ist
    def check_new_packagenumber(self):
        try:
            if not self._packagenumber_check():
                return True
            self.__sqlentry_package()
            return True
        except:
            traceback.print_exc() 
            return False

    #Funktion: vergleicht Inhalt der Datenabnk mit dem Wert, der Übergeben wird
    #Parameter (_packagenumber): die aktuell, eingegebene Paketnummer  
    #Rückgabe : True wenn Nummer noch nicht vorhanden ist
    def __sqlentry_package(self):
        #führt SQL-Befehl aus: Fügt Daten in die DB
        Main_sqlalchemy("package_number").insert_data( self._packagenumber  )
        return 

    #Funktion: vergleicht Inhalt der Datenabnk mit dem Wert, der Übergeben wird
    #Parameter (_packagenumber): die aktuell, eingegebene Paketnummer  
    #Rückgabe : True wenn Nummer noch nicht vorhanden ist
    def _packagenumber_check(self):
        return self._check_packagenumber() and self._check_package_compare()

    #Funktion: vergleicht Inhalt der Datenabnk mit dem Wert, der Übergeben wird
    #Parameter (pass_other_number): die Mgk eine eigens eingegeben Paketnummer zu vergleichen
    #Rückgabe : True wenn Nummer noch nicht vorhanden ist
    def _check_package_compare(self, pass_other_number = None):
        packagenumber_to_check = " "
        if pass_other_number == None:
            packagenumber_to_check = self._packagenumber
        else:
            packagenumber_to_check = pass_other_number
        print ("68: " , packagenumber_to_check)
        all_packagenumbers = Main_sqlalchemy("package_number").execute_get_data()
        #geht Inhalt aus Datenbank durch
        for entry in all_packagenumbers:
            if entry == packagenumber_to_check:
                return False
        return True

    #Funktion: überprüft ob eingegebene Paketnummer anforderungen entspricht
    #Parameter (_packagenumber): aktuell eingetragene Paketnummer 
    #Rückgabe: True, wenn Nummer valide ist
    def _check_packagenumber(self , pass_other_number = None ):
        packagenumber_to_check = " "
        if pass_other_number == None:
            packagenumber_to_check = self._packagenumber
        else:
            packagenumber_to_check = pass_other_number
        #überprüft Gültigkeit der Nummer
        if len(packagenumber_to_check) < 40: 
                return True
        else:
                return False


#Klasse verarbeitet die geänderten Mail-Adressen
class Changed_packagenumbers(New_packagenumbers):

    #Funktion: führt die Email-Validierung entsprechend der Mail_change anforderung durch 
    #Parameter: erhält die E-Mail 
    #Rückgabe: Validierung erfolgereich = True
    def __init__(self, packagenumbers):
        self._old_packagenumber = packagenumbers["old"]
        self.__new_packagenumber = packagenumbers["new"]
        super().__init__(self.__new_packagenumber)
        self.__delete_space()

    #Funktion: führt die Email-Validierung entsrpehcn der Mail_change anforderung durch 
    #Parameter: erhält die E-Mail 
    #Rückgabe: Validierung erfolgereich = True
    def __delete_space(self):
        self._old_packagenumber = self._old_packagenumber.lstrip() 
        self._old_packagenumber = self._old_packagenumber.rstrip()
        return    

    #Funktion: vergleicht Inhalt der Datenabnk mit dem Wert, der Übergeben wird
    #Parameter (_packagenumber): die aktuell, eingegebene Paketnummer  
    #Rückgabe : True wenn Nummer noch nicht vorhanden ist
    def packagenumber_change_checker(self):
        try:
            if not self.__check_both_numbers():
                return True
            self.__sql_update()
            return True
        except:
            traceback.print_exc() 
            return False

    #Funktion: Prüft ob alte und neue Paketnummer valide und vorhanden ist
    #Parameter: keine 
    #Rückgabe: Validierung erfolgereich = True, sonst False
    def __check_both_numbers(self):
        return super()._packagenumber_check() and self._old_number_check()

    #Funktion: Prüft ob alte Paketnummer valide und vorhanden ist
    #Parameter: keine 
    #Rückgabe: Validierung erfolgereich = True, sonst False
    def _old_number_check(self):
        return not(super()._check_package_compare(self._old_packagenumber)) and super()._check_packagenumber(self._old_packagenumber)

    #Funktion: fügt in db die neue mail ein 
    #Parameter (self): enthält sich selbst
    #Rückgabe: keinen
    def __sql_update(self):
        #führt SQL-Befehl aus 
        Main_sqlalchemy("package_number").update_value( self._old_packagenumber , self._packagenumber )
        return
 
#Klasse verarbeitet die glöschten Mail-Adressen
class Deleted_packagenumbers(New_packagenumbers):

    #Konstruktor
    def __init__(self, old_package_number):
        super().__init__(old_package_number)

    #Funktion: vergleicht Inhalt der Datenabnk mit dem Wert, der Übergeben wird
    #Parameter (_packagenumber): die aktuell, eingegebene Paketnummer  
    #Rückgabe : True wenn Nummer noch nicht vorhanden ist
    def packagenumber_delete_checker(self):
        try:
            if not self._old_number_check():
                return True
            self.__sql_delete()
            return True
        except:
            traceback.print_exc() 
            return False


    #Funktion: Prüft ob alte Paketnummer valide und vorhanden ist
    #Parameter: keine 
    #Rückgabe: Validierung erfolgereich = True, sonst False
    def _old_number_check(self):
        return not(super()._check_package_compare(self._packagenumber)) and super()._check_packagenumber(self._packagenumber)

    #Funktion: löscht in db Paketnummer 
    #Parameter (self): enthält sich selbst
    #Rückgabe: keinen
    def __sql_delete(self):
        Main_sqlalchemy("package_number").delete_data(self._packagenumber)
        return

