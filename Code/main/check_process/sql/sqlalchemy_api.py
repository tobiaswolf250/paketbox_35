from shutil import which
from check_process.sql.database_connection import Db_Connection
import check_process.sql.database02 as db
import json
import datetime
import traceback
import io
import csv

#Klasse für zum managen von verschiedenen Datenbankanfragen/Zugriffen
class Main_sqlalchemy (Db_Connection) :

             
    #Konstruktor
    def __init__( self , which_database = None ) -> None:
        self.__db_name = " "
        self.attribute = " "
        self.line = []
        self.__which_database = which_database 
        if (self.__which_database == "package_number"):
            self.__db_name = db.Package_Number
            self.attribute = self.__db_name.number
        
        elif (self.__which_database == "emailaddress"):
            self.__db_name = db.Emailaddress
            self.attribute = self.__db_name.address
        
        elif (self.__which_database == "log"):
            self.__db_name = db.Log
            self.attribute = self.__db_name.log
            self.line = [('Log '), ('Date '), ('Time')]
        
        elif (self.__which_database == "package_number_delivered"):
            self.__db_name = db.Package_Number_Delivered
            self.attribute = self.__db_name.number_delivered
            self.line = [("number_delivered "), ("Date "), ("Time")]

        super().__init__()     

    #Methode liefert die Nutzungszeit aus der Datenbank                  
    #Übergabeparameter: sich selbst
    #Rückgabe: result_dict: Nutzungszeit im dict_Format
    def get_worktime(self):
        output = super().db_get_worktime_data()
        result_dict = {}
        result_dict.update ({"start_hour" : output [0][0] })
        result_dict.update ({"start_minute" : output [0][1] })
        result_dict.update ({"end_hour" : output [0][2] })
        result_dict.update ({"end_minute" : output [0][3] })
        return result_dict
    
    #Methode ruft Methode zum Ändern der Nutzungszeit auf                 
    #Übergabeparameter: old_time: alte Nutzungszeit
    #                   new_time: neue Nutzungszeit
    #Rückgabe: keine
    def update_worktime( self , old_time , new_time):
        super().db_update_worktime_data( old_time , new_time )

    #Methode ruft Methode zum Ändern der Paketnummer auf                 
    #Übergabeparameter: old_time: alte Paketnummer
    #                   new_time: neue Paketnummer
    #Rückgabe: keine
    def update_value( self , old_number , new_number ):
        super().db_update_data( self.__which_database ,old_number , new_number )

    #Methode dient dem Einfügen von neuen Einträgen in die Datenbank                  
    #Übergabeparameter: data: daten die eingefügt werden sollen
    #Rückgabe: result_dict: keine
    def insert_data (self , data):
        new_entry = " "
        if self.__which_database == "package_number":
            new_entry = self.__db_name( number = data )

        elif self.__which_database == "emailaddress":
            new_entry = self.__db_name( address = data )

        elif self.__which_database == "log":
            new_entry = self.__db_name( log = data )
        
        elif self.__which_database == "package_number_delivered":
            new_entry = self.__db_name( number_delivered = data )

        super().db_insert_data( new_entry )

        return

    #Funktionen: führt einen Select * From ... der jeweiligen Tabelle aus  
    #Parameter (self): sich selbst
    #Parameter (second): wenn dies True ist, dann wird der timestamp der Tabelle geholt
    #Rückgabe: Ein Array der Daten, die geholte werden sollen
    def execute_get_data( self , timestamp = False ):
        #aufruf der Funktion in der SQL-ALchemy, speichern in variable
        data_array = []
        timestamp_array = []
        
        #Falls Daten aus der Tabelle package_number benötigt werden
        if self.__which_database == "package_number":
            output = super().db_get_data_desc( self.__db_name )
            for instance in output:
                data_array.append (instance.number) 

        #Falls Daten aus der Tabelle package_number_delivered benötigt werden
        if self.__which_database == "package_number_delivered":
            output = super().db_get_data_desc( self.__db_name )
            
            for instance in output:
                data_array.append (instance.number_delivered)

        #Falls Daten aus der Tabelle emailaddress benötigt werden
        if self.__which_database == "emailaddress":
            output = super().db_get_data_desc( self.__db_name )
            for instance in output:
                data_array.append (instance.address)    

        #Falls Daten aus der Tabelle log benötigt werden
        if self.__which_database == "log":
            output = super().db_get_data_desc( self.__db_name )                
            for instance in output:
                data_array.append (instance.log) 

        #Fallst timestamp benötigt wird
        if (timestamp):
            for instance in output:
                timestamp_array.append (instance.timestamp)
            return data_array , timestamp_array

        return data_array

    #Methode löscht Daten                 
    #Übergabeparameter: sich selbst, zu löschendes Attribut
    #Rückgabe: keine
    def delete_data ( self , to_delete ): 
        super().db_delete_data( self.__db_name , self.attribute , to_delete )   
        return 

    #Methode findet Einträge in Datenbanken                 
    #Übergabeparameter: sich selbst, to_find: gesuchtes Attribut
    #Rückgabe: result: True: falls Eintrag gefunden
    #                  False: falls kein Eintrag gefunden
    def find_data ( self , to_find ):      
        result = super().db_find_data( self.__db_name , self.attribute , to_find )   
        return result  
    
    #Funktion: zählt den Inhalt der Eintrage einer Tabelle und begrenzt die EInträge auf bestimmte Anzahl 
    #Parameter (table): Name der Tabelle
    #Parameter (number): maximale Anzahl der Eintrage die angezigt werden
    #Rückgabe: True, wenn noch Einträge vollzogen werden dürfen, sonst False
    def rowcount (self , number):
        entries = super().row_count( self.__which_database)
        #True, wenn weniger Zeilen als erlaubt
        if entries < number:
            return True
        else:
            return False

    #Funktion: liest das, was es bekommt Stück für Stück ein und speichert jeden einzelnen Wert in ein Array
    ##Parameter (result): die aus der Datenbank ausgelesene Datei wird so konvertiert, dass Jinji 2 ihn besser verarbeiten kann (nötig wenn mehrere Zeilen eingelesen werden)
    ##Rückgabe: gibt den Inhalt im Array zurück 
    def get_change_sql_output ( self, timestamp = False, extra_info = ""):
        result = self.execute_get_data(timestamp)
        #Reihenfolge umkehren
        result.reverse()
        #Array als Zwischenspeicher
        result_dict = {}         
        #füllt ins Array result_array für jeden Eintrag aus result den aktuellen Wert ein
        for count , row in enumerate(result):
            result_dict.update({ count : row })
        #result_dict = json.dumps(result_dict)
        return result_dict



#Klasse für Datenbankzugriffe mit eine Anzahl an Ausgaben
class Limited_output(Main_sqlalchemy):



    #Funktion: Konstruktor
    #Parameter (number): die gewünschte Anzahl, wenn man alle will, gibt man nichts ein
    #Parameter (which_database): einen String mit dem Namen der DB --> definiert sind die Namen in der Datei: database02
    #Rückgabe: keinen
    def __init__(self , which_database , number = -1  ) -> None:
        self.__value_time = []
        self.__value_date = []
        self.__value_entry = []
        self.__value_dict = {}
        self.number = number 
        super().__init__(which_database)
        self.number = number
        return

    #Funktion: füllt ein dict mit den Daten timestamp und einem Attribute aus der Datenbank
    #Parameter: reverse: falls true, wird die Reihenfolge der Liste gedreht
    #Rückgabe: keine
    def get_data_amount(self, reverse = False):
        all_value_entrys , all_timestamp_entrys = super().execute_get_data( True )
        self.__value_time.clear()
        self.__value_date.clear()
        self.__value_entry.clear()
        self.__value_dict.clear()

        for count , timestamp_entry in enumerate (all_timestamp_entrys):
            time = timestamp_entry.time().strftime("%H:%M:%S")
            date = timestamp_entry.date().strftime("%d-%b-%Y")
            self.__value_time.append(time)
            self.__value_date.append(date)
            self.__value_entry.append(all_value_entrys [count])
            if (count >= (self.number-1) and self.number != -1):
                break

        if reverse:
            self.__value_time.reverse()
            self.__value_date.reverse()
            self.__value_entry.reverse()

        self.__value_dict = { "time":self.__value_time , "date" : self.__value_date , "value" : self.__value_entry }
        return 

    #Funktion: gibt aus dem Daten dict die Einträge vom gewünschten Attribute
    #Parameter: keine
    #Rückgabe: das Array mit den Einträgen aus der DB
    def get_values (self):
        output =  self.__value_dict ["value"]
        return output
    
    #Funktion: gibt aus dem Daten dict die Einträge vom Datum des timestamps
    #Parameter: keine
    #Rückgabe: das Array mit den Einträgen aus der DB
    def get_date (self):
        output =  self.__value_dict ["date"]
        return output
        
    #Funktion: gibt aus dem Daten dict die Einträge von der Zeit des timestamps
    #Parameter: keine
    #Rückgabe: das Array mit den Einträgen aus der DB
    def get_time (self):
        output =  self.__value_dict ["time"]
        return output

    #Funktion: gibt aus dem Daten dict die Einträge von allen Einträgen zurück
    #Parameter: keine
    #Rückgabe: das Array mit den Einträgen aus der DB
    def get_all (self):
        output =  self.__value_dict
        return output

#Klasse zum Erstellen von CSV-Dateien
class Create_csv (Limited_output): 

    #Funktion: Konstruktor
    #Parameter (number): die gewünschte Anzahl, wenn man alle will, gibt man nichts ein
    #Parameter (which_database): einen String mit dem Namen der DB --> definiert sind die Namen in der Datei: database02
    #Rückgabe: keinen
    def __init__(self , which_database) -> None:
        super().__init__(which_database)
        return

    #Funktion: liest den Inhalt aus der Tablle einer Datenbank ein 
    #Parameter (which_table): wählt die Tabelle aus, in die geschrieben werden soll
    #Rückgabe: False, wenn es nicht geklappt hat
    def write_excel_file (self):
        try:
            super().get_data_amount()
            value = super().get_values()
            date = super().get_date()
            time = super().get_time()
            #schreibt die oben beschriebene Line in die csv Datei
            output = io.StringIO()
            writer = csv.writer(output)
            writer.writerow(self.line)
            #geht den Gesamten Inhalt durch und schreibt in csv
            for count, entry in enumerate(value):
                #line = [(str(row[0])),(str(row[1])),(str(row[2]))]
                #for others than Libre office
                line = [(entry),(str(date[count])),(str(time[count]))]
                writer.writerow(line)
            output.seek(0)
            #gibt die Datei zurück
            return output
        except:
            traceback.print_exc() 
            return False 





    



