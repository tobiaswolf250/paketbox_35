#Orientiert an: https://mariadb.com/de/resources/blog/using-sqlalchemy-with-mariadb-connector-python-part-1/

#Datei dient der Erstellung von den Tabellen in der Datenbank
from time import timezone
from unicodedata import name
import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import func
from sqlalchemy.sql.schema import PrimaryKeyConstraint
import uuid
import json
import sys
import os


def db_login():
    sys.path.append(os.path.dirname(os.path.abspath(__file__)).split("/Code")[0] + "/Code/main")
    from filefolder_finder import Find_file

    path_to_json_file = Find_file().find_filefolder("config")

    with open(path_to_json_file,"r+") as states_file:
        #Speichert das Eingelesene extra ab in einer map-klasse
        states_data = states_file.read()
        #konvertiert zu json File

    states_data = json.loads(states_data)

    pwd = states_data["db_pwd"]
    user = states_data["db_user"]

    return "mysql+pymysql://"+user+":"+pwd+"@localhost:3306/Paketbox"


Base = declarative_base()

#Klasse erstellt die Tablle Emailaddress in der Datenbank
class Emailaddress(Base):
    __tablename__='emailaddress'
    address=sqlalchemy.Column(sqlalchemy.VARCHAR(100))
    timestamp= sqlalchemy.Column(sqlalchemy.DateTime(timezone=True), server_default=func.now())
    id = sqlalchemy.Column(sqlalchemy.INTEGER, primary_key=True, autoincrement=True)

#Klasse erstellt die Tablle Package_Number in der Datenbank
class Package_Number(Base):
    __tablename__='package_number'
    number=sqlalchemy.Column(sqlalchemy.VARCHAR(40))
    timestamp= sqlalchemy.Column(sqlalchemy.DateTime(timezone=True), server_default=func.now())
    id = sqlalchemy.Column(sqlalchemy.INTEGER, primary_key=True, autoincrement=True)

#Klasse erstellt die Tablle Package_Number_Delivered in der Datenbank
class Package_Number_Delivered(Base):
    __tablename__='package_number_delivered'
    number_delivered=sqlalchemy.Column(sqlalchemy.VARCHAR(40))
    timestamp= sqlalchemy.Column(sqlalchemy.DateTime(timezone=True),server_default=func.now())
    id = sqlalchemy.Column(sqlalchemy.INTEGER, primary_key=True, autoincrement=True)

#Klasse erstellt die Tablle Log in der Datenbank
class Log(Base):
    __tablename__='log'
    log=sqlalchemy.Column(sqlalchemy.VARCHAR(50))
    timestamp= sqlalchemy.Column(sqlalchemy.DateTime(timezone=True), server_default=func.now())
    id = sqlalchemy.Column(sqlalchemy.INTEGER, primary_key=True, autoincrement=True)
    
#Klasse erstellt die Tablle Working_Time in der Datenbank
class Working_Time(Base):
    __tablename__='working_time'
    start_h=sqlalchemy.Column(sqlalchemy.VARCHAR(3))
    start_m=sqlalchemy.Column(sqlalchemy.VARCHAR(3))
    end_h=sqlalchemy.Column(sqlalchemy.VARCHAR(3))
    end_m=sqlalchemy.Column(sqlalchemy.VARCHAR(3))
    id = sqlalchemy.Column(sqlalchemy.INTEGER, primary_key=True, autoincrement=True)
    

class fill_databases :

    def __init__(self , string_db_login) -> None:
        engine = sqlalchemy.create_engine(string_db_login, pool_size=10, max_overflow=20)
        Session = sqlalchemy.orm.sessionmaker()
        Session.configure(bind=engine)
        self.Session = Session()

    def execute (self):
        self.insert_worktime_data()
        self.insert_emailadress_test()
        self.insert_packagenumber_delivered_test()
        self.insert_packagenumber_test()
        self.insert_log_test()
        print ("Die Tabelle wurde erfolgreich gefüllt und ist nun einsatzbereit.")
        return

    #Überschreibt die Daten in der Log Datenbank
    #Parameter1: sich selbst
    #Parameter2: die neuen Daten mit den die alten überschrieben werden sollen
    def insert_log_test(self):
        name_database = Log
        start_entry = name_database( log = "Scanner active")
        self.Session.add(start_entry)
        self.Session.commit()
        return 

    #Überschreibt die Daten in der Package_Number Datenbank
    #Parameter1: sich selbst
    #Parameter2: die neuen Daten mit den die alten überschrieben werden sollen
    def insert_packagenumber_test(self):
        name_database = Package_Number
        start_entry = name_database( number = "1234567890")
        self.Session.add(start_entry)
        self.Session.commit()
        return 

    #Überschreibt die Daten in der Package_Number_Delivered Datenbank
    #Parameter1: sich selbst
    #Parameter2: die neuen Daten mit den die alten überschrieben werden sollen
    def insert_packagenumber_delivered_test(self):
        name_database = Package_Number_Delivered
        start_entry = name_database( number_delivered = "0987654321")
        self.Session.add(start_entry)
        self.Session.commit()
        return 

    #Überschreibt die Daten in der Emailaddress Datenbank
    #Parameter1: sich selbst
    #Parameter2: die neuen Daten mit den die alten überschrieben werden sollen
    def insert_emailadress_test(self):
        name_database = Emailaddress
        start_entry = name_database( address = "phipsifredle@web.de" )
        self.Session.add(start_entry)
        self.Session.commit()
        return 

    #Überschreibt die Daten in der Working_Time Datenbank
    #Parameter1: sich selbst
    #Parameter2: die neuen Daten mit den die alten überschrieben werden sollen
    def insert_worktime_data(self):
        name_database = Working_Time
        start_entry = name_database( start_h = "07" , start_m = "00" , \
                    end_h = "19" , end_m = "00" )
        self.Session.add(start_entry)
        self.Session.commit()
        return 



if __name__ == '__main__':
    Base.metadata.create_all(sqlalchemy.create_engine(db_login()))
    fill_databases(db_login()).execute()








       
