import traceback
import pymysql
from datetime import datetime
from sqlalchemy import create_engine
from sqlalchemy.sql.expression import column
import check_process.sql.database02 as db
from sqlalchemy.orm import sessionmaker
from sqlalchemy import false, func
from sqlalchemy import desc
import json
import os
import sys


def db_login():
    sys.path.append(os.path.dirname(os.path.abspath(__file__)).split("/Code")[0] + "/Code/main")
    from filefolder_finder import Find_file

    path_to_json_file = Find_file().find_filefolder("config")

    with open(path_to_json_file,"r+") as states_file:
        #Speichert das Eingelesene extra ab in einer map-klasse
        states_data = states_file.read()
        #konvertiert zu json File

    states_data = json.loads(states_data)

    pwd = states_data["db_pwd"]
    user = states_data["db_user"]

    return "mysql+pymysql://"+user+":"+pwd+"@localhost:3306/Paketbox"


Session = sessionmaker(create_engine(db_login(), pool_size=10, max_overflow=20))
#Klasse liest Daten aus der Datenbank aus und speichert, löscht oder ändert Einträge
class Db_Connection():

    #Konstruktor
    def __int__(self):
        print("Db_Connection created") 

    #gibt Zeiten aus der Datenbank von der Tabelle Working_Time aus
    #Parameter: keine
    #Rückgabe: Zeien als Liste zurückgegeben
    def db_get_worktime_data(self):
        try:
            #starte Session und greift auf DB zu
            with Session.begin() as session:
                output = session.query( db.Working_Time.start_h , db.Working_Time.start_m , db.Working_Time.end_h , db.Working_Time.end_m ).all()
                return output
        except:
            traceback.print_exc()
            return

    #Methode updatet Daten einer Tabelle in der Datenbank
    #Parameter (which_table): auf welche Tabelle wird zugegriffen
    #Parameter (old_content): Daten, welche geupdatet werden 
    #Parameter (new_content): Daten, mit denen die alten überschrieben werden
    #Rückgabe: keine
    def db_update_data(self , which_table , old_content , new_content ):
        try:
            with Session.begin() as session:
                #prüft welche Tabelle und weist dem entsprechenden Attribute dem Wert zu uns speichert neuen Wert im dict updatet_entry 
                if which_table == "emailaddress":
                    name_table = db.Emailaddress
                    attribute =  name_table.address

                elif which_table == "package_number":
                    name_table = db.Package_Number
                    attribute =  name_table.number

                updated_entry = { attribute : new_content }
                #greift auf Datenbank zu und updatet den Inhalt in der der Tabelle 
                session.query( name_table ).filter( attribute == old_content ).update( updated_entry )
                return 
        except:
            traceback.print_exc()
            return

    #Methode updatet Daten in der Working_Time Tabelle in der Datenbank
    #Parameter (old_content): Daten, welche geupdatet werden 
    #Parameter (new_content): Daten, mit denen die alten überschrieben werden
    #Rückgabe: nichts
    def db_update_worktime_data(self, old_content , new_content):
        try:
            with Session.begin() as session:
                name_table = db.Working_Time
                update_dict = { name_table.start_h : new_content["start_hour"] , name_table.start_m : new_content["start_minute"] , \
                            name_table.end_h : new_content["end_hour"] , name_table.end_m : new_content["end_minute"] }

                #greift auf Datenbank zu und updatet den Inhalt in der der Tabelle 
                session.query(name_table).filter(name_table.start_h == old_content["start_hour"] , name_table.start_m == old_content["start_minute"] , \
                                                    name_table.end_h == old_content["end_hour"], name_table.end_m == old_content["end_minute"]).update(update_dict)

                return 
        except:
            traceback.print_exc()
            return
    
    #Methode Fügt Daten in die Datenbank ein
    #Parameter (new_data): in bereits passendem Format neue Daten in DB einspeichern 
    #Rückgabe: nichts
    def db_insert_data(self, new_data):
        try:
            with Session.begin() as session:
                session.add(new_data)
                session.commit()
                return
        except:
            traceback.print_exc()
            return
            
    #Methode holt alle Daten aus einer Tabelle der Datenbank
    #Parameter (name_table): Tabelle, aus der Daten geholt werden sollen
    #Rückgabe: eine Liste des gesamten Inhaltes der Tabelle 
    def db_get_data (self, name_table):
        try:
            with Session.begin() as session:
                result = session.query(name_table).all()
                return result 
        except:
            traceback.print_exc()
            return

    #Methode holt alle Daten sortier(oben neuester Eintrag) aus einer Tabelle der Datenbank
    #Parameter (name_table): Tabelle, aus der Daten geholt werden sollen
    #Rückgabe: eine Liste des gesamten Inhaltes der Tabelle 
    def db_get_data_desc (self , name_table):
        try:
            with Session.begin() as session:
                result = session.query(name_table).order_by(desc(name_table.timestamp))
                return result 
        except:
            traceback.print_exc()
            return
        
    #Methode löscht Daten aus der Datenbank
    #Parameter (name_table): Name der Datenbank, aus der etwas gelöscht werden soll
    #Parameter (attribute): welches Attribut der Tabelle, aus der gelöscht werden sollen
    #Parameter (content): Daten die gelöscht werden sollen
    def db_delete_data (self, name_table , attribute ,  content):
        try:
            with Session.begin() as session:
                session.query(name_table).filter(attribute == content).delete()
                return 
        except:
            traceback.print_exc()
            return

    #Methode prüft ob Datum in Datenbank gespeichert ist
    #Parameter (name_table): Name der Tabelle
    #Parameter (attribute): welches Attribut der Tabelle, aus der gesucht werden sollen
    #Parameter (content): Datum nach dem gesucht werden soll
    #Rückgabe: True, wenn Paketnummer in datenbank vorhanden ist, sonst False
    def db_find_data (self, name_table , attribute ,  content):
        try:
            with Session.begin() as session:
                if session.query(name_table).filter(attribute == content).one_or_none() is None:
                    return False
                else:
                    return True
        except:
            traceback.print_exc()
            return

    #Methode gibt die Anzahl der Zeilen einer Tabelle zurück
    #Parameter (wich_database): Name der Tabelle
    #Rückgabe: Anzahl der Zeilen der entsprechenden Datenbank
    def row_count (self , which_database):
        with Session.begin() as session:
            all_numbers = 0
            if which_database == "package_number":
                all_numbers = session.query(func.count(db.Package_Number.number)).scalar()
            return all_numbers

    #Methode Liest alle Daten aus der Datenbank aus
    #Parameter: Name der Datenbank aus der die Daten ausgelesen werden sollen
    #Rückgabe: Mehrdimensionale Liste, mit den Daten(Log, Package_number,...), in der ersten Dimension und dem timestamp in der zweiten Dimension
    def select_all(self, name_table):
        with Session.begin() as session:
            #Daten aus Package_Number_Delivered auslesen
            if name_table == db.Package_Number_Delivered:
                result = session.query(name_table).all()
                list1 = []
                list2 = []
                for number_data in result:
                    list1.append(number_data.__dict__["number_delivered"])
                    list2.append(number_data.__dict__["timestamp"])
                return list1, list2
            
            #Daten aus Package_Number auslesen
            elif name_table == db.Package_Number:
                result = session.query(name_table).all()
                list1 = []
                list2 = []
                for number_data in result:
                    list1.append(number_data.__dict__["number"])
                    list2.append(number_data.__dict__["timestamp"])
                return list1, list2

            #Daten aus Log auslesen
            elif name_table == db.Log:
                result = session.query(name_table).all()
                list1 = []
                list2 = []
                for number_data in result:
                    list1.append(number_data.__dict__["log"])
                    list2.append(number_data.__dict__["timestamp"])
                return list1, list2

            #Daten aus Emailaddress auslesen
            elif name_table == db.Emailaddress:
                result = session.query(name_table).all()
                list1 = []
                list2 = []
                for number_data in result:
                    list1.append(number_data.__dict__["address"])
                    list2.append(number_data.__dict__["timestamp"])
                return list1, list2

    #Zerstört die Instanz
    def __del__(self):
        print("Db_Connection deleted")

    
