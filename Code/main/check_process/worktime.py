import datetime
from check_process.sql.sqlalchemy_api import Main_sqlalchemy  

#Klasse überprüft, ob sich das System innerhalb der Nutzungszeiten befindet
class Check_time:

    #Die Methode prüft, ob sihc das System innerhalb der Nutzungszeiten befindet
    #Übergabeparameter: keine
    #Rückgabe: True, falls innherhalb der Nutzungszeit
    #          False, falls außerhalb der Nutzungszeit
    def worktime(self):
        approval = True
        #Benutzerzeitraum aus Datenbank einlesen

        working_time = Main_sqlalchemy().get_worktime()


        #End- und Anfangszeiten in Variablen speichern
        h_now = int(datetime.datetime.now().strftime('%H'))
        m_now = int(datetime.datetime.now().strftime('%M'))
        h_low_db = int(working_time['start_hour'])
        m_low_db = int(working_time['start_minute'])
        h_high_db = int(working_time['end_hour'])
        m_high_db = int(working_time['end_minute'])

        #Prüfung, ob innerhalb der Nutzungszeit oder nicht
        if h_now > h_low_db and h_now < h_high_db \
            or h_now == h_low_db and m_now >= m_low_db \
            or h_now == h_low_db and h_now == h_high_db \
            and m_now >= m_low_db and m_now <= m_high_db \
            or h_now == h_high_db and m_now <= m_high_db:  
            approval = True
        else:
            approval = False

        return approval

