from email_validator import validate_email, EmailNotValidError
from check_process.sql.sqlalchemy_api import Main_sqlalchemy
import json
import traceback

#Klasse ist für die Verarbeitung und Prüfung der eingegeben Mail-Adressen zuständig
class Emails():

    #Funktion: Konstruktor weißt Werte zu 
    #Parameter (email): enthält die eingetragene Mail
    #Parameter (self): enthält sich selbst
    #Rückgabe: keine
    def __init__(self , new_email):
        self._email = new_email 
        self.__delete_space()
        return 

    #Destruktor
    def __delete_space(self):
        self._email = self._email.lstrip() 
        self._email = self._email.rstrip() 
        return 

    #Funktion: führt Emailprüfung der neuen Adressen durch 
    #Parameter (self): enthält sich selbst
    #Rückgabe: True wenn Prüfung erfolgreich
    def check_new_email(self):
        try:
            if not self.make_mailcheck():
                return True
            self.sqlentry_email()
            return True
        except:
            traceback.print_exc()
            return False 

    #Funktion: führt Emailprüfung durch 
    #Parameter (self): enthält sich selbst
    #Rückgabe: True wenn prüfung erfolgreich
    def make_mailcheck(self):
        return self.valid_email() and self.check_email_compare()

    #Funktion: fügt in db die neue Mail-Adresse ein 
    #Parameter (self): enthält sich selbst
    #Rückgabe: keine
    def sqlentry_email(self):
        #führt SQL-Befehl aus 
        Main_sqlalchemy("emailaddress").insert_data(self._email)
        return

    #Funktion: prüft ob Email-Adresse bereits vorhanden ist
    #Parameter: erhält die eingetragene Mail-Adresse
    #Rückgabe: True wenn Email-Adresse nicht enthalten ist, False wenn Email bereits eingetragen wurde
    def check_email_compare(self, pass_other_mail=None):
        if pass_other_mail == None:
            mail_to_check = self._email
        else:
            mail_to_check = pass_other_mail

        all_emails = Main_sqlalchemy("emailaddress").execute_get_data()
        #geht alle eingetragenen E-Mails durch
        for row in all_emails:
            if row == mail_to_check:
                return False
        return True

    #Funktion: prüft, ob Adresse die Standard-Anforderungen an Mail-Adressen erfüllt
    #Parameter: erhält die E-Mail 
    #Rückgabe: Validierung erfolgereich = True (email ist valide)
    def valid_email(self, pass_other_mail=None):
        if pass_other_mail == None:
            mail_to_check = self._email
        else:
            mail_to_check = pass_other_mail
        try:
            # Validierung, gibt Error wenn was nicht passt
            print (mail_to_check)
            validate_email(mail_to_check) 
            return True
        except EmailNotValidError:
            traceback.print_exc()
            # Validierung fehlgeschlagen, gibt einen Error
            return False

#Klasse verarbeitet die geänderten Mail-Adressen
class Changed_emails(Emails):

    #Funktion: Konstruktor, Initalisierung der Variablen
    #Parameter: emails: die E-Mail-Adressen 
    #Rückgabe: keine
    def __init__(self, emails):
        self.__old_email = emails["old"]
        self.__new_email = emails["new"]
        super().__init__(self.__new_email)
        self.__delete_space()

    #Funktion: Entfernt Leerzeichen vor und nach der Mail-Adresse
    #Parameter: Keine 
    #Rückgabe: keine
    def __delete_space(self):
        self.__old_email = self.__old_email.lstrip() 
        self.__old_email = self.__old_email.rstrip()
        return    

    #Funktion: Prüft die neue und die alten Mail-Adresse, ändert Eintrag in Datenbank
    #Parameter: keine
    #Rückgabe: Validierung erfolgereich = True, sonst False
    def mail_change_checker(self):
        try:
            if not self.__check_both_emails():
                return True
            #Änderung in Datenbank speichern
            self.sql_update()
            return True
        except:
            traceback.print_exc()
            return False 

    #Funktion: Prüft beide Mail-Adressen 
    #Parameter: keine
    #Rückgabe: Validierung erfolgereich = True, sonst False
    def __check_both_emails(self):
        return super().make_mailcheck() and self.__old_number_check()

    #Funktion: Prüft ob alte Email-Adresse valide und vorhanden ist
    #Parameter: keine 
    #Rückgabe: Validierung erfolgereich = True, sonst False
    def __old_number_check(self):
        return not(self.check_email_compare(self.__old_email)) and self.valid_email(self.__old_email)

    #Funktion: Fügt in db die neue Mail-Adresse in die Datenbank ein und löscht die alte 
    #Parameter (self): enthält sich selbst
    #Rückgabe: keinen
    def sql_update(self):
        Main_sqlalchemy("emailaddress").update_value( self.__old_email , self._email )
        return

#Klasse verarbeitet die gelöschten Mail-Adressen
class Deleted_emails(Emails):

    #Konstruktor
    def __init__(self, mail):
        self._email = mail
        super().__init__(self._email)

    #Funktion: Überprüft gelöschte Mail-Adresse und löscht diese aus Datenbank
    #Parameter (self): enthält sich selbst
    #Rückgabe: True: falls erfolgreich
    #          False: fall nicht erfolgreich
    def mail_delete_checker(self):
        try:
            if not self.__old_number_check():
                return True
            #Mail-Adresse aus DB löschen
            self.__sql_delete()
            return True
        except:
            traceback.print_exc()
            return False 

    #Funktion: Prüft ob zu löschende Email-Adresse valide und vorhanden ist
    #Parameter: keine 
    #Rückgabe: Validierung erfolgereich = True, sonst False
    def __old_number_check(self):
        return not(self.check_email_compare()) and self.valid_email()

    #Funktion: Löscht alte Mail-Adresse aus der Datenbank 
    #Parameter (self): enthält sich selbst
    #Rückgabe: keinen
    def __sql_delete(self):
        Main_sqlalchemy("emailaddress").delete_data(self._email)
        return

