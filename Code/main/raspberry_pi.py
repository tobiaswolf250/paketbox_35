import RPi.GPIO as GPIO
import paho.mqtt.publish as publish
import paho.mqtt.client as paho
from states_process.states import States
import time
import threading 
import logging
from scanner_barcode_reader import Barcode_reader



#Die Klasse steuert die GPIO-Pins der Leds abhängig von MQTT-Nachrichten und sendet eine MQTT-Nachricht, 
#wenn die entsprechende Methode aufgerufen wird
class Raspberry():

    #Konstruktor
    def __init__(self):
        self.change_states = States()
        
        GPIO.setmode(GPIO.BOARD)

        #LED: Paketboxöffner
        GPIO.setup(31, GPIO.OUT)
        GPIO.output(31, False)

        #LED: Beleuchtung
        GPIO.setup(37, GPIO.OUT)
        GPIO.output(37, False)

        #Taster: Error_Button
        GPIO.setup(26, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)

        print("Raspberry created")

    #Die Methode steuert den GPIO-Pin 31 (Led für Zustand der Paketbox offen/geschlossen), 
    #bei einer Zustandsänderung über MQTT
    #Übergabeparameter: client, userdata: Parameter der MQTT-Nachricht
    #                   message: Inhalt der MQTT-Nachricht
    #Rückgabe: keine
    def __led_boxstate(self, client, userdata, message):
        #Nachricht wird in dict-Format umgeschrieben
        msg_Boxstate = self.change_states.optimzie_mqtt(message)
        #Ausgang auf high setzen
        if msg_Boxstate["Boxstate"] == 1:
            GPIO.output(31, True)
        #Ausgang auf low setzen
        elif msg_Boxstate["Boxstate"]== 0:
            GPIO.output(31, False)
        return

    #Die Methode steuert den GPIO-Pin 37 (Led für Zustand Beleuchtung), bei einer Zustandsänderung über MQTT
    #Übergabeparameter: client, userdata: Parameter der MQTT-Nachricht 
    #                   message: Inhalt der MQTT-Nachricht
    #Rückgabe: keine
    def __led_lighting(self, client, userdata, message):
        #Nachricht wird in dict-Format umgeschrieben
        msg_Lighting = self.change_states.optimzie_mqtt(message)
        #Ausgang auf high setzen
        if msg_Lighting["Lighting"] == 1:
            GPIO.output(37, True)
        #Ausgang auf low setzen
        elif msg_Lighting["Lighting"] == 0:
            GPIO.output(37, False)
        return

    #Die Methode sendet eine MQTT-Nachricht, dass der Fehlertaster betätigt wurde
    #Übergabeparameter: msg: Inhalt der MQTT-Nachricht
    #                   host: Name des Hosts für die MQTT-Nachricht
    #Rückgabe: keine
    def __send_mqtt(self, top, msg):
        publish.single(topic=top, payload=msg, hostname="localhost")
        return

    #Die Methode baut eine Verbindung vom MQTT-broker auf und Abonniert alles unter "paketbox/frontend/#"
    #Übergabeparameter: keine
    #Rückgabe: keine
    def mqtt_connect(self):
        client = paho.Client()

        client.message_callback_add("paketbox/frontend/light", self.__led_lighting)
        client.message_callback_add("paketbox/frontend/boxstate", self.__led_boxstate)
        print("mqtt")        
        client.connect("127.0.0.1", 1883, 60)

        client.subscribe("paketbox/frontend/#", 0)

        client.loop_forever()


    #Die Methode verarbeitet die Änderungen am GPIO-Pin 40 (Fehlertaster)
    #Übergabeparameter: keine
    #Rückgabe: keine
    def button(self):
        reset = True

        GPIO.setmode(GPIO.BOARD)

        #Taster: Error_Button
        GPIO.setup(40, GPIO.IN, pull_up_down = GPIO.PUD_UP)

        while 1:
            #Wird ausgeführt, wenn Taster nicht betätigt ist
            if GPIO.input(40) == GPIO.HIGH and reset == False:
                reset = True
            
            #Wird ausgeführt wenn Taster betätigt ist und der Taster davor nicht gedürckt ist
            if GPIO.input(40) == GPIO.LOW and reset == True:
                #MQTT-Nachricht wird versendet
                self.__send_mqtt("paketbox/backend/Errorbutton", "Errorbutton: 1")
                time.sleep(0.5)
                reset = False

        GPIO.cleanup()
        return

    #Die Methode verarbeitet die eingescannten Paketnummern
    #Übergabeparameter: keine
    #Rückgabe: keine
    def scanner(self):
        try:  
            old_number = "0"
            number = Barcode_reader()          
            while True:
                new_number = number.barcode_reader()
                if new_number is not old_number:
                    self.__send_mqtt("paketbox/backend/Scanner_number", new_number)
                    print(new_number)
                    old_number = new_number

        except KeyboardInterrupt:
            print("Keyboard interrupt")
            logging.debug('Keyboard interrupt')

        except Exception as err:
            print("Error")
            logging.error(err)

#Aufruf der Klasse "Raspberry"
if __name__ == '__main__':
    raspberry = Raspberry()
    thread_led = threading.Thread(target=raspberry.mqtt_connect)
    thread_button = threading.Thread(target=raspberry.button)
    thread_scanner = threading.Thread(target=raspberry.scanner)
    thread_led.start()
    thread_button.start()
    thread_scanner.start()