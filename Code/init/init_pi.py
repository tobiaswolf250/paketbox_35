import sys
import subprocess

#apt commands
subprocess.run(["sudo", "apt", "update"])
subprocess.run(["sudo", "apt", "upgrade"])
subprocess.run(["sudo", "apt", "install", "mariadb-server mariadb-client", "-y"])
subprocess.run(["sudo", "apt", "install", "python3-pip", "-y"])
subprocess.run(["sudo", "apt", "install", "espeak", "-y"])
subprocess.run(["sudo", "apt", "install", "python-pip", "-y"])
subprocess.run(["sudo", "apt", "install", "mosquitto", "-y"])
subprocess.run(["sudo", "apt-get", "install", "mosquitto", "-y"])
#subprocess.run(["sudo", "apt", "install", "git", "-y"])
subprocess.run(["sudo", "apt", "install", "mosquitto-clients", "-y"])
subprocess.run(["sudo", "apt", "install", "espeak python3-espeak", "-y"])

subprocess.run(["sudo", "mosquitto", "-d"])

#installation of every needed package
subprocess.check_call([sys.executable, "-m", "pip", "install", "gunicorn"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "datetime"])
#subprocess.check_call([sys.executable, "-m", "pip", "install", "time"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "Jinja2"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "SQLAlchemy"])

#sudo installation of pip packages
subprocess.run(["sudo", "pip", "install", "paho-mqtt"])
subprocess.run([sys.executable, "-m", "pip", "install", "flask"])
subprocess.run([sys.executable, "-m", "pip", "install", "pymysql"])
subprocess.run([sys.executable, "-m", "pip", "install", "flask-SQLAlchemy"])
subprocess.run([sys.executable, "-m", "pip", "install", "flask-socketio"])
subprocess.run([sys.executable, "-m", "pip", "install", "flask-mqtt"])
subprocess.run([sys.executable, "-m", "pip", "install", "email-validator"])
subprocess.run([sys.executable, "-m", "pip", "install", "paho-mqtt"])
subprocess.run([sys.executable, "-m", "pip", "install", "gunicorn==20.1.0"])
subprocess.run([sys.executable, "-m", "pip", "install", "eventlet==0.30.2"])
subprocess.run([sys.executable, "-m", "pip", "install", "rpi.gpio"])
subprocess.ruun([sys.executable, "-m", "pip", "install", "pygame"])

