import json
import sys 
import os
import traceback



class Handle_db_init():

    def __init__(self):
        self.db_user = input("Geben Sie bitte den Datenbank Benutzer ein:")
        self.db_pwd = input("Geben Sie bitte das zugehörige Passwort ein:")

        self.__file = "config"
        #Dateipfad für state.json feststellen
        try:
            sys.path.append(os.path.dirname(os.path.abspath(__file__)).split("/Code")[0] + "/Code/main")
            from filefolder_finder import Find_file
        except:
            traceback.print_exc()

        self.path_to_json_file = Find_file().find_filefolder(self.__file)
        
        self.__config_file = self.config_read()

    def config_read(self):
        with open(self.path_to_json_file,"r+") as states_file:
            #Speichert das Eingelesene extra ab in einer map-klasse
            states_data = states_file.read()
            #konvertiert zu json File
    
        states_data = json.loads(states_data)
        return states_data 

    def handle_db_init(self):  
        self.db_login_data = {
            "db_pwd": self.db_pwd,
            "db_user": self.db_user
        }
            
    def state_write(self):    
        for key in self.db_login_data:
            self.__config_file[key] = self.db_login_data[key]
        #Beschreibt alles nochmal neu und nimmt so den alten Wert mit 
        with open(self.path_to_json_file, "w") as states_file:
            json.dump(self.__config_file, states_file)
        return 

    def execute(self):
        self.handle_db_init()
        self.state_write()

print(__name__)
if __name__ == "__main__":
    Handle_db_init().execute()
    