    #ändern zu einem Objekt machen
#Code der Email-Verwaltung
from email_validator import validate_email, EmailNotValidError
from check_process.sql.sqlalchemy_api import main_sqlalchemy
import json
import traceback


class work_with_new_emails():

    #Funktion: Konstruktor weißt Werte zu 
    #Parameter (email): enthält die eingetragene Mail
    #Parameter (self): enthält sich selbst
    #Rückgabe: keine
    def __init__(self , new_email):
        self.email = new_email 
        self.delete_space()
        return 

    def delete_space (self):
        self.email = self.email.lstrip() 
        self.email = self.email.rstrip() 
        return 

    #Funktion: führt Emailprüfung durch 
    #Parameter (self): enthält sich selbst
    #Rückgabe: True wenn prüfung erfolgreich
    def check_new_email(self):
        try:
            if not self.make_mailcheck():
                return True
            self.sqlentry_email()
            return True
        except:
            traceback.print_exc()
            return False 

    #Funktion: führt Emailprüfung durch 
    #Parameter (self): enthält sich selbst
    #Rückgabe: True wenn prüfung erfolgreich
    def make_mailcheck(self):
        return self.valid_email() and self.check_email_compare()

    #Funktion: fügt in db die neue mail ein 
    #Parameter (self): enthält sich selbst
    #Rückgabe: keinen
    def sqlentry_email(self):
        #führt SQL-Befehl aus 
        main_sqlalchemy("emailaddress").insert_data(self.email)
        return

    #Funktion: prüft ob Email bereits vorhanden ist
    #Parameter: erhält die eingetragene Mail
    #Rückgabe: True wenn Email nicht enthalten ist, False wenn Email bereits eingetragen wurde
    def check_email_compare(self , pass_other_mail=None):
        mail_to_check = " "
        if pass_other_mail == None:
            mail_to_check = self.email
        else:
            mail_to_check = pass_other_mail

        all_emails = main_sqlalchemy("emailaddress").execute_get_data()
        #geht alle eingetragenen E-Mails durch
        for row in all_emails:
            if row == mail_to_check:
                return False
        return True

    #Funktion: prüft, ob Email die Standard-Anforderungen erfüllt
    #Parameter: erhält die E-Mail 
    #Rückgabe: Validierung erfolgereich = True (email ist valide)
    def valid_email(self , pass_other_mail=None):
        if pass_other_mail == None:
            mail_to_check = self.email
        else:
            mail_to_check = pass_other_mail
        try:
            # Validierung, gibt Error wenn was nicht passt
            validate_email(mail_to_check) 
            return True
        except EmailNotValidError:
            traceback.print_exc()
            # Validierung fehlgeschlagen, gibt einen Error
            return False


class work_with_changed_emails(work_with_new_emails):


    #Funktion: führt die Email-Validierung entsrpehcn der Mail_change anforderung durch 
    #Parameter: erhält die E-Mail 
    #Rückgabe: Validierung erfolgereich = True
    def __init__(self, emails):
        emails = json.loads(emails)
        self.old_email = emails["old"]
        new_email = emails["new"]
        super().__init__(new_email)
        self.__delete_space__()


    #Funktion: führt die Email-Validierung entsrpehcn der Mail_change anforderung durch 
    #Parameter: erhält die E-Mail 
    #Rückgabe: Validierung erfolgereich = True
    def __delete_space__ (self):
        self.old_email = self.old_email.lstrip() 
        self.old_email = self.old_email.rstrip()
        return    

        
    #Funktion: führt die Email-Validierung entsrpehcn der Mail_change anforderung durch 
    #Parameter: erhält die E-Mail 
    #Rückgabe: Validierung erfolgereich = True
    def mail_change_checker(self):
        try:
            if not self.__check_both_emails__():
                return True
            self.sql_update()
            return True
        except:
            traceback.print_exc()
            return False 

    def __check_both_emails__(self):
        return super().make_mailcheck() and self.__old_number_check__()


    def __old_number_check__ (self):
        return not(self.check_email_compare(self.old_email)) and self.valid_email(self.old_email)

   #Funktion: fügt in db die neue mail ein 
    #Parameter (self): enthält sich selbst
    #Rückgabe: keinen
    def sql_update(self):
        main_sqlalchemy("emailaddress").update_value( self.old_email , self.email )
        return




class work_with_deleted_emails(work_with_new_emails):

    def __init__(self, mail):
        super().__init__(mail)

    def mail_delete_checker(self):
        try:
            if not self.__old_number_check__():
                return True
            self.sql_delete()
            return True
        except:
            traceback.print_exc()
            return False 

    def __old_number_check__ (self):
        return not(self.check_email_compare()) and self.valid_email()



    #Funktion: fügt in db die neue mail ein 
    #Parameter (self): enthält sich selbst
    #Rückgabe: keinen
    def sql_delete(self):
        main_sqlalchemy("emailaddress").delete_data( self.email )
        return

