#Klasse zum Bearbeiten der Öffnungszeiten der Paketbox
#from database import *
#from sql_uses import *
from check_process.sql.sqlalchemy_api import main_sqlalchemy

import traceback

class work_with_times:

    #dict für die Zeiten aus dem Frontend
    __new_times__ = {}
    #dict für die zeiten aus der db
    __timecheck__ = {}

    def __init__(self, start_time = None , end_time = None):
        #Zeiten ab wann Paketbox offen ist
        self.__new_times__.update({ "start_hour": start_time[:2]})
        self.__new_times__.update({ "start_minute": start_time[3:5]})
        #Zeiten ab wann Paketbox geschlossen ist
        self.__new_times__.update({ "end_hour": end_time[:2]})
        self.__new_times__.update({ "end_minute": end_time[3:5]})


    #Params: sich selbst
    #return: True, wenn alles nötige abgearbeitet ist
    #Funktion: geht den Ablauf der Zeitprüfung durch 
    def check_times(self):
        try: 
            #bekommt Info der Eingetragenen Zeiten aus der DB
            self.__db_entry_times__()
            #geht durch und schaut, ob ein Eintrag None ist und fügt in diesem Fall den Wert aus der DB ein und ändert eintrag vom dict
            self.__complete_times_dict__()
            #schaut ob zeiten gleich sind, wenn, dann soll bearbeitung abgebrochen werden und diese Zeiten sollen eingetragen werden in die DB
            self.__check_timeisanumber__()
            if self.__times_equal__():
                return True
            

            
            
            #prüft die Reihenfolge der eingetragenen Zeiten und --> vgl von start und end
            if not self.__times_right_pos__ ():
                #ändert die Reihenfolge, da die Endzeit kleiner als die Startzeit ist
                self.__swap_times__()         
            
            #trägt neue Zahlen in DB ein
            self.__change_sqlentry_time__()


            ################################################################
            print ("Z 51: ",self.__new_times__)
            ################################################################

            return True
        except:
            traceback.print_exc() 
            return False

    #Params: sich selbst
    #return: keine 
    #Funktion:  trägt die neuen Zeiten in die DB ein
    def __change_sqlentry_time__(self):
        #ändert die Zeiten, wo die Paketbox offen ist
        
                ################################################################
        print ("Z 66: ",self.__new_times__)
        ################################################################
        
        
        main_sqlalchemy("working_time").update_worktime(self.__timecheck__  ,self.__new_times__)
        return 

    #Params: sich selbst
    #return: keine 
    #Funktion:  wechselt Position der Eingegeben Zeiten 
    def __swap_times__(self):
        #ändert die Zeiten, da der Wert nicht richtig eingegeben wurde
        new_start_h = self.__new_times__["end_hour"]
        new_start_m = self.__new_times__["end_minute"]
        self.__new_times__["end_hour"] = self.__new_times__["start_hour"]
        self.__new_times__["end_minute"] = self.__new_times__["start_minute"]
        self.__new_times__["start_hour"] = new_start_h
        self.__new_times__["start_minute"] = new_start_m 
        return 

    #Params: sich selbst
    #return: False, wenn Reihenfolge nicht passt
    #Funktion:  prüft die Reihenfolge der eingetragenen Zeiten
    def __times_right_pos__ (self):
        #geht die Bedingungen durch, dass die Zeiten in der richtigen Reihenfolge eingegeben wurden
        if int(self.__new_times__["end_hour"]) < int(self.__new_times__["start_hour"]):
            return False
        if self.__new_times__["end_hour"] == self.__new_times__["start_hour"] and \
           int(self.__new_times__["end_minute"]) < int(self.__new_times__["start_minute"]):
            return False
        return True

    #Params: sich selbst
    #return: True, wenn alle gleich sind
    #Funktion:  schaut, ob gesamte Eingabe in das Schema einer Zahl und das Schema Stunden/ Minuten passt
    def __times_equal__(self):
        if self.__new_times__["end_hour"] == self.__new_times__["start_hour"] and\
           self.__new_times__["end_minute"] == self.__new_times__["start_minute"]:
            return True
        else:
            return False

    

    #Params: sich selbst
    #return: True, wenn alles nötige abgearbeitet ist
    #Funktion:  schaut, ob gesamte Eingabe in das Schema einer Zahl und das Schema Stunden/ Minuten passt
    def __check_timeisanumber__(self):
        for key in self.__new_times__:
            #wenn ein Eintrag nicht den anforderungen entspricht, wird der alte Wert eingetragen
            if self.__single_number_check__(self.__new_times__[key] , key) == False:
                self.__new_times__[key] = self.__timecheck__[key]


    #Params: sich selbst
    #return: True, wenn alles nötige abgearbeitet ist
    #Funktion:  schaut, jede einzelne Eingabe, ob sie in das Schema einer Zahl und das Schema Stunden/ Minuten passt
    def __single_number_check__(self ,entry,  key):
        if entry.isdecimal():

            #schauen, ob Eingabe dem Format Stunden oder Minuten entspricht
            minute_or_hour = key.find("hour")

            #Zahl für das Zeitformat
            number = 0
            if  minute_or_hour >= 0:
                #prüft, ob Eingabe dem Stunden FOrmat entspricht
                number = 23
            else:   
                minute_or_hour = key.find("minute")
                if minute_or_hour >= 0:
                    number = 59
                #prüft, ob Eingabe dem Stunden FOrmat entspricht   
            if number == 0:
                return False
            else:
                return self.__check_time_format__(int (entry) , number)
                
        else:
            return False

    #Params (self): sich selbst ;  den Eintrag aus dem dict, den man prüfen muss ; die Zahl, entsprechend dem Zahlenformat
    #Params (time_entry): den Eintrag aus dem dict, den man prüfen muss 
    #Params (max_number): die Zahl, entsprechend dem Zahlenformat
    #return: True, wenn alles nötige abgearbeitet ist
    #Funktion:  schaut, ob  Zahö in das Schema Stunden/ Minuten passt
    def __check_time_format__(self, time_entry , max_number):
        if time_entry < 0 or time_entry > max_number:
            return False
        else:
            return True 

    #Params (self): sich selbst
    #return: True, wenn alles nötige abgearbeitet ist
    #Funktion: fügt in bearbeitungsdict der neuen Zahlen die Zahl aus der DB ein
    def __complete_times_dict__(self):
        for key in self.__new_times__:
            if self.__new_times__[key] == None:
                self.__new_times__[key] = self.__timecheck__[key]
        return 


################################################################################################################################################
#DB
################################################################################################################################################
    #Params (self): sich selbst
    #return: nichts
    #Funktion: erzeugt ein dict mit den Daten aus der DB
    def __db_entry_times__(self):
        #DB-Verbindung und SQL Befehl
        self.__timecheck__ = main_sqlalchemy("working_time").get_worktime()
        return 
