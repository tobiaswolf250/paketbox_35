#Database_Connection
import traceback
import pymysql
from datetime import datetime
from sqlalchemy import create_engine
import sqlalchemy
#from sqlalchemy.engine import create_engine
from sqlalchemy.sql.expression import column
import check_process.sql.database02 as db
from sqlalchemy.orm import sessionmaker
from sqlalchemy import false, func
from sqlalchemy import desc

# engine = sqlalchemy.create_engine("mysql+pymysql://root:123456@localhost:3306/Paketbox")
# Session = sqlalchemy.orm.sessionmaker()
# Session.configure(bind=engine)
# Session = Session()

engine = create_engine("mysql+pymysql://root:123456@localhost:3306/Paketbox")

Session = sessionmaker(engine)


class Db_Connection():

    def __int__(self):
        print("Db_Connection created") 

    #List alle Zeiten aus der Datenbank Working_Time aus
    #Parameter1: Name der Datenbank
    #Rückgabe: Daten werden als Dictionary zurückgegeben
    def get_worktime_data(self):#, name_database):
        with Session.begin() as session:
            output = session.query( db.Working_Time.Start_h , db.Working_Time.Start_m , db.Working_Time.End_h , db.Working_Time.End_m ).all()
                
            return output

    #Überschreibt die Daten in der Package_Number Datenbank
    #Parameter1: sich selbst
    #Parameter2: die neuen Daten mit den die alten überschrieben werden sollen
    def update_data(self , which_database , old_content , new_content ):
        with Session.begin() as session:
            if which_database == "emailaddress":
                name_database = db.Emailaddress
                attribute =  name_database.address
                updated_entry = { attribute : new_content }

            elif which_database == "package_number":
                name_database = db.Package_Number
                attribute =  name_database.number
                updated_entry = { attribute : new_content }

            session.query( name_database ).filter( attribute == old_content ).update( updated_entry )
            
            return 

    #Überschreibt die Daten in der Working_Time Datenbank
    #Parameter1: sich selbst
    #Parameter2: die neuen Daten mit den die alten überschrieben werden sollen
    def update_worktime_data(self, old_content , new_content  ):
        with Session.begin() as session:
            print ("zeile 51 in D_c: " , new_content)
            name_database = db.Working_Time
            update_dict = { name_database.Start_h : new_content["start_hour"] , name_database.Start_m : new_content["start_minute"] , \
                        name_database.End_h : new_content["end_hour"] , name_database.End_m : new_content["end_minute"] }
            session.query(name_database).filter(name_database.Start_h == old_content["start_hour"] , name_database.Start_m == old_content["start_minute"] , \
                                                name_database.End_h == old_content["end_hour"], name_database.End_m == old_content["end_minute"]).update(update_dict)

            return 
    
    #Fügt Daten in die Datenbank ein
    #Parameter1: Name der Datenbank
    #Parameter2: Daten die eingefügt werden sollen
    def insert_data(self, new_data):
        with Session.begin() as session:
            session.add(new_data)
            session.commit()
            

    def get_data (self, name_database):
        with Session.begin() as session:
            result = session.query(name_database).all()
            return result 

    def get_data_desc (self , name_database):
        with Session.begin() as session:
            result = session.query(name_database).order_by(desc(name_database.timestamp))
            return result #wegen DESC stmt = select(users_table).order_by(desc(users_table.c.name))
        
    #Löscht Daten aus der Datenbank
    #Parameter1: Name der Datenbank, aus der etwas gelöscht werden soll
    #Parameter2: Daten die gelöscht werden sollen
    def delete_data (self, name_database , attribute ,  content):
        with Session.begin() as session:
            session.query(name_database).filter(attribute == content).delete()
            
        return 

    #Schaut, ob Paketnummer in DAtenbank gespeichert ist
    #Parameter1: Name der Datenbank
    #Parameter2: Paketnummer nach der gesucht werden soll
    #Rückgabe: True, wenn Paketnummer in datenbank vorhanden ist. False, wenn Paketnummer nicht vorhanden ist
    def find_data (self, name_database , attribute ,  content):
        with Session.begin() as session:
            try:
                if session.query(name_database).filter(attribute == content).one_or_none() is None:
                    return False
                else:
                    return True
            except:
                traceback.print_exc()



    def row_count ( self , which_database , number):
        with Session.begin() as session:
            all_numbers = 0
            if which_database == "package_number":
                all_numbers = session.query(func.count(db.Package_Number.number)).scalar()
            return all_numbers

########################################################################################################################
    #Liest alle Daten aus der Datenbank aus
    #Parameter1: Name der Datenbank aus der die Daten ausgelesen werden sollen
    #Rückgabe: Mehrdimensionale Liste, mit den Daten(Log, Package_number,...), in der ersten Dimension und dem timestamp in der zweiten Dimension
    def select_all(self, name_database):
        with Session.begin() as session:
            if name_database == db.Package_Number_Delivered:
                result = session.query(name_database).all()
                list1 = []
                list2 = []
                for number_data in result:
                    list1.append(number_data.__dict__["number_delivered"])
                    list2.append(number_data.__dict__["timestamp"])
                return list1, list2

            elif name_database == db.Package_Number:
                result = session.query(name_database).all()
                list1 = []
                list2 = []
                for number_data in result:
                    list1.append(number_data.__dict__["number"])
                    list2.append(number_data.__dict__["timestamp"])
                return list1, list2

            elif name_database == db.Log:
                result = session.query(name_database).all()
                list1 = []
                list2 = []
                for number_data in result:
                    list1.append(number_data.__dict__["log"])
                    list2.append(number_data.__dict__["timestamp"])
                return list1, list2

            elif name_database == db.Emailaddress:
                result = session.query(name_database).all()
                list1 = []
                list2 = []
                for number_data in result:
                    list1.append(number_data.__dict__["address"])
                    list2.append(number_data.__dict__["timestamp"])
                return list1, list2
######################################################################################################################## 

########################################################################################################################
    #Schaut, ob Paketnummer in DAtenbank gespeichert ist
    #Parameter1: Name der Datenbank
    #Parameter2: Paketnummer nach der gesucht werden soll
    #Rückgabe: True, wenn Paketnummer in datenbank vorhanden ist. False, wenn Paketnummer nicht vorhanden ist
    # def find_data(self, name_database, content):
    #     if name_database == db.Package_Number:
    #         if Session.query(name_database).filter(name_database.number == content).one_or_none() is None:
    #             return False
    #         else:
    #             return True
########################################################################################################################

    #Zerstört die Instanz
    def __del__(self):
        print("Db_Connection deleted")

    
