#https://mariadb.com/de/resources/blog/using-sqlalchemy-with-mariadb-connector-python-part-1/
from time import timezone
from unicodedata import name
import sqlalchemy
#from sqlalchemy import engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import func
from sqlalchemy.sql.schema import PrimaryKeyConstraint
import uuid






Base = declarative_base()

engine = sqlalchemy.create_engine("mysql+pymysql://root:123456@localhost:3306/Paketbox")

class Emailaddress(Base):
    __tablename__='emailaddress'
    address=sqlalchemy.Column(sqlalchemy.VARCHAR(100))
    timestamp= sqlalchemy.Column(sqlalchemy.DateTime(timezone=True), server_default=func.now())
    id = sqlalchemy.Column(sqlalchemy.INTEGER, primary_key=True, autoincrement=True)
    
class Package_Number(Base):
    __tablename__='package_number'
    number=sqlalchemy.Column(sqlalchemy.VARCHAR(40))
    timestamp= sqlalchemy.Column(sqlalchemy.DateTime(timezone=True), server_default=func.now())
    id = sqlalchemy.Column(sqlalchemy.INTEGER, primary_key=True, autoincrement=True)

class Package_Number_Delivered(Base):
    __tablename__='package_number_delivered'
    number_delivered=sqlalchemy.Column(sqlalchemy.VARCHAR(40))
    timestamp= sqlalchemy.Column(sqlalchemy.DateTime(timezone=True),server_default=func.now())
    id = sqlalchemy.Column(sqlalchemy.INTEGER, primary_key=True, autoincrement=True)

class Log(Base):
    __tablename__='log'
    log=sqlalchemy.Column(sqlalchemy.VARCHAR(50))
    timestamp= sqlalchemy.Column(sqlalchemy.DateTime(timezone=True), server_default=func.now())
    id = sqlalchemy.Column(sqlalchemy.INTEGER, primary_key=True, autoincrement=True)
    
class Working_Time(Base):
    __tablename__='working_time'
    Start_h=sqlalchemy.Column(sqlalchemy.VARCHAR(3))
    Start_m=sqlalchemy.Column(sqlalchemy.VARCHAR(3))
    End_h=sqlalchemy.Column(sqlalchemy.VARCHAR(3))
    End_m=sqlalchemy.Column(sqlalchemy.VARCHAR(3))
    id = sqlalchemy.Column(sqlalchemy.INTEGER, primary_key=True, autoincrement=True)
    


#Überschreibt die Daten in der Working_Time Datenbank
#Parameter1: sich selbst
#Parameter2: die neuen Daten mit den die alten überschrieben werden sollen
def insert_worktime_data():
    name_database = Working_Time
    start_entry = name_database( Start_h = "07" , Start_m = "00" , \
                End_h = "19" , End_m = "00" )
    Session.add(start_entry)
    Session.commit()
    return 


#Überschreibt die Daten in der Working_Time Datenbank
#Parameter1: sich selbst
#Parameter2: die neuen Daten mit den die alten überschrieben werden sollen
def insert_emailadress_test():
    name_database = Emailaddress
    start_entry = name_database( address = "phipsifredle@web.de" )
    Session.add(start_entry)
    Session.commit()
    return 

#Überschreibt die Daten in der Working_Time Datenbank
#Parameter1: sich selbst
#Parameter2: die neuen Daten mit den die alten überschrieben werden sollen
def insert_packagenumber_delivered_test():
    name_database = Package_Number_Delivered
    start_entry = name_database( number_delivered = "0987654321")
    Session.add(start_entry)
    Session.commit()
    return 

#Überschreibt die Daten in der Working_Time Datenbank
#Parameter1: sich selbst
#Parameter2: die neuen Daten mit den die alten überschrieben werden sollen
def insert_packagenumber_test():
    name_database = Package_Number
    start_entry = name_database( number = "1234567890")
    Session.add(start_entry)
    Session.commit()
    return 

#Überschreibt die Daten in der Working_Time Datenbank
#Parameter1: sich selbst
#Parameter2: die neuen Daten mit den die alten überschrieben werden sollen
def insert_log_test():
    name_database = Log
    start_entry = name_database( log = "Scanner active")
    Session.add(start_entry)
    Session.commit()
    return 

if __name__ == '__main__':
    Base.metadata.create_all(engine)

    engine = sqlalchemy.create_engine("mysql+pymysql://root:123456@localhost:3306/Paketbox")
    Session = sqlalchemy.orm.sessionmaker()
    Session.configure(bind=engine)
    Session = Session()
    #delete_data ()
    insert_worktime_data()
    insert_emailadress_test()
    insert_packagenumber_delivered_test()
    insert_packagenumber_test()
    insert_log_test()






       
