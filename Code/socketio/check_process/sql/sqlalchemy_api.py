from shutil import which
from check_process.sql.database_connection import Db_Connection
import check_process.sql.database02 as db
import json
import datetime
import traceback
import io
import csv

class main_sqlalchemy (Db_Connection) :
    which_database = " "
    db_name = " "
    attribute = " "
    line = []   
             

    def __init__( self , which_database = None ) -> None:
        self.which_database = which_database 
        if (which_database == "package_number"):
            self.db_name = db.Package_Number
            self.attribute = self.db_name.number
        
        elif (which_database == "emailaddress"):
            self.db_name = db.Emailaddress
            self.attribute = self.db_name.address
        
        elif (which_database == "log"):
            self.db_name = db.Log
            self.attribute = self.db_name.log
            self.line = [('Log '), ('Date '), ('Time')]
        
        elif (which_database == "package_number_delivered"):
            self.db_name = db.Package_Number_Delivered
            self.attribute = self.db_name.number_delivered
            self.line = [("number_delivered "), ("Date "), ("Time")]

        super().__init__()     
                       
    def get_worktime(self):
        output = super().get_worktime_data()
        result_dict = {}
        result_dict.update ({"start_hour" : output [0][0] })
        result_dict.update ({"start_minute" : output [0][1] })
        result_dict.update ({"end_hour" : output [0][2] })
        result_dict.update ({"end_minute" : output [0][3] })
        return result_dict

    def update_worktime( self , old_time , new_time):
        super().update_worktime_data( old_time , new_time )

    def update_value( self , old_number , new_number ):
        super().update_data( self.which_database ,old_number , new_number )

    def insert_data (self , data):
        new_entry = " "
        if self.which_database == "package_number":
            new_entry = self.db_name( number = data )

        elif self.which_database == "emailaddress":
            new_entry = self.db_name( address = data )

        elif self.which_database == "log":
            new_entry = self.db_name( log = data )
        
        elif self.which_database == "package_number_delivered":
            new_entry = self.db_name( number_delivered = data )

        super().insert_data( new_entry )

    #Funktionen: führt einen Select * From ... aus  
    #Parameter (self): sich selbst
    #Parameter (second): wenn dieser True, dann wird der timestamp der Tabelle geholt
    #Rückgabe: Ein Array der Daten, die geholte werden sollen
    def execute_get_data( self , second = False ):
        #aufruf der Funktion in der SQL-ALchemy, speichern in variable
        data_array = []
        timestamp_array = []
        
        if self.which_database == "package_number":
            output = super().get_data_desc( self.db_name )
            for instance in output:
                data_array.append (instance.number) 

        if self.which_database == "package_number_delivered":
            output = super().get_data_desc( self.db_name )
            for instance in output:
                data_array.append (instance.number_delivered)

        if self.which_database == "emailaddress":
            output = super().get_data_desc( self.db_name )
            for instance in output:
                data_array.append (instance.address)    

        if self.which_database == "log":
            output = super().get_data_desc( self.db_name )                
            for instance in output:
                data_array.append (instance.log) 

        if (second):
            for instance in output:
                timestamp_array.append (instance.timestamp)
            return data_array , timestamp_array

        return data_array

    def delete_data ( self , to_delete ):      
        super().delete_data( self.db_name , self.attribute , to_delete )   
        return 

    def find_data ( self , to_find ):      
        result = super().find_data( self.db_name , self.attribute , to_find )   
        return result  
    
    #Funktion: zählt den Inhalt der Eintrage einer Tabelle und begrenzt die EInträge auf bestimmte Anzahl 
    #Parameter (table): Name der Tabelle
    #Parameter (number): maximale Anzahl der Eintrage die angezigt werden
    #Rückgabe: True, wenn noch einträge vollzogen werden dürfen 
    def rowcount (self , number):
        entries = super().row_count( self.which_database , number)
        #True, wenn weniger Zeilen als erlaubt
        if entries < number:
            return True
        else:
            return False

    #Funktion: liest das, was es bekommt Stück für Stück ein und speichert jeden einzelnen Wert in ein Array
    ##Parameter (result): die aus der Datenbank ausgelesene Datei wird so konvertiert, dass Jinji 2 ihn besser verarbeiten kann (nötig wenn mehrere Zeilen eingelesen werden)
    ##Rückgabe: gibt den Inhalt im Array zurück 
    def get_change_sql_output ( self ):
        result = self.execute_get_data()
        #Array als Zwischenspeicher
        result_dict = {}         
        #füllt ins Array result_array für jeden Eintrag aus result den aktuellen Wert ein
        for row in result:
            result_dict.update({ row : row })
        #result_dict = json.dumps(result_dict)
        return result_dict

class use_limited_output(main_sqlalchemy):
    value_time = []
    value_date = []
    value_entry = []
    value_dict = {}
    number = 0

    #Funktion: Konstruktor
    #Parameter (number): die gewünschte Anzahl, wenn man alle will, gibt man nichts ein
    #Parameter (which_database): einen String mit dem Namen der DB --> definiert sind die Namen in der Datei: database02
    #Rückgabe: keinen
    def __init__(self , which_database , number = -1  ) -> None:
        super().__init__(which_database)
        self.number = number
        return


    #Funktion: füllt ein dict mit den Daten timestamp und einem attribute aus der Datenbank
    #Parameter: keine
    #Rückgabe: keinen
    def get_data_amount( self ):
        all_value_entrys , all_timestamp_entrys = super().execute_get_data( True )
        self.value_time.clear()
        self.value_date.clear()
        self.value_entry.clear()
        self.value_dict.clear()

        
        for count , timestamp_entry in enumerate (all_timestamp_entrys):
            # print (timestamp_entry.time())
            time = timestamp_entry.time().strftime("%H:%M:%S")
            date = timestamp_entry.date().strftime("%d-%b-%Y")
            self.value_time.append(time)
            self.value_date.append(date)
            self.value_entry.append(all_value_entrys [count])
            if (count >= (self.number-1) and self.number != -1):
                break
        self.value_dict = {  "time":self.value_time , "date" : self.value_date , "value" : self.value_entry }
        return 

    #Funktion: gibt aus dem Daten dict die Einträge vom gewünschten Attribute
    #Parameter: keine
    #Rückgabe: das Array mit den Einträgen aus der DB
    def get_values (self):
        output =  self.value_dict ["value"]
        return output
    
    #Funktion: gibt aus dem Daten dict die Einträge vom Datum des timestamps
    #Parameter: keine
    #Rückgabe: das Array mit den Einträgen aus der DB
    def get_date (self):
        output =  self.value_dict ["date"]
        return output
        
    #Funktion: gibt aus dem Daten dict die Einträge von der Zeit des timestamps
    #Parameter: keine
    #Rückgabe: das Array mit den Einträgen aus der DB
    def get_time (self):
        output =  self.value_dict ["time"]
        return output

class create_csv (use_limited_output): 

    #Funktion: Konstruktor
    #Parameter (number): die gewünschte Anzahl, wenn man alle will, gibt man nichts ein
    #Parameter (which_database): einen String mit dem Namen der DB --> definiert sind die Namen in der Datei: database02
    #Rückgabe: keinen
    def __init__(self , which_database ) -> None:
        super().__init__(which_database)
        return

    #Funktion: liest den Inhalt aus der Tablle einer Datenbank ein 
    #Parameter (which_table): wählt die Tabelle aus, in die geschrieben werden soll
    #Rückgabe: False, wenn es nicht geklappt hat
    def write_excel_file (self):
        try:
            super().get_data_amount()
            value = super().get_values()
            date = super().get_date()
            time = super().get_time()
            #schreibt die oben beschriebene Line in die csv Datei
            output = io.StringIO()
            writer = csv.writer(output)
            writer.writerow(self.line)
            #geht den Gesamten Inhalt durch und schreibt in csv
            for count, entry in enumerate(value):
                #line = [(str(row[0])),(str(row[1])),(str(row[2]))]
                #for others than Libre office
                line = [(entry),(str(date[count])),(str(time[count]))]
                writer.writerow(line)
            output.seek(0)
            #gibt die Datei zurück
            return output
        except:
            traceback.print_exc() 
            return False 





    



