import datetime
from check_process.sql.sqlalchemy_api import main_sqlalchemy  
#from database_connection import Db_Connection


path_config_json = "/flask/static/exchange/config.json"


class check_time:

    def worktime(self):
        #connection = Db_Connection()
        approval = True
        #Benutzerzeitraum aus Datenbank einlesen
        working_time = main_sqlalchemy().get_worktime()
        print (working_time)

        h_now = int(datetime.datetime.now().strftime('%H'))
        m_now = int(datetime.datetime.now().strftime('%M'))
        h_low_db = int(working_time['start_hour'])
        m_low_db = int(working_time['start_minute'])
        h_high_db = int(working_time['end_hour'])
        m_high_db = int(working_time['end_minute'])

        print(h_now)
        print(m_now)

        if h_now > h_low_db and h_now < h_high_db \
            or h_now == h_low_db and m_now >= m_low_db \
            or h_now == h_low_db and h_now == h_high_db \
            and m_now >= m_low_db and m_now <= m_high_db \
            or h_now == h_high_db and m_now <= m_high_db:  
            approval = True
        else:
            approval = False

        #if h_low_db > h_high_db:
        #    if h_now > h_low_db and h_now > h_high_db \
        #        or h_now < h_low_db and m_now < h_high_db \
        #        or h_now == h_low_db and m_now >= m_low_db \
        #        or h_now == h_high_db and m_now >= m_high_db:
        #        approval = True
        #    else:
        #        approval = False

        #del connection

        return approval

