#Verwaltung der Paketnummer
#from database import *
#from z_old_sql_uses import *
import json
import traceback
from check_process.sql.sqlalchemy_api import main_sqlalchemy


# import sys
# home_path = sys.path[0]
# sys.path[0] += "/check_process"

# from sql.sqlalchemy_api import main_sqlalchemy

# sys.path[0] = home_path


class work_with_new_packagenumbers():

    #Funktion: vergleicht Inhalt der Datenabnk mit dem Wert, der Übergeben wird
    #Parameter (__packagenumber__): die aktuell, eingegebene Paketnummer  
    #Rückgabe : True wenn Nummer noch nicht vorhanden ist
    def __init__(self , new_package_number) -> None:
        self.__packagenumber__ = new_package_number
        self.__delete_space__()
        return  

    #Funktion: vergleicht Inhalt der Datenabnk mit dem Wert, der Übergeben wird
    #Parameter (__packagenumber__): die aktuell, eingegebene Paketnummer  
    #Rückgabe : True wenn Nummer noch nicht vorhanden ist
    def __delete_space__ (self):
        self.__packagenumber__ = self.__packagenumber__.lstrip() 
        self.__packagenumber__ = self.__packagenumber__.rstrip() 
        return 



    #Funktion: vergleicht Inhalt der Datenabnk mit dem Wert, der Übergeben wird
    #Parameter (__packagenumber__): die aktuell, eingegebene Paketnummer  
    #Rückgabe : True wenn Nummer noch nicht vorhanden ist
    def check_new_packagenumber (self):
        try:
            if not self.__packagenumber_check__():
                return True
            self.__sqlentry_package__()
            return True
        except:
            traceback.print_exc() 
            return False


    #Funktion: vergleicht Inhalt der Datenabnk mit dem Wert, der Übergeben wird
    #Parameter (__packagenumber__): die aktuell, eingegebene Paketnummer  
    #Rückgabe : True wenn Nummer noch nicht vorhanden ist
    def __sqlentry_package__(self):
        #führt SQL-Befehl aus: Fügt Daten in die DB
        main_sqlalchemy("package_number").insert_data( self.__packagenumber__  )
        return 

    #Funktion: vergleicht Inhalt der Datenabnk mit dem Wert, der Übergeben wird
    #Parameter (__packagenumber__): die aktuell, eingegebene Paketnummer  
    #Rückgabe : True wenn Nummer noch nicht vorhanden ist
    def __packagenumber_check__(self):
        return self.__check_packagenumber__() and self.__check_package_compare__()

    #Funktion: vergleicht Inhalt der Datenabnk mit dem Wert, der Übergeben wird
    #Parameter (pass_other_number): die Mgk eine eigens eingegeben Paketnummer zu vergleichen
    #Rückgabe : True wenn Nummer noch nicht vorhanden ist
    def __check_package_compare__ (self , pass_other_number = None):
        packagenumber_to_check = " "
        if pass_other_number == None:
            packagenumber_to_check = self.__packagenumber__
        else:
            packagenumber_to_check = pass_other_number
        print ("68: " , packagenumber_to_check)
        all_packagenumbers = main_sqlalchemy("package_number").execute_get_data()
        #geht Inhalt aus Datenbank durch
        for entry in all_packagenumbers:
            if entry == packagenumber_to_check:
                return False
        return True

    #Funktion: überprüft ob eingegebene Paketnummer anforderungen entspricht
    #Parameter (__packagenumber__): aktuell eingetragene Paketnummer 
    #Rückgabe: True, wenn Nummer valide ist
    def __check_packagenumber__(self , pass_other_number = None ):
        packagenumber_to_check = " "
        if pass_other_number == None:
            packagenumber_to_check = self.__packagenumber__
        else:
            packagenumber_to_check = pass_other_number
        #überprüft Gültigkeit der Nummer
        if len(packagenumber_to_check) < 40: 
                return True
        else:
                return False



class work_with_changed_packagenumber(work_with_new_packagenumbers):


    #Funktion: führt die Email-Validierung entsrpehcn der Mail_change anforderung durch 
    #Parameter: erhält die E-Mail 
    #Rückgabe: Validierung erfolgereich = True
    def __init__(self, packagenumbers):
        packagenumbers = json.loads(packagenumbers)
        self.__old_packagenumber__ = packagenumbers["old"]
        new_packagenumber = packagenumbers["new"]
        super().__init__(new_packagenumber)
        self.__delete_space__()


    #Funktion: führt die Email-Validierung entsrpehcn der Mail_change anforderung durch 
    #Parameter: erhält die E-Mail 
    #Rückgabe: Validierung erfolgereich = True
    def __delete_space__ (self):
        self.__old_packagenumber__ = self.__old_packagenumber__.lstrip() 
        self.__old_packagenumber__ = self.__old_packagenumber__.rstrip()
        return    


    #Funktion: vergleicht Inhalt der Datenabnk mit dem Wert, der Übergeben wird
    #Parameter (__packagenumber__): die aktuell, eingegebene Paketnummer  
    #Rückgabe : True wenn Nummer noch nicht vorhanden ist
    def packagenumber_change_checker (self):
        try:
            if not self.__check_both_numbers__():
                return True
            self.__sql_update__()
            return True
        except:
            traceback.print_exc() 
            return False


    def __check_both_numbers__(self):
        return super().__packagenumber_check__() and self.__old_number_check__()


    def __old_number_check__ (self):
        return not(super().__check_package_compare__(self.__old_packagenumber__)) and super().__check_packagenumber__(self.__old_packagenumber__)

    #Funktion: fügt in db die neue mail ein 
    #Parameter (self): enthält sich selbst
    #Rückgabe: keinen
    def __sql_update__(self):
        #führt SQL-Befehl aus 
        main_sqlalchemy("package_number").update_value( self.__old_packagenumber__ , self.__packagenumber__ )
        return
 

class work_with_deleted_packagenumbers(work_with_new_packagenumbers):

    def __init__(self, old_package_number):
        super().__init__(old_package_number)


    #Funktion: vergleicht Inhalt der Datenabnk mit dem Wert, der Übergeben wird
    #Parameter (__packagenumber__): die aktuell, eingegebene Paketnummer  
    #Rückgabe : True wenn Nummer noch nicht vorhanden ist
    def packagenumber_delete_checker (self):
        try:
            if not self.__old_number_check__():
                return True
            self.__sql_delete__()
            return True
        except:
            traceback.print_exc() 
            return False

    def __old_number_check__ (self):
        return not(super().__check_package_compare__(self.__packagenumber__)) and super().__check_packagenumber__(self.__packagenumber__)

    #Funktion: löscht in db Paketnummer 
    #Parameter (self): enthält sich selbst
    #Rückgabe: keinen
    def __sql_delete__(self):
        main_sqlalchemy("package_number").delete_data(self.__packagenumber__)
        return



################################################################################################################################################

